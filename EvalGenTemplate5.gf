--# -coding=UTF-8
abstract EvalGenTemplate5 = {
    flags
        startcat = Statement;
    cat
        --Semantic cats
        Statement;
        IndexedStatement;
        BasePhysicalObject;
        PhysicalObject;
        PhysicalProperty PropertyType;
        PropertyType;
        Location;
        LocationRelation;
        
    
    fun
        AddStatementIndex : Int -> Statement -> IndexedStatement;
        Color : PropertyType;
        
        SemVar5aRed,SemVar5bYellow,SemVar5cBlue,SemVar5dGreen,SemVar5eBlack,SemVar5fWhite : PhysicalProperty Color;
        SemVar4aDiningTable,SemVar4bSilverTray : Location; --Context fixing by using a specific type of table, Finnish conflates "table" and "desk"
        
        SemVar3aOnRelation : LocationRelation;
        SemVar3aOnRelationWithVerb : LocationRelation;
        
        --Semantic funs
        
        
        
        SemVar1aBottle,SemVar1bCup,SemVar1cPlate,SemVar1dMug : BasePhysicalObject;
        
        SemVar2aBarePhysicalObject : BasePhysicalObject -> PhysicalObject; 
        SemVar2bPhysicalObjectWithLocationAdverb : BasePhysicalObject -> Location -> LocationRelation -> PhysicalObject;
        SemVar2bPhysicalObjectWithLocationRelative : BasePhysicalObject -> Location -> LocationRelation -> PhysicalObject;
        
        --Syntactic funs
        --This is the base semantic form
        HavePhysicalProperty : (k : PropertyType) -> PhysicalObject -> PhysicalProperty k -> Statement;
        
        --These are the syntactic alternatives for the semantic form
        
        --"the ball is red"
        Subject_PhysObject1Predicative_Property2 : (k : PropertyType) -> PhysicalObject -> PhysicalProperty k -> Statement;
        
        --"the ball's color is red"
        Subject_PhysObjectGenitive_PropertyName1Predicative_Property2 : (k : PropertyType) -> PhysicalObject -> PhysicalProperty k -> Statement;
        
        
        --"the color of the ball is red"
        Subject_PropertyName_PropertyNamePossessive_PhysObject1Predicative_Property2 : (k : PropertyType) -> PhysicalObject -> PhysicalProperty k -> Statement;
        
        --"the ball is red in color"
        Subject_PhysObject1Predicative_Property_PropertyNameInsideLocative2 : (k : PropertyType) -> PhysicalObject -> PhysicalProperty k -> Statement;
        
        --"pallo on väriltään punainen
        Subject_PhysObject1Predicative_Property_PropertyNameOutsideSource2 : (k : PropertyType) -> PhysicalObject -> PhysicalProperty k -> Statement;
        
        
    
    }