#!/usr/bin/env python3
import subprocess
import sys
import shutil
import os
import copy
import time
import re
import argparse
import multiprocessing as mp
import logging
import string
import itertools
import math
from omorfi.omorfi import Omorfi

from collections import deque

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',filename='score_trans.log',level=logging.INFO,filemode='w')

def construct_case_shift_key(rcase,hcase):
	return "CASESHIFT-%s-%s"%(hcase,rcase)


class OmorfiAnalysis():
	def __init__(self,analysis_tuple,surface=None):
		analysis_string = analysis_tuple[0]
		analysis_compound_parts = analysis_string.split("[BOUNDARY=COMPOUND]")
		def split_parts(comp_part):
			analysis_splits = comp_part.split("]")
			analysis_fields = [x.split("=") for x in analysis_splits]
			#print(analysis_fields)
			compound_fields = {field[0][1:]: field[1] for field in analysis_fields if len(field) > 1}
			return compound_fields
		self.parts = [split_parts(x) for x in analysis_compound_parts]
		self.fields = self.parts[-1]
		#print(self.parts)
		if "GUESS" in self.fields:
			self.unknown = True
		else:
			self.unknown = False
		self.lemma = "+".join([x["WORD_ID"] for x in self.parts])
		if "CASE" in self.fields:
			self.case = self.fields["CASE"]
		else:
			self.case = None
		if "UPOS" in self.fields:
			self.wordclass = self.fields["UPOS"]
		else:
			self.wordclass = None
		if "NUM" in self.fields:
			self.num = self.fields["NUM"]
		else:
			self.num = None
		if len(self.parts) > 1:
			self.is_compound = True	
		else:
			self.is_compound = False
		if surface is not None:
			self.surface = surface
		if "PRONTYPE" in self.fields and self.fields["PRONTYPE"]=="PRS":
			self.animate = True
		else:
			self.animate = False
		if "TENSE" in self.fields:
			self.tense = self.fields["TENSE"]
		else:
			self.tense = None
		self.neg = "NEG" in self.fields
		if "PERS" in self.fields:
			self.person = self.fields["PERS"]
		else:
			self.person = None
		#print(self.fields)

	def __str__(self):
		return str(self.fields)

def analyse_ref_tokens(sym_path,omorfi):
	ref_tokens = set()
	sym_analyses = []
	compound_analyses = []
	with open(sym_path,'r') as syms_file:
		for line in syms_file.readlines():
			split_line =  line.split()
			if len(split_line) > 0:
				sym_token = split_line[0]
			else:
				continue
			if sym_token == "<unk>" or sym_token == "<eps>":
				continue
			sym_token_analyses = omorfi.analyse(sym_token)
			ref_tokens.add(sym_token)
			for sym_token_analysis in sym_token_analyses:
				#skip compound analyses, they are mostly junk
				analysis = OmorfiAnalysis(sym_token_analysis,sym_token)
				if analysis.is_compound:
					compound_analyses.append(analysis)
				sym_analyses.append((sym_token,analysis))
	return (ref_tokens,sym_analyses,compound_analyses)

class CompoundEdits():
	def __init__(self):
		self.lemma_joins = []
		self.surface_joins = []
  

def extract_compound_edits(tokenized_translation,compound_analyses,omorfi):
	compound_edits = CompoundEdits()
	token_analyses = [[OmorfiAnalysis(y) for y in omorfi.analyse(x)] for x in tokenized_translation]
	token_lemmas = [[y.lemma for y in x] for x in token_analyses]
	for compound_analysis in compound_analyses:
		starting_lemma = compound_analysis.parts[0]["WORD_ID"]
		for (index,lemmaset) in enumerate(token_lemmas):
			if len(token_lemmas) < index + len(compound_analysis.parts):
				#logging.info("tokens lemmas: %s, index: %s"%(token_lemmas,index))
				continue 
			if starting_lemma in lemmaset:
				compound_tokens = [tokenized_translation[index]]
				#print(compound_analysis)
				lemma_match = True
				next_index = index+1
				#print(compound_analysis.parts)
				for compound_analysis_part in compound_analysis.parts[1:]:
					next_lemma = compound_analysis_part["WORD_ID"]
			
					if next_lemma not in token_lemmas[next_index]:
						lemma_match = False
						break
					compound_tokens.append(tokenized_translation[next_index])
					next_index+=1
				if lemma_match:
					compound_edits.lemma_joins.append((compound_tokens,compound_analysis.surface))
	return compound_edits

#pronoun edits need to be a different class on count of different diagnostic relevance
def extract_pron_edits(hyp_token_analyses,sym_analyses):
	pron_matches = [x for x in sym_analyses if x[1].wordclass=="PRON"]
	
	#if you have a number+case match, it's an animate/nonanimate switch
	token_animate = len([x for x in hyp_token_analyses if x.animate]) > 0
	different_animacy_matches = [x for x in pron_matches if x[1].animate != token_animate]
	
	animacy_edits = [x for x in different_animacy_matches if (x[1].num,x[1].case) in [(y.num,y.case) for y in hyp_token_analyses]]
	
	return animacy_edits
	
#these are for subs to same lemma, but they also record the type of the case change
def extract_case_shifts(lemma_matches,hyp_token_analyses):
	#check for case in lemma matches, then check what case hyp token with same lemma has
	case_shifts = []

	for lemma_match in lemma_matches:
		if lemma_match[1].case is not None:
			hyp_token_analysis_same_lemma_diff_case = [
				x for x in hyp_token_analyses 
				if x.lemma == lemma_match[1].lemma 
				and x.case is not None
				and x.case != lemma_match[1].case
			]
			for hyp_analysis in hyp_token_analysis_same_lemma_diff_case:
				case_shifts.append((lemma_match[1].surface,(lemma_match[1].case,hyp_analysis.case)))
	logging.info("%d case shifts found."%len(case_shifts))
	return case_shifts
			

def extract_morpho_edits(tokenized_translation,ref_tokens,sym_analyses,omorfi):
	morpho_edits = dict()
	pron_edits = dict()
	for token in tokenized_translation:
	#apply only for tokens that are not in the reference fst
	#if token not in ref_tokens:
		#print(token)
		hyp_token_analyses = [OmorfiAnalysis(x) for x in omorfi.analyse(token)]
		unktoken = len([x for x in hyp_token_analyses if not x.unknown])==0
		#print([str(x) for x in hyp_token_analyses])
		#first check if lemma in ref tokens
		lemma_matches = [x for x in sym_analyses if x[1].lemma in [y.lemma for y in hyp_token_analyses]]
		#for lemma_match in lemma_matches:
		#	 print("%s\t%s"%(token,lemma_match[0]))

		#csae shifts are a special type of lemma shift where case changes
		case_shifts = extract_case_shifts(lemma_matches,hyp_token_analyses)

		#for tensed verbs
		verb_sym_analyses = [x for x in sym_analyses if x[1].wordclass == "VERB" and x[1].tense is not None and x[1].case is None]
		verb_hyp_analyses = [x for x in hyp_token_analyses if x.wordclass == "VERB" and x.tense is not None and x.case is None]
		tense_num_neg_matches = [x for x in verb_sym_analyses if (x[1].tense,x[1].person,x[1].neg) in [(y.tense,y.person,y.neg) for y in verb_hyp_analyses]]
		
		#print(tense_num_neg_matches)
		
		case_matches = []
		case_num_matches = []

		#case and number matches should be applied only in case of non-compounds, otherwise they'll allow adding tokens on the sly
		token_noncomp_analyses = [x for x in hyp_token_analyses if not x.is_compound]
		if len(token_noncomp_analyses) > 0:
			sym_noncomp_analyses = [x for x in sym_analyses if not x[1].is_compound]
			case_matches = [x for x in sym_noncomp_analyses if x[1].case in [y.case for y in hyp_token_analyses if y.case is not None and y.wordclass == x[1].wordclass]]
			#print([(x,str(y)) for (x,y) in case_matches])
			#for case_match in case_matches:
			#	 print("%s\t%s"%(token,case_match[0]))

			case_num_matches = [x for x in case_matches if x[1].num in [y.num for y in hyp_token_analyses if y.num is not None and y.wordclass == x[1].wordclass]]
			#for case_num_match in case_num_matches:
			#	 print("%s\t%s"%(token,case_num_match[0]))

		#print(set(case_matches).intersection(num_matches))
		morpho_edits[token] = (lemma_matches,case_matches,case_num_matches,tense_num_neg_matches,case_shifts,unktoken)

		pron_analyses = [x for x in hyp_token_analyses if x.wordclass == "PRON"]
		if pron_analyses:
			pron_edits[token] = extract_pron_edits(pron_analyses,sym_analyses)
	
	return (morpho_edits,pron_edits)


class Transition:
	def __init__(self,start,end,input_sym,output_sym,weight):
		self.start = start
		self.end = end
		self.input_sym = input_sym
		self.output_sym = output_sym
		self.weight = weight

	def __str__(self):
		return "%s %s %s %s %s"%(self.start,self.end,self.input_sym,self.output_sym,self.weight)

def create_edit_transducer(ref_syms_path,translation_syms_path,compiled_edit_transducer_path,hyp_tokens,morpho_edits,pron_edits,compound_edits,weight):
	edit_fst = []
	edit_fst_path = compiled_edit_transducer_path.replace(".fst",".txt")
	
	input_alphabet = []
	#input alphabet is translation syms, output ref_syms

	

	logging.info("Processing edit input alphabet %s"%translation_syms_path)
	with open(translation_syms_path,'r') as translation_syms:
		for line in translation_syms.readlines():
			input_alphabet.append(line.split()[0])

	output_alphabet = []
	#input alphabet is translation syms, output ref_syms
	logging.info("Processing edit output alphabet %s"%ref_syms_path)
	with open(ref_syms_path,'r') as ref_syms:
		for line in ref_syms.readlines():
			output_alphabet.append(line.split()[0])

	# No edit
	for l in input_alphabet:
		if l in output_alphabet and l != "<eps>" and l != "<unk>":
			edit_fst.append(Transition("0", "0", l, l, 0))

	# Substitutions: input one character, output another. Note that epsilon is included in characters, so insertion and deletion is included in this
	for l in input_alphabet:
		for r in output_alphabet:
			if len(l) == 1 and l in string.punctuation and r == "<eps>":	
				edit_fst.append(Transition("0", "0", l, r, weight["punct"]))
			elif len(r) == 1 and r in string.punctuation and l == "<eps>":	
				edit_fst.append(Transition("0", "0", l, r, weight["punct"]))
			elif l != r and r == "<eps>":
				edit_fst.append(Transition("0", "0", l, r, weight["delete"]))
			elif l == "<eps>" and r != l:
				edit_fst.append(Transition("0", "0", l, r, weight["insert"]))
			elif l != r and r != "<unk>":
				edit_fst.append(Transition("0", "0", l, r, weight["sub"]))

		if l in morpho_edits:
			(lemma_edits,casenum_edits,case_edits,tense_num_neg_edits,case_shifts,unktoken) = morpho_edits[l]

			if unktoken:
				token_del_and_subs = [x for x in edit_fst if x.input_sym == l]
				for transition in token_del_and_subs:
					if transition.weight == weight["sub"]:
						transition.weight = weight["unksub"]
					elif transition.weight == weight["delete"]:
						transition.weight = weight["unkdel"]					
			else:
				for lemma_edit in lemma_edits:
					edit_fst.append(Transition("0", "0", l, lemma_edit[0], weight["lemma_sub"]))

				for casenum_edit in casenum_edits:
					edit_fst.append(Transition("0", "0", l, casenum_edit[0], weight["casenum_sub"]))

				for case_edit in case_edits:
					#print(case_edit)
					edit_fst.append(Transition("0", "0", l, case_edit[0], weight["case_sub"]))
		
				for tensenum_edit in tense_num_neg_edits:
					#print(case_edit)
					edit_fst.append(Transition("0", "0", l, tensenum_edit[0], weight["tensenum_sub"]))

				for case_shift in case_shifts:
					case_shift_key = construct_case_shift_key(*case_shift[1])
					edit_fst.append(Transition("0", "0", l, case_shift[0], weight[case_shift_key]))


		if l in pron_edits:
			animacy_edits = pron_edits[l]
			
			for animacy_edit in animacy_edits:
				edit_fst.append(Transition("0", "0", l, animacy_edit[0], weight["animacy_sub"]))


	compound_edit_next_state = 0
	for (compound_tokens,compound) in compound_edits.lemma_joins:
		compound_edit_start_state = 0
		compound_edit_token = compound
		for compound_token in compound_tokens[:-1]:
			compound_edit_next_state += 1
			edit_fst.append(Transition(
				str(compound_edit_start_state),
				str(compound_edit_next_state),
				compound_token,
				compound_edit_token, 
				weight["comp_lemma_join"]))
			compound_edit_token = "<eps>"
			compound_edit_start_state = compound_edit_next_state
		last_compound_token = compound_tokens[-1]
		edit_fst.append(Transition(str(compound_edit_start_state),"0",last_compound_token,"<eps>", weight["comp_lemma_join"]))

	
	#remove redundant transitions (like subs when same lemma or case edits exist)
	edit_fst.sort(key=lambda x: (x.start,x.end,x.input_sym,x.output_sym))
	transition_groups = itertools.groupby(edit_fst,lambda x: (x.start,x.end,x.input_sym,x.output_sym))
	filtered_edit_fst = [min(x[1],key=lambda x: x.weight) for x in transition_groups] 
	

	with open(edit_fst_path,'w') as edit_fst_file:
		for transition in filtered_edit_fst:
			edit_fst_file.write(str(transition)+'\n')	
		# Final state
		edit_fst_file.write("0")
		
	#compile edit transducer
	with open(compiled_edit_transducer_path,'w') as compiled_edit_transducer:
		subprocess.call(["fstcompile","--keep_isymbols=true","--keep_osymbols=true","--isymbols=%s"%translation_syms_path,"--osymbols=%s"%ref_syms_path,edit_fst_path],stdout=compiled_edit_transducer)

class TranslationPermutation():
	def __init__(self,tokens,transitions):
		self.tokens = tokens
		self.transitions = transitions
		#this is used when later constructing the fst, to keep track of start state
		self.start_state = 0
		
	def __str__(self):
		return "\n".join(str(x) for x in self.tokens)

	def score(self):
		return sum([x[1] for x in self.transitions])

def add_permutation(permutation_dict,permutation):
	token_key = tuple(permutation.tokens)
	#print(permutation.transitions)
	#print(permutation.tokens)
	#print(permutation.score())
	if token_key in permutation_dict.keys():
		if permutation.score() < permutation_dict[token_key].score():
			permutation_dict[token_key] = permutation
	else:
		permutation_dict[token_key] = permutation
	return permutation_dict

def create_straight_fst(unked_translation):
	transitions = set()
	state = 0
	for token_index,token in enumerate(unked_translation):
		transitions.add("%s %s %s %s\n"%(state,state+1,token,0))
		state += 1
	transitions.add("%s"%state)
	return transitions

def create_translation_fst(unked_translation,reordering_window,weight,move_limit):
		
	#move_limit = len(unked_translation)/2
	permutations = {"": TranslationPermutation(list(enumerate(unked_translation)),[])}
	
	for (index,token) in enumerate(unked_translation):
		#print(index)
		new_permutations = dict()
		window_start = index
		window_end = min(len(unked_translation),index+reordering_window)
		
		for permutation in permutations.values():
			#for each window index create the possible permutation variants (transposition and reordering at the moment)
			for window_index in range(window_start,window_end):
				#this is the normal order of the translation
				if window_index == index:
					transition_cost = 0
					costless_permutation_transitions = list(permutation.transitions)
					costless_permutation_transitions.append((permutation.tokens[window_index],0))
					new_permutations = add_permutation(new_permutations,TranslationPermutation(list(permutation.tokens),costless_permutation_transitions))
				else:
					transition_cost = weight["move"]

					newtransitions = list(permutation.transitions)					  
					newtransitions.append((permutation.tokens[window_index],transition_cost))
					
					#transposition, i.e. the index and window index tokens are switched
					"""new_tokens = list(permutation.tokens)
					if window_index-new_tokens[index][0] <= reordering_window:
						new_tokens[window_index],new_tokens[index] = new_tokens[index],new_tokens[window_index]
						new_permutations = add_permutation(new_permutations,TranslationPermutation(new_tokens,newtransitions))"""

					#reordering, i.e. the index token is moved forward to window index position or the window index token to index position
					reordering_tokens = list(permutation.tokens)
					if all([((index+x[0]+1) - (x[1][0]) <= reordering_window) for x in enumerate(reordering_tokens[index:window_index])]):
						reordering_transitions = list(permutation.transitions)
						fronted_token = reordering_tokens[window_index]
						del reordering_tokens[window_index]
						reordering_tokens.insert(index,fronted_token)
						
						reordering_transitions.append((fronted_token,transition_cost))
						new_permutations = add_permutation(new_permutations,TranslationPermutation(reordering_tokens,reordering_transitions))

					reordering_tokens_right = list(permutation.tokens)
					if window_index-reordering_tokens_right[index][0] <= reordering_window:    
						reordering_transitions_right = list(permutation.transitions)
						
						forwarded_token = reordering_tokens_right[index]
						reordering_tokens_right.insert(window_index+1,forwarded_token)
						del reordering_tokens_right[index]
						reordering_transitions_right.append((reordering_tokens_right[index],transition_cost))
						new_permutations = add_permutation(new_permutations,TranslationPermutation(reordering_tokens_right,reordering_transitions_right))
					

		#dump those permutations where there's been movement past the reordering limit

		permutations = dict(new_permutations)
		#this probably isn't necessary, not important at least, won't slow things down too much, and not sure it works properly
		"""for permutation_key in new_permutations.keys():
			moves = enumerate(x[0] for x in new_permutations[permutation_key].tokens)
			if any([x[0]-x[1] > reordering_window for x in moves]):
				print(permutations[permutation_key].tokens)
			   #del permutations[permutation_key]"""

		#remove permutations with too much accumulated cost
		for permutation_key,permutation in new_permutations.items():
			if permutation.score() > move_limit:
				#print(permutation.tokens)
				del permutations[permutation_key]
		
	transitions = set()
	state_index = 0
	temp_transition_dict = dict()
	all_permutations = permutations.values()
	#process transitions letter by letter
	for token_index,_ in enumerate(unked_translation):
		for permutation in all_permutations:
			#collect all transitions into a dict per loop
			current_transition = permutation.transitions[token_index]
			current_transition_key = (permutation.start_state,current_transition)
			if current_transition_key not in temp_transition_dict:
				state_index += 1
				temp_transition_dict[current_transition_key] = state_index
				
			end_state = temp_transition_dict[current_transition_key]
			transitions.add("%s %s %s %s\n"%(permutation.start_state,end_state,current_transition[0][1],current_transition[1]))
			permutation.start_state = end_state
		#empty the dict, not really necessary
		temp_transition_dict = dict()

	#add end states
	for permutation in all_permutations:
		transitions.add("%s\n"%(permutation.start_state))

	return transitions

	
def evaluate_translation(tokenized_translation,translation_syms_path,translation_index,translation_set_path,test_set_dir,minimize_input,random_reference,shortest_txt_path,compiled_edit_transducer_path,sorted_fst_path,scorer_path,reordering_window,move_limit,weight):
	syms_file_path = translation_syms_path
	input_txt_fsm_path = translation_set_path.replace(".translations",".translation.%s.txt"%translation_index)
	input_fst_path = translation_set_path.replace(".translations",".translation.%s.fst"%translation_index)
	determinized_input_fst_path = translation_set_path.replace(".translations",".translation.determinized.%s.fst"%translation_index)
	minimized_input_fst_path = translation_set_path.replace(".translations",".translation.minimized.%s.fst"%translation_index)
	sorted_input_fst_path = translation_set_path.replace(".translations",".translation.%s.sorted.fst"%translation_index)
	input_edit_ref_fst_path = translation_set_path.replace(".translations",".inputeditref.%s.fst"%translation_index)
	
	nbest_fst_path = translation_set_path.replace(".translations",".nbest.%s.fst"%translation_index)
	shortest_fst_path = translation_set_path.replace(".translations",".shortest.%s.fst"%translation_index)
	
	result_index = translation_set_path.replace(".translations",".%s"%translation_index)

	#testset fsm vocabulary
	vocab = []
	#first substitute UNK for translation tokens that are not in the syms file
	#with open(syms_file_path,'r') as syms:
	#	 for line in syms.readlines():
	#		 vocab.append(line.split()[0])

	#unked_translation = [x if x in vocab else "<unk>" for x in tokenized_translation]
	#unking is probably unnecesssary, it's easier to see the edits without unks anyway
	unked_translation = tokenized_translation


	#transform translation into a fst with a window of permutation
	if reordering_window == 0:
		translation_fst = create_straight_fst(unked_translation)
	else:
		translation_fst = create_translation_fst(unked_translation,reordering_window,weight,move_limit)
	sorted_translation_fst = sorted(translation_fst,key=lambda x: int(x.split()[0]))
	with open(input_txt_fsm_path,'w') as input_txt:
		for transition in sorted_translation_fst:
			input_txt.write(transition)
	#print("compiling input fst")
	logging.info("Compiling translation fst with alphabet %s"%syms_file_path)
	with open(input_fst_path,'w') as input_fst:
		subprocess.call(["fstcompile","--acceptor","--keep_isymbols=true","--keep_osymbols=true","--isymbols=%s"%syms_file_path,"--osymbols=%s"%syms_file_path,input_txt_fsm_path],stdout=input_fst)

	#print("processing input fst")
	if minimize_input:
		logging.info("Minimizing input")
		with open(determinized_input_fst_path,'w') as determinized_input_fst:
			subprocess.call(["fstdeterminize", input_fst_path],stdout=determinized_input_fst)
		with open(minimized_input_fst_path,'w') as minimized_input_fst:
			subprocess.call(["fstminimize",determinized_input_fst_path],stdout=minimized_input_fst)
		with open(sorted_input_fst_path,'w') as sorted_input_fst:
			subprocess.call(["fstarcsort",minimized_input_fst_path],stdout=sorted_input_fst)
	else:
		with open(sorted_input_fst_path,'w') as sorted_input_fst:
			subprocess.call(["fstarcsort",input_fst_path],stdout=sorted_input_fst)

	"""logging.info("Composing hypothesis and edit")
	
	compiled_input_edit_fst_path = translation_set_path.replace(".translations",".inputedit.%s.fst"%translation_index)
	determinized_input_edit_fst_path = translation_set_path.replace(".translations",".inputedit.determinized.%s.fst"%translation_index)
	minimized_input_edit_fst_path = translation_set_path.replace(".translations",".inputedit.minimized.%s.fst"%translation_index)
	sorted_input_edit_fst_path = translation_set_path.replace(".translations",".inputedit.%s.sorted.fst"%translation_index)
	
	with open(compiled_input_edit_fst_path,'w') as hypedit_fst:
		subprocess.call(["fstcompose", sorted_input_fst_path,compiled_edit_transducer_path],stdout=hypedit_fst)
	with open(determinized_input_edit_fst_path, 'w') as determinized_input_edit_fst:
		subprocess.call(["fstdeterminize", compiled_input_edit_fst_path],stdout=determinized_input_edit_fst)
	with open(minimized_input_edit_fst_path,'w') as minimized_input_edit_fst:
		subprocess.call(["fstminimize",determinized_input_edit_fst_path],stdout=minimized_input_edit_fst)
	with open(sorted_input_edit_fst_path,'w') as sorted_input_edit_fst:
		subprocess.call(["fstarcsort",compiled_input_edit_fst_path],stdout=sorted_input_edit_fst)"""



	with open(shortest_txt_path,'w') as shortest_txt:
		starttime = time.time()
		subprocess.call([scorer_path,sorted_input_fst_path,compiled_edit_transducer_path,sorted_fst_path,nbest_fst_path],stdout=shortest_txt)
		#print("duration: %f"%(time.time()-starttime))

def generate_work_files(work_dir,input_trans_path,index_file_path,source_file_path):
	with open(input_trans_path,'r') as input_trans, open(index_file_path) as index_file, open(source_file_path) as source_file:
		sentence_translations = dict()
		for translation in input_trans:
			(template_index,semvar_index) = index_file.readline().strip().split('\t')
			source = source_file.readline()
			if (template_index,semvar_index) not in sentence_translations:
				sentence_translations[(template_index,semvar_index)] = []
			sentence_translations[(template_index,semvar_index)].append((source,translation))
	
	for (template_index,semvar_index),translation_pairs in sentence_translations.items():
		work_file_name = os.path.join(work_dir,"%s.%s.Fin.translations"%(template_index,semvar_index))
		source_file_name = os.path.join(work_dir,"%s.%s.Eng.strings"%(template_index,semvar_index))
		logging.info("creating work files %s, %s"%(work_file_name,source_file_name))
		with open(work_file_name,'w') as work_file, open(source_file_name,'w') as src_work_file:
			for (source,translation) in translation_pairs:
				work_file.write(translation)
				src_work_file.write(source)

def truecase(translation,ref_tokens):
	translation_tokens = translation.split()
	if translation_tokens[0] in ref_tokens:
		logging.info("Upper case form at start, preserving: %s"%(translation_tokens))	
		return translation
	else:
		translation_tokens[0] = translation_tokens[0].lower() 
		return " ".join(translation_tokens)


def parse_nbest(nbestlister_path,nbest_path,nbest_n=500,thresholds=[0.5,1,1.5]):
	nbest = subprocess.check_output("cat %s | %s %d"%(nbest_path,nbestlister_path,nbest_n),shell=True)	
	nbest_lines = nbest.decode("utf-8").strip().split('\n')
	nbest_by_score = dict()
	for line in nbest_lines:
		score,edited = line.split(' ',1)
		nbest_by_score[float(score)] = edited
	sorted_scores = sorted(nbest_by_score.keys())
	best = sorted_scores[0]
	close_to_best = []
	for threshold in thresholds:
		close_to_best.append([x for x in sorted_scores[1:] if x - best < threshold])
	return (best,[len(x) for x in close_to_best])

def parse_error_counts(total_error_counts):
	errorlist = "\n".join(sorted(["%s: %d"%(k,v) for k,v in total_error_counts.items() if v > 0]))
	return "error list\n%s"%errorlist

def parse_recurring_edits(precise_edits):
	recurring_edits = "\n".join(sorted(["%s: %d"%(k,v) for k,v in precise_edits.items() if v > 5]))
	return "recurring edits\n%s"%recurring_edits
		
def main():
	parser = argparse.ArgumentParser(
		description=("Scores translations against evaluation FSMs."),
		formatter_class=argparse.ArgumentDefaultsHelpFormatter)

	parser.add_argument('-i','--input_translations', dest="input_trans", default="",type=str,\
		help="File with translations")
	parser.add_argument('-e','--test_prefix', dest="test_prefix", default="evalgen_testsets/testset_source.source",type=str,\
		help="Prefix for the source sentences and ids of the test set")
	
	parser.add_argument('-w','--work_dir', dest="trans_dir", default="evalgen_workdir",type=str,help="Dir where translation FSMs are built")
	parser.add_argument('-s','--scorer_path', dest="scorer_path", default="./fst_scoretranslation",type=str,help="Path to the scorer executable")
	parser.add_argument('-b','--nbestlister_path',dest="nbestlister_path", default="../openfst-utils/fst_print_nbest",type=str,help="Path to the executable that lists nbest edits")
	parser.add_argument('-t','--testset_dir', dest="testset_dir", default="evalgen_testsets",type=str,help="The dir where testset is")	
	parser.add_argument('-c','--cpus', dest="cpus", default=1,type=int,help="How many CPUs to use in scoring")
	parser.add_argument(
		'-n','--no_minimize_input', dest="minimize_input", \
		action="store_false",help="Should the input be minimized (disabling this seems to break scoring, so leave on.)")

	parser.add_argument(
		'-r','--random_ref', dest="random_ref", \
		action="store_true",help="Just use one random ref for scoring")
	parser.add_argument(
		'-d','--detokenized_input', dest="detokenized_input", \
		action="store_true",help="Set this if the input is detokenized")
	parser.add_argument(
		'-o','--reord_limit', dest="reord_limit", \
		type=int,default=100,help="The max range a reordering move can performed over.")
	parser.add_argument(
		'-m','--move_limit', dest="move_limit", \
		type=int,default=3,help="Total amount of moves that can be performed.")
	args = parser.parse_args()

	index_file = args.test_prefix + ".ids"
	source_file = args.test_prefix + ".tok"
	


	translation_set_dir = args.trans_dir
	testset_dir = args.testset_dir
	cpus = args.cpus
	minimize_input = args.minimize_input
	random_reference = args.random_ref
	scored_results = []

	

	if args.input_trans and args.test_prefix:
		if os.path.exists(translation_set_dir):
			shutil.rmtree(translation_set_dir)

		os.makedirs(translation_set_dir)
		#generate the translation work files from the input file
		generate_work_files(translation_set_dir,args.input_trans,index_file,source_file)
	else:
		sys.stderr.write("input file not defined")
		sys.exit()
	
	
	edit_weights = {
		"move": 0.7,
		"delete": 0.9,
		"unkdel": 0.89,
		"itdelete": 0.88,
		"insert": 0.8,
		"sub": 1.5,
		"unksub": 1.49,
		"animacy_sub": 0.4,
		"lemma_sub": 0.351,
		"casenum_sub": 0.6,
		"tensenum_sub": 0.74,
		"case_sub": 0.75,
		"comp_lemma_join": 0.352,
		"punct": 0.01
	}


	#reverse the edit weight dict, so that edit types can be identified based on cot
	reverse_edit_weights ={str(v): k for k,v in edit_weights.items()}

	#the order of this list determines the priority of edits, the first are the most expensive (by a fraction), so will be
	#overwritten by later ones. Rarest first is a good principle
	omorfi_cases = reversed(["NOM","PAR","ACC","GEN","INE","ELA","ILL","ADE","ABL","ALL","ESS","TRA","ABE","COM","INS","LAT"])

	case_shift_weight = edit_weights["lemma_sub"]
	for case_shift in [construct_case_shift_key(*x) for x in [y for y in itertools.permutations(omorfi_cases,2) if len(y) == 2]]:
		case_shift_weight -= 0.00001
		edit_weights[case_shift] = case_shift_weight
		trimmed_weight_key = ("%.5f"%case_shift_weight).rstrip("0")
		reverse_edit_weights[trimmed_weight_key] = case_shift
	
	total_error_counts = {k: 0 for k in edit_weights.keys()}
	close_edits_by_error_count = [[] for x in range(100)] #max 100 errors should be plenty
	precise_edits = dict()		
	logform_results = []

	omorfi = Omorfi()
	omorfi.load_from_dir() #if can't find, define (os.environ["OMORFI_PATH"])

	for translation_set_path in [os.path.join(translation_set_dir,x) for x in os.listdir(translation_set_dir) if x.endswith(".translations")]:
		evaluated_translations = dict()
		results = []
		#get the test set file paths
		head,tail = os.path.split(translation_set_path)

		source_strings_path = translation_set_path.replace("Fin.translations","Eng.strings")
		with open(source_strings_path,'r') as source_strings_file:
			source_strings = source_strings_file.readlines()

		syms_file_path = os.path.join(testset_dir,tail.replace(".translations",".syms"))

		#these are omorfi analyses of the ref symbols, used for generating morpho edits
		(ref_tokens,sym_analyses,ref_compounds) = analyse_ref_tokens(syms_file_path,omorfi)

		sorted_fst_path = os.path.join(testset_dir,tail.replace(".translations",".sorted.fst"))
		if random_reference:
			sorted_fst_path = sorted_fst_path.replace(".Fin",".randomsingle.Fin")

		#score each translation
		with open(translation_set_path,'r',encoding="utf8") as translation_set:
			pool = mp.Pool(processes=cpus)
			for (translation_index,translation) in enumerate(translation_set.readlines()):
				source_string = source_strings[translation_index]
				#tokenize, lowercase (add truecasing if proper names added to sets) and remove period
				if args.detokenized_input:
					translation = re.sub(r"([-,.])",r" \1",translation) #hyphen should be really handled in the fst, give different options for breaking up hyphenated words
				translation = truecase(translation,ref_tokens)
				translation = translation.strip()
				translation = translation.strip('. ')
				
				tokenized_translation = translation.split()
				#print(tokenized_translation)

				if translation in evaluated_translations:
					(shortest_path,fst_path) = evaluated_translations[translation]
					results.append((source_string,translation,shortest_path,fst_path))
					continue
				else:
					shortest_txt_path = translation_set_path.replace(".translations",".shortest.%s.txt"%translation_index)
					nbest_fst_path = translation_set_path.replace(".translations",".nbest.%s.fst"%translation_index)

					evaluated_translations[translation] = (shortest_txt_path,nbest_fst_path)

					results.append((source_string,translation,shortest_txt_path,nbest_fst_path))

				#create an edit transducer with morphological subs for the input sentence
				compiled_edit_transducer_path = translation_set_path.replace(".translations",".edit.%s.fst"%translation_index)
				
				#translation needs a syms file that contains the syms that are not in the ref fst but that can be edited to ones in the ref fst with morpho rules.
				translation_syms_path = translation_set_path.replace(".translations",".translation.%s.syms"%translation_index)
				with open(translation_syms_path,'w') as syms_file:
					syms_file.write("<eps> 0\n")
					syms_file.write("<unk> 1\n")
					sym_index = 2
					for translation_sym in set(tokenized_translation):
						syms_file.write("%s %s\n"%(translation_sym,sym_index))
						sym_index+=1

				compound_edits = extract_compound_edits(tokenized_translation,ref_compounds,omorfi)
				#print(compound_edits)
				(morpho_edits,pron_edits) = extract_morpho_edits(tokenized_translation,ref_tokens,sym_analyses,omorfi)
				create_edit_transducer(syms_file_path,translation_syms_path,compiled_edit_transducer_path,tokenized_translation,morpho_edits,pron_edits,compound_edits,edit_weights)

				sys.stderr.write("starting evaluation for %d"%translation_index);
				sys.stderr.flush()
				if (cpus==1):
					evaluate_translation(tokenized_translation,translation_syms_path,translation_index,translation_set_path,testset_dir,minimize_input,random_reference,shortest_txt_path,compiled_edit_transducer_path,sorted_fst_path,args.scorer_path,args.reord_limit,args.move_limit,edit_weights)
				else:
					r = pool.apply_async(evaluate_translation,args=(tokenized_translation,translation_syms_path,translation_index,translation_set_path,testset_dir,minimize_input,random_reference,shortest_txt_path,compiled_edit_transducer_path,sorted_fst_path,args.scorer_path,args.reord_limit,args.move_limit,edit_weights))
				sys.stderr.write("started evaluation for %d"%translation_index);
				sys.stderr.flush()

				
			pool.close()
			pool.join()
	
		logform_cost = 0
		logform_reflen = 0
		for (result_index,(source_string,translation,shortest_txt_path,nbest_fst_path)) in enumerate(results):
			segment_id = "%s.%s"%(source_strings_path,result_index)
			 
			sentence_error_counts = {k: 0 for k in edit_weights.keys()}

			#calculate cost from the shortest path
			with open(shortest_txt_path,'r') as shortest_txt:
				states = dict()
				for line in shortest_txt.readlines():
					split_line = line.split()
					if len(split_line) > 2:
						states[split_line[1]] = split_line
					else:
						#there will be one end state
						end_state = split_line[0]

				ref = []
				edits = []
				cost = 0
				adjusted_cost = 0
				while end_state in states:
					(start,end,input_symb,output_symb,trans_weight) = states[end_state]
					if trans_weight != "0":
						if trans_weight in reverse_edit_weights:
							edit_type = reverse_edit_weights[trans_weight]
							if edit_type != "punct":
								sentence_error_counts[edit_type] += 1
						elif trans_weight.startswith(str(edit_weights["move"])):
							#some corner case where move weight gets a tiny bit bigger than specified,
							#probably because of combination with comma. Other weights start with move weight,
							#but they are handled in the first condition, so won't come here.
							edit_type = "move"
							sentence_error_counts[edit_type] += 1
						else:
							#this means the edit is a combination of a move and something else
							#first check for 5f precision keys, they also match 3f keys so you'll have trouble otherwise
							other_edit_type_weight=str(float("%.5f"%(float(trans_weight)-edit_weights["move"])))
							if other_edit_type_weight not in reverse_edit_weights:
								other_edit_type_weight=str(float("%.3f"%(float(trans_weight)-edit_weights["move"])))
							other_edit_type = reverse_edit_weights[other_edit_type_weight]
							edit_type = "move + %s"%other_edit_type	
							sentence_error_counts[other_edit_type] += 1
							sentence_error_counts["move"] += 1
						precise_edit = "[%s: %s/%s (%s)]"%(edit_type,input_symb,output_symb,trans_weight)
						edits.append(precise_edit)
						if precise_edit in precise_edits:
							precise_edits[precise_edit] += 1
						else:
							precise_edits[precise_edit] = 1

						cost += float(trans_weight)
						#adjusted cost not used, it was supposed to blunt the effects of recurring edits, 
						#but that's not a good solution, as it introduces its own problems (system A always
						#makes the same error, system B makes two different errors in same position. B should
						#not be penalized)
						adjusted_cost += float(trans_weight) * (1/(max(1,math.log(precise_edits[precise_edit],5)+1)))

					elif "<eps>" not in output_symb:
						edits.append(output_symb)
						
					if "<eps>" not in output_symb:#don't record epsilons, they mess up scoring otherwise
						ref.append(output_symb)
					end_state = start
					
				logform_reflen += len(ref)
				logform_cost += cost

				errors = sum(sentence_error_counts.values())
				best,close_to_best_count = parse_nbest(args.nbestlister_path,nbest_fst_path)
				close_edits_by_error_count[errors].append(close_to_best_count)
				for edit_type in sentence_error_counts.keys():
					total_error_counts[edit_type] += sentence_error_counts[edit_type]
				score = cost/len(ref)
				scored_result = (cost,ref,result_index,translation)
				scored_results.append((segment_id,source_string,scored_result))
				sys.stdout.write("segment_id: %s\nsource: %s\nhyp: %s\nedits: %s\nref: %s\nscore: %s (%s/%s)\n"%(segment_id,source_string.strip(),translation.strip()," ".join(reversed(edits))," ".join(reversed(ref)),score,cost,len(ref)))
				sys.stdout.write("sentence errors: %d.best edit path cost: %f close to best: %s\n"%(errors,best,str(close_to_best_count)))
				sys.stdout.write(parse_error_counts(sentence_error_counts)+'\n\n')
				
		logform_results.append((source_string,(logform_reflen,logform_cost,len(results))))

	total_cost = 0
	total_reflen = 0
	for (segment_id,source,(cost,ref,result_index,translation)) in scored_results:
		total_reflen += len(ref)
		total_cost += cost
		
	for (representative_source_string,(logform_reflen,logform_cost,logform_len)) in logform_results:
		sys.stdout.write("\nresults for logical form [%s]: %s (%s/%s)\n"%(representative_source_string,logform_cost/logform_reflen,logform_cost,logform_reflen))
	sys.stdout.write("\nclose edits by error count: %s\n"%(close_edits_by_error_count))
	sys.stdout.write("\ntotal score: %s (%s/%s)\n"%(total_cost/total_reflen,total_cost,total_reflen))
	sys.stdout.write(parse_recurring_edits(precise_edits)+'\n')
	sys.stdout.write(parse_error_counts(total_error_counts)+'\n')

if __name__ == '__main__':
	main()
