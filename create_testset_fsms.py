import subprocess
import sys
import re
import os
import itertools


def create_edit_transducer(syms_path,edit_fst_path):
    alphabet = []

    with open(syms_path,'r') as syms:
        for line in syms.readlines():
            alphabet.append(line.split()[0])

    weight = {
        "delete": 1.0,
        "insert": 1.0,
        "sub": 1.0
    }

    with open(edit_fst_path,'w') as edit_fst:
        # No edit
        for l in alphabet:
            edit_fst.write("0 0 %s %s %.3f\n" % (l, l, 0))

        # Substitutions: input one character, output another
        for l in alphabet:
            for r in alphabet:
                if l is not r and r != "<unk>":
                    edit_fst.write("0 0 %s %s %.3f\n" % (l, r, weight["sub"]))

        # Final state
        edit_fst.write("0")

testset_dir = sys.argv[1]

for txt_fst_path in [os.path.join(testset_dir,x) for x in os.listdir(testset_dir) if x.endswith(".txt")]:
    print("processing %s"%txt_fst_path)
    
    """txt_singlerandom_fst_path = txt_fst_path.replace(".Fin","singlerandom.Fin")
    compiled_singlerandom_fst_path = txt_singlerandom_fst_path.replace(".txt",".compiled.fst")
    sorted_singlerandom_fst_path = compiled_singlerandom_fst_path.replace(".txt",".sorted.fst")"""
    
    syms_file_path = txt_fst_path.replace(".txt",".syms")
    if "randomsingle" in txt_fst_path:
        syms_file_path = syms_file_path.replace("randomsingle.","")
    edit_transducer_path = txt_fst_path.replace(".txt",".edit") 
    compiled_edit_transducer_path = txt_fst_path.replace(".txt",".edit.fst")
    compiled_fst_path = txt_fst_path.replace(".txt",".compiled.fst")
    minimized_fst_path = txt_fst_path.replace(".txt",".minimized.fst")
    sorted_fst_path = txt_fst_path.replace(".txt",".sorted.fst")
    src_variants_path = txt_fst_path.replace(".txt",".strings")
    
    #create the edit transducer
    create_edit_transducer(syms_file_path,edit_transducer_path)
    
    #compile edit transducer
    with open(compiled_edit_transducer_path,'w') as compiled_edit_transducer:
        subprocess.run(["fstcompile","--keep_isymbols=true","--keep_osymbols=true","--isymbols=%s"%syms_file_path,"--osymbols=%s"%syms_file_path,edit_transducer_path],stdout=compiled_edit_transducer)
    
    #compile ref fst
    with open(compiled_fst_path,'w') as compiled_fst:
        subprocess.run(["fstcompile","--acceptor","--keep_isymbols=true","--keep_osymbols=true","--isymbols=%s"%syms_file_path,"--osymbols=%s"%syms_file_path,txt_fst_path],stdout=compiled_fst)
        
    #minimize the ref, it's a very bulky trie
    with open(minimized_fst_path,'w') as minimized_fst:
        subprocess.run(["fstminimize",compiled_fst_path],stdout=minimized_fst)
    
    #sort input edits, may fail otherwise
    with open(sorted_fst_path,'w') as sorted_fst:
        subprocess.run(["fstarcsort",minimized_fst_path],stdout=sorted_fst)
    """
    #compile single random
    with open(compiled_singlerandom_fst_path,'w') as compiled_fst:
        subprocess.run(["fstcompile","--acceptor","--keep_isymbols=true","--keep_osymbols=true","--isymbols=%s"%syms_file_path,"--osymbols=%s"%syms_file_path,txt_singlerandom_fst_path],stdout=compiled_fst)
    
    #sort single random
    with open(sorted_singlerandom_fst_path,'w') as sorted_fst:
        subprocess.run(["fstarcsort",compiled_singlerandom_fst_path],stdout=sorted_fst)
    """
    #generate source sentences (takes time with many options, so only produce the src ones)
    if "Eng" in txt_fst_path:
        with open(src_variants_path,'w') as src_variants:
            subprocess.run(["hfst-fst2strings","--xfst=print-space",sorted_fst_path],stdout=src_variants)