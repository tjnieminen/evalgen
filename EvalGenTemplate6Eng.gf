concrete EvalGenTemplate6Eng of EvalGenTemplate6 = open Prelude,LangEng,ParadigmsEng,SyntaxEng,DictEng,(ResEng = ResEng),ExtraEng,MorphoEng in {
    oper
        metphen_rec : Type = {act_vp : VP;act_np : NP;mov_np : NP; mov_v : V};
        complex_metphen_rec : Type = metphen_rec ** {mov_adv : Adv;mov_a : A;mov_n : N};
        generate_adv_variants_for_geo_and_period_vp : Temp -> VP -> NP -> NP -> S = \temp,vp,g,t ->
            let
                geo_adv : Adv = mkAdv in_Prep g;
                period_adv : Adv = mkAdv for_Prep t;
            in
                variants
                    {
                        mkS temp positivePol (mkCl (mkVP (mkVP vp geo_adv) period_adv));
                        mkS period_adv (mkS temp positivePol (mkCl (mkVP vp geo_adv)));
                        mkS geo_adv (mkS temp positivePol (mkCl (mkVP vp period_adv)))
                        
                        
                    };
                    
        generate_adv_variants_for_geo_and_period_npvp : Temp -> NP -> VP -> NP -> NP -> S = \temp,np,vp,g,t ->
            let
                geo_adv : Adv = mkAdv in_Prep g;
                period_adv : Adv = mkAdv for_Prep t;
            in
                variants
                    {
                        mkS temp positivePol (mkCl np (mkVP (mkVP vp geo_adv) period_adv));
                        mkS period_adv (mkS temp positivePol (mkCl np (mkVP vp geo_adv)));
                        mkS geo_adv (mkS temp positivePol (mkCl np (mkVP vp period_adv)))
                        
                        
                    };
        
        generate_adv_variants_for_period_npvp : Temp -> NP -> VP -> NP -> S = \temp,np,vp,t ->
            let
                adv : Adv = mkAdv for_Prep t;
            in
                variants
                    {
                        mkS temp positivePol (mkCl np (mkVP vp adv));
                        mkS adv (mkS temp positivePol (mkCl np vp))
                    };
        
        generate_adv_variants_for_geo_np : Temp -> NP -> NP -> S = \temp,np,g ->
            let
                adv : Adv = mkAdv in_Prep g;
            in
                variants
                    {
                        mkS temp positivePol (mkCl (mkNP np adv));
                        mkS adv (mkS temp positivePol (mkCl np))
                    };
                
        generate_adv_variants_for_geo_and_period_np : Temp -> NP -> NP -> NP -> S = \temp,np,g,t ->
            let
                geo_adv : Adv = mkAdv in_Prep g;
                period_adv : Adv = mkAdv for_Prep t;
            in
                variants
                    {
                        mkS temp positivePol (mkCl (mkNP (mkNP np period_adv) geo_adv));
                        mkS temp positivePol (mkCl (mkNP (mkNP np geo_adv) period_adv));
                        mkS period_adv (mkS temp positivePol (mkCl (mkNP np geo_adv)));
                        mkS geo_adv (mkS temp positivePol (mkCl (mkNP np period_adv)))
                        
                        
                    };
                
    lincat
        Statement = S;
        TimePeriod = NP;
        TimeUnit = N;
        GeoArea = NP;
        MetPhen = metphen_rec;
        ComplexMetPhen = complex_metphen_rec;
        UndergoVerb = V2;
        PastForm = Temp;

    lin
        Experience = experience_V2;
        See = see_V2;
        Have = have_V2;
        
        SemVar1aWeek = week_N;
        SemVar1bDay = day_N;
        SemVar1cHour = hour_N;
        SemVar2aSeveral timeunit =
            variants
                {
                    mkNP aPl_Det timeunit;
                    mkNP (MorphoEng.mkDeterminer plural "many") timeunit;
                    mkNP (MorphoEng.mkDeterminer plural "several") timeunit;
                    mkNP (MorphoEng.mkDeterminer plural "several") timeunit;
                };
        SemVar3aAlps = mkNP thePl_Det (mkN "Alp"); --easiest to treat the Alps as normal noun, bit complicated to set agreement for plural proper noun
        SemVar3bLapland = mkNP (mkPN "Lapland");
        SemVar3cSiberia = mkNP (mkPN "Siberia");
        SemVar3dAndes = mkNP thePl_Det (mkN "Ande");
        SemVar3eGreenland = mkNP (mkPN "Greenland");
        SemVar3fSweden = mkNP (mkPN "Sweden");
        SemVar4aSnow =
            {
                act_vp = mkVP (mkV "snow");
                act_np = mkNP snowfall_N;
                mov_np = mkNP (mkCN snow_N);
                mov_v = fall_V;
            };
        SemVar4bHeavySnow =
            let
                mov_adv_variant : Adv = mkAdv heavy_A;
            in
                {
                    act_vp = mkVP (mkVP snow_V) mov_adv_variant;
                    act_np = mkNP (mkCN heavy_A snowfall_N);
                    mov_np = mkNP (mkCN snow_N);
                    mov_v = fall_V;
                    mov_adv = mov_adv_variant;
                    mov_a = heavy_A;
                    mov_n = snow_N;
                };
        
        SemVar5aPresentPerfect = mkTemp presentTense anteriorAnt;
        SemVar5bPastPerfect = mkTemp pastTense anteriorAnt;
        SemVar5cSimplePast = mkTemp pastTense simultaneousAnt;
        
        SimplifyComplexMetPhen compl =
            {
                act_vp = compl.act_vp;
                act_np = compl.act_np;
                mov_np = mkNP (mkCN compl.mov_a compl.mov_n);
                mov_v = compl.mov_v
            };
        
        --"it has been snowing in the alps for weeks
        PleonasticItTransitive0Predicate_MetPhen1InsideLocative_GeoArea2Benefactive_TimePeriod3 pform m g t =
            generate_adv_variants_for_geo_and_period_vp pform m.act_vp g t;
                
        --"there has been snowfall in the alps for weeks
        PleonasticThere0Object_MetPhen1InsideLocative_GeoArea2Benefactive_TimePeriod3 pform m g t =
            generate_adv_variants_for_geo_and_period_np pform m.act_np g t;
                
        
        --"there have been weeks of snowfall in the alps
        PleonasticThere0Object_TimePeriod1ObjectOfGenitive_MetPhen1InsideLocative_GeoArea2 pform m g t =
            let
                period_met_np : NP = mkNP t (mkAdv (mkPrep "of") m.act_np); 
            in
                generate_adv_variants_for_geo_np pform period_met_np g;
                
        
        --"the alps have experienced/seen snowfall for weeks
        Transitive0Verb_Undergo1Subject_GeoArea2Object_MetPhen3_Benefactive_TimePeriod4 pform undergo_v2 g m t =
            let
                vp_variant : VP = variants
                    {
                        mkVP undergo_v2 m.act_np;
                        progressiveVP (mkVP undergo_v2 m.act_np)
                    };
            in
                generate_adv_variants_for_period_npvp pform g vp_variant t;
                
        
        --"the alps have experienced/seen/had weeks of snowfall
        Transitive0UndergoVerb1Subject_GeoArea2TimePeriod3ObjectOfGenitive_MetPhen4 pform undergo_v2 g m t =
            let
                period_met_np : NP = mkNP t (mkAdv (mkPrep "of") m.act_np); 
                vp_variant : VP = variants
                    {
                        mkVP undergo_v2 period_met_np;
                        progressiveVP (mkVP undergo_v2 period_met_np)
                    };
            in
                mkS pform positivePol (mkCl g vp_variant);
        
        --"snowfall/snowing has continued for weeks in the alps
        IntransitiveTimeVerb0Subject_MetPhen1Object_MetPhen1InsideLocative_GeoArea2Benefactive_TimePeriod3 pform m g t =
            let
                vp : VP = mkVP (mkV "continue");
            in
                generate_adv_variants_for_geo_and_period_npvp pform m.act_np vp g t;
        
        --"snow has been falling for weeks in the alps
        IntransitiveMovementVerb0Subject_MetPhen1Object_MetPhen1InsideLocative_GeoArea2Benefactive_TimePeriod3 pform m g t =
            let
                vp : VP = mkVP m.mov_v;
                
            in
                generate_adv_variants_for_geo_and_period_npvp pform m.mov_np vp g t;
                --mkS presentTense anteriorAnt positivePol (mkCl m.mov_np (mkVP (mkVP (mkVP m.mov_v) geo_adv) period_adv));
        
        --"heavy snow has been falling for weeks in the alps
        IntransitiveMovementVerb0Subject_ComplexMetPhen1Object_MetPhen1InsideLocative_GeoArea2Benefactive_TimePeriod3 pform m g t =
            let
                vp : VP = mkVP (mkVP m.mov_v) m.mov_adv;
            in
                generate_adv_variants_for_geo_and_period_npvp pform m.mov_np vp g t;

        
}   