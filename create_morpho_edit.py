#!/usr/bin/env python3
import sys
import os
import re
from omorfi.omorfi import Omorfi

class OmorfiAnalysis():
    def __init__(self,analysis_tuple):
        analysis_string = analysis_tuple[0]
        analysis_split = analysis_string.split("]")
        analysis_fields = [x.split("=") for x in analysis_split]
        #print(analysis_fields)
        self.fields = {field[0][1:]: field[1] for field in analysis_fields if len(field) > 1}
        self.lemma = self.fields["WORD_ID"]
        if "CASE" in self.fields:
            self.case = self.fields["CASE"]
        else:
            self.case = None
        if "UPOS" in self.fields:
            self.wordclass = self.fields["UPOS"]
        if "NUM" in self.fields:
            self.num = self.fields["NUM"]
        else:
            self.num = None
        if "BOUNDARY" in self.fields and self.fields["BOUNDARY"] == "COMPOUND":
            self.is_compound = True
        else:
            self.is_compound = False
        #print(self.fields)

    def __str__(self):
        return str(self.fields)
    

def create_edit_transducer(ref_syms_path,edit_fst_path,hyp_tokens,lemma_edits,casenum_edits_case_edits):
    alphabet = []

    with open(ref_syms_path,'r') as syms:
        for line in syms.readlines():
            alphabet.append(line.split()[0])

    weight = {
        "delete": 1.0,
        "insert": 1.0,
        "sub": 1.0,
        "lemma_sub": 0.25,
        "casenum_sub": 0.5,
        "case_sub": 0.75
    }

    with open(edit_fst_path,'w') as edit_fst:
        # No edit
        for l in alphabet:
            edit_fst.write("0 0 %s %s %.3f\n" % (l, l, 0))

        # Substitutions: input one character, output another. Note that epsilon is included in characters, so insertion and deletion is included in this
        for l in hyp_token:
            for r in alphabet:
                if l is not r and r != "<unk>":
                    edit_fst.write("0 0 %s %s %.3f\n" % (l, r, weight["sub"]))

        for lemma_edit in lemma_edits:
            edit_fst.write("0 0 %s %s %.3f\n" % (lemma_edit[0], lemma_edit[1], weight["lemma_sub"]))

        for casenum_edit in casenum_edits:
            edit_fst.write("0 0 %s %s %.3f\n" % (casenum_edit[0], casenum_edit[1], weight["casenum_sub"]))

        for case_edit in case_edits:
            edit_fst.write("0 0 %s %s %.3f\n" % (case_edit[0], case_edit[1], weight["case_sub"]))

        # Final state
        edit_fst.write("0")

sym_path = sys.argv[1]
omorfi = Omorfi()
omorfi.load_from_dir(os.environ["OMORFI_PATH"])

ref_tokens = set()
sym_analyses = []
with open(sym_path,'r') as syms_file:
    for line in syms_file.readlines():
        split_line =  line.split()
        if len(split_line) > 0:
            sym_token = split_line[0]
        else:
            continue
        if sym_token == "<unk>" or sym_token == "<eps>":
            continue
        sym_token_analyses = omorfi.analyse(sym_token)
        ref_tokens.add(sym_token)
        for sym_token_analysis in sym_token_analyses:
            #skip compound analyses, they are mostly junk
            analysis = OmorfiAnalysis(sym_token_analysis)
            if analysis.is_compound:
                continue
            sym_analyses.append((sym_token,analysis))



for translation in sys.stdin:
    translation = re.sub(r"([-,.])",r" \1",translation) #hyphen should be really handled in the fst, give different options for breaking up hyphenated words
    translation = translation.lower()
    translation = translation.strip()
    translation = translation.strip('. ')
    for token in translation.split():
        if token not in ref_tokens:
            #print(token)
            hyp_token_analyses = [OmorfiAnalysis(x) for x in omorfi.analyse(token)]
            print([str(x) for x in hyp_token_analyses])
            #first check if lemma in ref tokens
            lemma_matches = [x for x in sym_analyses if x[1].lemma in [y.lemma for y in hyp_token_analyses]]
            for lemma_match in lemma_matches:
                print("%s\t%s"%(token,lemma_match[0]))

            #for morfo similarity, come up with some sane method, just using case won't do, because many verb forms have case
            case_matches = [x for x in sym_analyses if x[1].case in [y.case for y in hyp_token_analyses if y.case is not None and y.wordclass == x[1].wordclass]]
            print([(x,str(y)) for (x,y) in case_matches])
            for case_match in case_matches:
                print("%s\t%s"%(token,case_match[0]))

            case_num_matches = [x for x in case_matches if x[1].num in [y.num for y in hyp_token_analyses if y.num is not None and y.wordclass == x[1].wordclass]]
            for case_num_match in case_num_matches:
                print("%s\t%s"%(token,case_num_match[0]))

            #print(set(case_matches).intersection(num_matches))
