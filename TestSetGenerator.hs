{-# LANGUAGE OverloadedStrings #-}
module Main where
import PGF
import PGF.Lexing
import PGF.Data
import PGF.Macros
import System.Random
import System.Environment
import System.IO
import qualified Data.Map as Map
import Data.Maybe as Maybe
import Data.List as List
import Control.Monad as Monad
import Data.Array.IArray
import qualified Data.IntMap as IntMap
import qualified Data.Set as Set
import PGFToCFG
import CFGToFA
import qualified FiniteState
import SLF

--from here: https://stackoverflow.com/questions/28003875/haskell-save-unicode-string-characters-to-file
writeFileWithEncoding enc fp content =
  withFile fp WriteMode $ \h -> do
    hSetEncoding h enc
    hPutStr h content
    
data SimpleWordTrie = SimpleWordTrie
    {
        transitions :: Map.Map String SimpleWordTrie
    } 

instance Show SimpleWordTrie where
    show (SimpleWordTrie x) = show (Map.keys x) ++ (show x)

simpleTrieToTxtFsm :: SimpleWordTrie -> ([String],[String])
simpleTrieToTxtFsm trie =
    let
        --the start state is 0 for all children, the end states start at 1
        (syms,txt,_) = List.foldl (simpleTrieToTxtFsm' 0) (Map.fromList [("<eps>",0),("<unk>",1)],[],1) (Map.toList $ transitions trie)
        syms_txt = List.map (\(x,y)-> x ++ " " ++ (show y)) $ Map.toList syms
    in
        (syms_txt,txt)

simpleTrieToTxtFsm' :: Int -> (Map.Map String Int,[String],Int) -> (String,SimpleWordTrie) -> (Map.Map String Int,[String],Int)
simpleTrieToTxtFsm' start_node (syms,txt,end_node) (transition_string,trie) =
    let
        (new_syms,transition_index) =
            case (Map.lookup transition_string syms) of
                Just index -> (syms,index)
                Nothing ->
                    let
                        next_index = Map.size syms
                    in
                        (Map.insert transition_string next_index syms,next_index)
        new_txt = (List.intercalate " " [show start_node,show end_node,transition_string,"0"]):txt
    in
        if Map.null (transitions trie)
        then
            let
                final_state_string = show end_node
            in 
                (new_syms,final_state_string:new_txt,end_node+1)
        else            
            List.foldl (simpleTrieToTxtFsm' end_node) (new_syms,new_txt,end_node+1) (Map.toList $ transitions trie)

addSentenceToSet :: SimpleWordTrie -> String -> SimpleWordTrie
addSentenceToSet wordtrie sentence = addSentenceToSet' (removeTrailingPunct $ bindTok $ words sentence) wordtrie

removeTrailingPunct :: [String] -> [String]
removeTrailingPunct (x:[]) = if x == "," then [] else [x]
removeTrailingPunct (x:xs) = x:(removeTrailingPunct xs)



addSentenceToSet' :: [String] -> SimpleWordTrie -> SimpleWordTrie
addSentenceToSet' (x:xs) wordtrie
    | Map.member x (transitions wordtrie) = SimpleWordTrie (Map.adjust (addSentenceToSet' xs) x (transitions wordtrie))
    | otherwise = SimpleWordTrie (Map.adjust (addSentenceToSet' xs) x $ Map.insert x (SimpleWordTrie Map.empty) (transitions wordtrie))

addSentenceToSet' [] wordtrie = wordtrie

generateFsmForVariant :: (String,Int) -> ([String],[String]) -> IO (String,Int)
generateFsmForVariant (lang,fsm_index) (syms,fsm) = do
    writeFileWithEncoding utf8 ("testsets\\" ++ (intercalate "." [(show fsm_index),lang,"txt"])) $ unlines $ reverse $ fsm
    writeFileWithEncoding utf8 ("testsets\\" ++ (intercalate "." [(show fsm_index),lang,"syms"])) $ unlines $ syms
    return (lang,fsm_index+1)

randomString :: StdGen -> [String] -> String
randomString stdgen strings = strings !! (fst $ randomR (0,(length strings)-1) stdgen)
    
tester = do
    orig_pgf <- readPGF "EvalGen.pgf"
    let pgf = restrictPGF (\x-> True) orig_pgf
    let pgflanguages = languages pgf
    let cnc = lookConcr pgf (head pgflanguages)
    let prods = productions cnc
    let rules = [(fcat,prod) | (fcat,set) <- IntMap.toList (productions cnc), prod <- Set.toList set]
    
    print $ cncfuns cnc
                       
    {-
    let cfg = pgfToCFG pgf (head pgflanguages)
    let fa = cfgToFA' cfg
    let slf = slfPrinter fa
    writeFileWithEncoding utf8 "slftest.txt" $ slf-}
    
main = do
    args <- getArgs
    pgf <- readPGF "EvalGen.pgf"
    
    stdgen <- getStdGen
    --let random_template = head $ generateRandom (stdgen) pgf (startCat pgf)
    let abstract_variants = generateAll pgf (startCat pgf)
    print abstract_variants
    --let random_expr = head $ generateRandomFrom stdgen pgf random_template
    --print random_expr
    let all_lins = List.map (linearizeVariantsAllLang pgf) abstract_variants
    
    --print $ unlines $ snd $ all_lins !! 1
    --print $ unlines $ snd $ head $ all_lins
    
    let tgt_fsms = List.map (simpleTrieToTxtFsm . Prelude.foldl addSentenceToSet (SimpleWordTrie Map.empty) . snd . (\x-> x!!1)) all_lins
    let src_fsms = List.map (simpleTrieToTxtFsm . Prelude.foldl addSentenceToSet (SimpleWordTrie Map.empty) . snd . (\x-> x!!0)) all_lins
    
    let tgt_fsms_randomsingle = List.map (simpleTrieToTxtFsm . addSentenceToSet (SimpleWordTrie Map.empty) . (randomString stdgen) . snd . (\x-> x!!1)) all_lins
    --let src_fsms_randomsingle = List.map (simpleTrieToTxtFsm . addSentenceToSet (SimpleWordTrie Map.empty) . (randomString stdgen) . snd . (\x-> x!!0)) all_lins
    
    Monad.foldM generateFsmForVariant ("Eng",0) src_fsms
    Monad.foldM generateFsmForVariant ("Fin",0) tgt_fsms
    
    --Monad.foldM generateFsmForVariant ("randomsingle.Eng",0) src_fsms_randomsingle
    Monad.foldM generateFsmForVariant ("randomsingle.Fin",0) tgt_fsms_randomsingle
    
    --writeFileWithEncoding utf8 "output.txt" $ unlines $ snd $ head $ all_lins
    
    --let random_story = linearizeRandom pgf (head $ languages pgf) stdgen random_expr
    --let random_story = tabularLinearizesRandom pgf (head $ languages pgf) stdgen random_expr
    --random_story_index <- randomRIO (0,(length random_stories)-1)
    --let random_story = random_stories !! random_story_index
    --print $ random_story