#!/usr/bin/env python3
import sys
import os
import glob
import pgf
import re
import subprocess
import argparse
import logging
import shutil
from nltk.metrics.distance import edit_distance
from random import shuffle,seed

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',filename='randomrefs.log',level=logging.INFO,filemode='w')

def create_refs(ref_count,ids_path,fst_dir):
	refs = []
	with open(ids_path,'r') as id_file:
		for id_row in id_file:
			(template,semvarindex) = id_row.split()
			fst_name = os.path.join(fst_dir,"%s.%s.Fin.minimized.fst"%(template,semvarindex))
			proc = subprocess.Popen(["hfst-fst2strings","-r", str(ref_count),"--xfst","print-space",fst_name],stdout=subprocess.PIPE)
			out,err = proc.communicate()
			randrefs = [x for x in out.decode().split("\n")]
			shuffle(randrefs)
			refs.append(randrefs)
		
	ref_files = [[] for x in range(ref_count)]
	for transrefs in refs:
		for ref_file in ref_files:
			ref_sent=""
			while ref_sent == "":
				ref_sent = transrefs.pop()
			ref_file.append(ref_sent)
			transrefs.insert(0,ref_sent)
	
	return ref_files	
		
def write_ref_files(refs,start,add):	
	ref_file_paths = []
	for index,ref in enumerate(refs[start:start+add]):
		ref_file_path = os.path.join("bleutemp","ref%d"%(start+index))
		ref_file_paths.append(ref_file_path)
		with open(ref_file_path,"w") as reffile:
			for refsent in ref:
				reffile.write(refsent+"\n")
	return ref_file_paths

def write_table(header,systems,filename):
	with open(filename,'w') as tablefile:
		tablefile.write(",".join(header)+"\n")
		for system,scores in systems.items():
			scores.insert(0,system)
			tablefile.write(",".join(scores)+"\n")
			
def score_bleu(hyp,ref_file_paths):
	hypo_path = (os.path.join("bleutemp","hypothesis"))
	with open(hypo_path,'w') as hypfile:
		for sent in hyp:
			hypfile.write(sent)

	proc = subprocess.Popen("/workspace/mosesdecoder/scripts/generic/multi-bleu-detok.perl -lc bleutemp/ref < bleutemp/hypothesis",shell=True,stdout=subprocess.PIPE)
	out,err = proc.communicate()
	return out.decode()

if __name__ == '__main__':
	parser = argparse.ArgumentParser(
		description=("Generates random refs and calculates BLEU for different amount of refs."),
		formatter_class=argparse.ArgumentDefaultsHelpFormatter)

	parser.add_argument('-t','--trans', dest="translation_pattern", default="*trans500",type=str,help="Glob pattern for translation files")
	
	parser.add_argument('-i','--id', dest="id_file", default="evalgen_testsets/testset500.ids",type=str,help="The pattern to the ids file")
	parser.add_argument('-f','--fstdir', dest="fst_dir", default="evalgen_testsets",type=str,help="The dir where the fsts are")
	parser.add_argument('-n','--number', dest="number",default=1,type=int,help="How many refs to generate")
	parser.add_argument('-o','--output', dest="output",default="bleu_graph.data",type=str,help="Name of output file")

	args = parser.parse_args()

	refs = create_refs(args.number,args.id_file,args.fst_dir)
	
	logging.info("Refs created")
	if os.path.isdir("bleutemp"):
		shutil.rmtree("bleutemp")
	os.mkdir("bleutemp")
	
	#get bleu in steps of one ref until ten, then switch to 5 until 50, 10 until 100 and 50 untl 1000
	additional_refs = [1]*10+[5]*10+[10]*10#+[50]*10+[100]*10
	system_lines = dict()	
	count_line = ["system"]
	start = 0	
	for ref_add in additional_refs:
		ref_file_paths = write_ref_files(refs,start,ref_add)
		start += ref_add
		
		logging.info("Amount of refs now %s"%start)
		count_line.append(str(start))
		for translationpath in sorted(glob.glob(args.translation_pattern)):
			if translationpath not in system_lines:
				system_lines[translationpath] = []
			
			logging.info("Processing translation %s"%translationpath)
			bleu = None
			with open(translationpath,'r') as transfile:
				translation = [x for x in transfile.readlines()]
				#NTLK bleu bugged
				bleu_line = score_bleu(translation,ref_file_paths)
				bleu_equals = bleu_line.split(",")[0]
				bleu = bleu_equals.split(" = ")[1]
			
			logging.info("%s scored with %s refs, bleu %s"%(translationpath,start,bleu))
			system_lines[translationpath].append(bleu)

			
			
	write_table(count_line,system_lines,args.output)

