#convert slf format to fsm which can be read by openfst
#Might use pyfst directly? maybe command line tools still more reliable
import subprocess
import sys

ref_file_root = sys.argv[1]
syms_file_path = ref_file_root + ".syms"
ref_fst_path = ref_file_root + ".ref.fst"
sorted_ref_fst_path = ref_file_root + ".sortedref.fst"

#slf nodes, the words are here
nodes = dict()
syms = set()
#slf arcs, these point from node to node
arcs = dict()

with open(sys.argv[1],'r') as slf:
    for line in slf.readlines():
        line_fields = {y[0]: y[1] for y in [x.split("=") for x in line.split()]}
        if "I" in line_fields:
            if line_fields["W"] == "!NULL" and "s" not in line_fields:
                #this is a start or end state
                nodes[line_fields["I"]] = None
            else:
                node_string = line_fields["s"]
                syms.add(node_string)
                nodes[line_fields["I"]] = node_string
        elif "J" in line_fields:
            end = line_fields["E"]
            if end in arcs:
                arcs[end].append(line_fields["S"])
            else:
                arcs[end] = [line_fields["S"]] #index by end, as that's where the transition string will be

#iterate nodes and create all the transitions leading to them, sort later
transitions = []
start_states = []
for node_key in nodes.keys():
    if node_key in arcs:
        for arc_to_node in arcs[node_key]:
            node_string = nodes[node_key]
            if node_string is not None:
                transitions.append("%s %s %s 0"%(arc_to_node,node_key,nodes[node_key]))
            else:
                transitions.append(arc_to_node)
    else:
        start_states.append(node_key)

with open(sys.argv[1]+".txt",'w') as fsm:
    for transition in sorted(transitions):
        fsm.write(transition+"\n")
        
with open(syms_file_path,'w') as syms_file:
    syms_file.write("<eps> 0\n")
    syms_file.write("<unk> 1\n")
    index = 2
    for sym in syms:
        syms_file.write("%s %s\n"%(sym,index))
        index += 1
        
#compile ref fst
with open(ref_fst_path,'w') as ref_fst:
    subprocess.run(["fstcompile","--acceptor","--keep_isymbols","--keep_osymbols","--isymbols=%s"%syms_file_path,"--osymbols=%s"%syms_file_path,sys.argv[1]+".txt"],stdout=ref_fst)

#sort input edits, may fail otherwise
with open(sorted_ref_fst_path,'w') as sorted_fst:
    subprocess.run(["fstarcsort",ref_fst_path],stdout=sorted_fst)