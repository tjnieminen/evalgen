#this will subdivide a grammar into subgrammars consisting of just one TestSentence tree and then convert that into slf format
#for both languages. This is a hack, but doing the fsm generation properly on a per sentence basis would require changes to GF
#source code, since the FSA generation is done at a grammar level.
import subprocess
import sys
import re
import os
import itertools
import shutil

class Transition():
    def __init__(self,source,dest,token,cost):
        self.source = source
        self.dest = dest
        self.token = token
        self.cost = cost
        
    def __str__(self):
        if self.dest is None:
            return self.source
        else:
            return ("%s %s %s %s"%(self.source,self.dest,self.token,self.cost))
        

def slf2fsm(slf_path,test_sent_index,permutation_index,lang):    
    print(slf_path)
    ref_file_root = os.path.join(os.path.split(os.path.split(slf_path)[0])[0],"%s.%s.%s"%(test_sent_index,permutation_index,lang))
    syms_file_path = ref_file_root + ".syms"
    ref_fst_path = ref_file_root + ".ref.fst"
    sorted_ref_fst_path = ref_file_root + ".sortedref.fst"

    #slf nodes, the words are here
    nodes = dict()
    syms = set()
    #slf arcs, these point from node to node
    arcs = dict()

    with open(slf_path,'r') as slf:
        for line in slf.readlines():
            line_fields = {y[0]: y[1] for y in [x.split("=") for x in line.split()]}
            if "I" in line_fields:
                if line_fields["W"] == "!NULL" and "s" not in line_fields:
                    #this is a start or end state
                    nodes[line_fields["I"]] = None
                else:
                    node_string = line_fields["s"]
                    syms.add(node_string)
                    nodes[line_fields["I"]] = node_string
            elif "J" in line_fields:
                end = line_fields["E"]
                if end in arcs:
                    arcs[end].append(line_fields["S"])
                else:
                    arcs[end] = [line_fields["S"]] #index by end, as that's where the transition string will be

    #iterate nodes and create all the transitions leading to them, sort later
    
    transitions = []
    start_states = []
    final_states = []
    
    for node_key in nodes.keys():
        if node_key in arcs:
            for arc_to_node in arcs[node_key]:
                node_string = nodes[node_key]
                if node_string is not None:
                    transitions.append(Transition(arc_to_node,node_key,nodes[node_key],0))
                else:
                    transitions.append(Transition(arc_to_node,None,None,None))
                    final_states.append(arc_to_node)
        else:
            start_states.append(node_key)

    #check if c runtime can be used to generate all strings efficiently. install GF on taito to try this out
            
    #remove trailing commas from the fsm (these are generated, since it's simpler to remove them than to make rules that account for relative clause placement)
    """for comma_transition in [x for x in transitions if x.token == ","]:
        if comma_transition.dest in final_states:
            comma_transition.token = None
            comma_transition.dest = None
            comma_transition.cost = None"""

    #handle bind tokens
    #for each transition with a bind token, get all transitions that lead to the bind token, and all the states that have transitions from the destination of the bind transition
    #then make new transitions between the nodes transiting to and from the bind token
    
    for bind_transition in [x for x in transitions if x.token == "&+"]:
        for transition_leading_to_bind in [x for x in transitions if x.dest == bind_transition.source]:
            for transition_leading_from_bind in [x for x in transitions if x.source == bind_transition.dest]:
                boundtoken = transition_leading_to_bind.token+"BIND"+transition_leading_from_bind.token
                syms.add(boundtoken)
                transitions.append(Transition(transition_leading_to_bind.source,transition_leading_from_bind.dest,boundtoken,0))
        
    with open(ref_file_root+".txt",'w') as fsm:
        for transition in sorted(transitions,key=lambda x: x.source):
            fsm.write(str(transition)+"\n")
            
    with open(syms_file_path,'w') as syms_file:
        syms_file.write("<eps> 0\n")
        syms_file.write("<unk> 1\n")
        index = 2
        for sym in syms:
            syms_file.write("%s %s\n"%(sym,index))
            index += 1
            
            
    #compile ref fst
    with open(ref_fst_path,'w') as ref_fst:
        subprocess.run(["fstcompile","--acceptor","--keep_isymbols=true","--keep_osymbols=true","--isymbols=%s"%syms_file_path,"--osymbols=%s"%syms_file_path,ref_file_root+".txt"],stdout=ref_fst)

    #sort input edits, may fail otherwise
    with open(sorted_ref_fst_path,'w') as sorted_fst:
        subprocess.run(["fstarcsort",ref_fst_path],stdout=sorted_fst)

class AbstractFun():
    def __init__(self,line):
        if "->" in line:
            self.product = ((line.split(":")[1]).split("->")[-1]).strip().strip(";")
            self.arguments = [x.strip() for x in line.split(":")[1].split("->")[:-1]]
        else:
            self.product = (line.split(":")[1]).strip().strip(";")
            self.arguments = []
        
        self.line = line
    
    def __str__(self):
        return "\n".join([str(self.product),str(self.arguments),self.line])

abstract_grammar_path = sys.argv[1]
concretes = [os.path.abspath(x) for x in sys.argv[2:]]

funs = dict()

abstract_template = ""
with open(abstract_grammar_path) as abstract:
    in_fun_section = False
    for line in abstract.readlines():
        if in_fun_section:
            if ":" in line and not line.strip().startswith("--"):
                abfun = AbstractFun(line)
                if abfun.product in funs:
                    funs[abfun.product].append(abfun)
                else:
                    funs[abfun.product] = [abfun]
        else:
            abstract_template += line
        if re.match("^\s*fun\s*$",line):
            in_fun_section = True



for (test_sent_index,test_sent) in enumerate(funs["TestSentence"]):
    #print(test_sent)
    relevant_cats = []
    stack = list(test_sent.arguments)
    while len(stack)>0:
        #print(stack)
        topmost = stack.pop()
        if not topmost in relevant_cats:
            relevant_cats.append(topmost)
        if topmost in funs:
            for fun in funs[topmost]:
                stack.extend(fun.arguments)
    #print([funs[x] for x in relevant_cats if x in funs])
    
    cat_options = [funs[x]+[None] for x in relevant_cats if x in funs] + [[test_sent]]
    #print([[str(y) for y in x] for x in cat_options])
    permutations = itertools.product(*cat_options)
    #print([[str(x) for x in y ] for y in permutations])
    
    #filter invalid permutations
    valid_permutations = []
    for permutation in permutations:
        #filter out Nones
        permutation = [x for x in permutation if x is not None]
        if len(permutation) == 0:
            continue
        permutation_args = list(itertools.chain(*([x.arguments for x in permutation] + [test_sent.arguments])))
        permutation_prods = [x.product for x in permutation]
        if any([x not in permutation_prods for x in permutation_args]):
            continue
        #print(permutation_args)
        #print([str(x) for x in permutation])
        if all([x.product in permutation_args or x.product == "TestSentence" for x in permutation]):
            #print("valid permutation: %s"%([str(x) for x in permutation]))
            
            valid_permutations.append(permutation)
        else:
            #print("invalid permutation: %s"%([str(x) for x in permutation]))
            continue
    
    for (permutation_index,valid_permutation) in enumerate(valid_permutations):
        if not os.path.exists("testsent_dir"):
            os.makedirs("testsent_dir")
        permutation_dir = os.path.join("testsent_dir","%s_%s"%(test_sent_index,permutation_index))
        if not os.path.exists(permutation_dir):
            os.makedirs(permutation_dir)
        
        sent_abs_path = os.path.join(permutation_dir,abstract_grammar_path)
        with open(sent_abs_path,"w") as sent_abs:
            sent_abs.write(abstract_template + "\n".join([x.line for x in valid_permutation]) + "}")
            
        #copy concretes to the same folder
        for concrete in concretes:
            shutil.copy(concrete,permutation_dir)
            subprocess.run(["gf","-i=../../","--make","--output-format=slf","--no-pmcfg","--optimize=all","--optimize-pgf",abstract_grammar_path,concrete],cwd=permutation_dir)
        
        
        for slf_path in [os.path.join(os.path.abspath(permutation_dir),x) for x in os.listdir(permutation_dir) if x.endswith(".slf")]:
            slf2fsm(slf_path,test_sent_index,permutation_index,slf_path.split('.')[0][-3:])
            