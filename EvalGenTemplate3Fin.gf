concrete EvalGenTemplate3Fin of EvalGenTemplate3 = open Prelude,ParamX,(M = MorphoFin),SyntaxFin,ParadigmsFin,DictFin,SentenceFin,NounFin,RelativeFin,(S = StemFin),(R = ResFin),ExtraFin in {
    param
        VerbinessOfNoun = Minen | NoStiAdverbs | Va; --noun derived from verbs have a variabled degree of verb features
        
        NegativeHealthOutcomeParam = NameNp | InchoativeNp;
        AdverbOrder = FreeAdverbial | BackAdverbial;
    oper
        mkPostPartV : V -> Str -> V = \v,s -> {s = \\f => v.s ! f ++ s; sc = v.sc ; lock_V = <> ; h = v.h ; p = v.p} ;
        relative_head_variant : CN =
                    mkCN 
                        (mkN
                            (variants
                                {
                                    ihminen_NK;
                                    henkilo'_NK;
                                }
                            )
                        );
        --the relative constructor in the library slows down linking immensely, this is a simpler version
        simple_relative : NP -> VP -> NP = \np,v->
            lin NP
            {
                s = case np.a of
                    {
                        R.Ag Sg _ => \\npform => np.s ! npform ++ "," ++ "joka" ++ v.s.s ! R.Presn Sg P3 ++ v.adv ! Pos ++ ",";
                        R.Ag Pl _ => \\npform => np.s ! npform ++ "," ++ "jotka" ++ v.s.s ! R.Presn Pl P3 ++ v.adv ! Pos ++ ",";
                        _ => np.s
                    };
                a=np.a;
                isPron=np.isPron;
                isNeg=np.isNeg;
            };
        
        negative_health_np : Type = {np : NP;neg_param : NegativeHealthOutcomeParam; compound_prefix : Str};
        verby_noun : Type = {verbiness : VerbinessOfNoun;cn : CN;v : V};
        effort_rec : Type = {soa : NP;effort : V;effort_nom : NP};
        --got an error when trying to extract complement from VP, so pass it as an argument
        mkN2IsPre : N -> Prep -> Bool -> N2 = \n,c,p -> n ** {c2 = c ; isPre = p ; lock_N2 = <>} ; --normal N2 constructor always places adv last, this has switch
        
        nominalize : V -> NP -> Case -> CN = \vpi,comp,cas->
            let
                infnom : N = mkN vpi;
                n2 : N2 = mkN2IsPre infnom (casePrep cas) False;
            in
                mkCN (mkCN n2 comp) (ParadigmsFin.mkAdv vpi.p);--the adv is a potential verb particle, it won't mostly exist                    
            
        --this version takes an AP, it's for cases where the AP can be on either side of fronted complement
        nominalizeWithAP : V -> NP -> Case -> AP -> CN = \vpi,comp,cas,ap->
            let
                infnom : N = mkN vpi;
            in
                variants
                {
                    --mkCN (mkCN ap (mkCN (mkN2IsPre infnom (casePrep cas) False) comp)) (mkAdv vpi.p); --the adv is a potential verb particle, it won't mostly exist
                    --Need an mkCN constructor that adds the AP in front of the head, so N2 -> AP -> NP, "väliensä julkisesta korjaamisesta"
                    mkCN (mkCNN2AP (mkN2IsPre infnom (casePrep cas) False) comp ap) (ParadigmsFin.mkAdv vpi.p);
                    --.mkCN (mkAdvCNReverse (SyntaxFin.mkAdv (casePrep cas) comp) (mkCN ap infnom)) (mkAdv vpi.p);
                };
                
        
        mkSSubjReverse : Subj -> S -> S -> S = \subj,b,a-> lin S {s = subj.s ++ b.s ++ "," ++ a.s};
        mkAdvCNReverse : Adv -> CN -> CN = \ad,cn-> lin CN {s = \\nf => ad.s ++ cn.s ! nf ; h = cn.h};
        
        mkSPostAdv : S -> Adv -> S = \s,a-> lin S {s = s.s ++ a.s};
        
        --this is an adverbial clause containing a subjunctive, e.g. "siitä huolimatta, että X"
        mkAdvSubj : Str -> Subj -> S -> Adv = \head,subj,s -> lin Adv {
            s = head ++ "," ++ subj.s ++ s.s ++ ",";
        };
        possessiveSuffixPron : {s : ResFin.NPForm => Str ; a : ResFin.Agr ; hasPoss : Bool ; poss : Str} = 
            {s = table {_=>[]};a = ResFin.Ag Pl P3; hasPoss = True; poss = []};
        
        vpToAdv : R.InfForm -> VP -> Adv = \inff,vp-> lin Adv {
            s = vp.s.s ! R.Inf inff ++ vp.s2 ! False ! ParamX.Pos ! ResFin.Ag Sg P3 ++ vp.adv ! ParamX.Pos;
        };
        
        predVPVariants : AdverbOrder -> NP -> VP -> Cl = \advorder,np,vp-> mkClauseVariants advorder (orB np.isNeg vp.vptyp.isNeg) (R.subjForm np vp.s.sc) np.a vp;
        
        --This is mkClause with a possibility to define restrictions on phrase order variants
        mkClauseVariants : AdverbOrder -> Bool -> (Polarity -> Str) -> R.Agr -> VP -> R.Clause =
            \advorder,isNeg,sub,agr,vp -> {
              s = \\t,a,b => 
                 let
                   pol = case isNeg of {
                    True => Neg ; 
                    _ => b
                    } ; 
                   c = (S.mkClausePlus sub agr vp).s ! t ! a ! pol 
                 in 
                 table {
                   SDecl  => case advorder of
                        {
                            FreeAdverbial =>  variants
                                {
                                    c.subj ++ c.adv ++ c.fin ++ c.inf ++ c.compl ++ c.ext;
                                    c.subj ++ c.fin ++ c.inf ++ c.compl ++ c.adv ++ c.ext;
                                    c.subj ++ c.fin ++ c.inf ++ c.adv ++ c.compl ++ c.ext;
                                };
                            BackAdverbial => c.subj ++ c.fin ++ c.inf ++ c.compl ++ c.adv ++ c.ext
                        };
                   SQuest => c.fin ++ BIND ++ R.questPart c.h ++ c.subj ++ c.inf ++ c.compl ++ c.adv ++ c.ext
                   }
              };
        
        --this attaches the ap between the two complements of the n2 (normally adjective is attached in front of both)
        mkCNN2AP : N2 -> NP -> AP -> CN = \f,x,ap-> lin CN {
            s = \\nf => preOrPost f.isPre (ap.s ! True ! nf ++ (S.snoun2nounSep f).s ! nf) (R.appCompl True Pos f.c2 x) ;
            h = f.h } ;
        
                
    lincat
        TestSentence = S;
        Parties = NP;
        PositiveAct = {np: NP; vp: VP};
        DiscordantRelation = {subj : NP; s : S; pron_s : S;subj_pron : NP};
        Country = NP;
        SerialCommaType = Str;
        
    lin
        DespitePositiveAct posact discord =
            let
                mkSImperfectOrPerfect : Cl -> S = variants {\cl-> mkS presentTense anteriorAnt cl;\cl-> mkS pastTense simultaneousAnt cl};
            in
                variants
                {
                    let
                        --discord_s : S = mkS presentTense (mkCl discord.subj discord.vp);
                        adv_variant : Adv = variants
                            {
                                SyntaxFin.mkAdv (postPrep elative "huolimatta") posact.np;
                                SyntaxFin.mkAdv (prePrep elative "huolimatta") posact.np;
                            };
                    in
                        variants
                            {
                                mkS adv_variant discord.s;
                                mkSPostAdv discord.s adv_variant;
                            };
                    let
                        adv_head = variants {"huolimatta siitä";"siitä huolimatta"};
                    in
                        mkS (mkAdvSubj adv_head (mkSubj "että") (mkSImperfectOrPerfect (mkCl discord.subj posact.vp))) discord.pron_s;
                    --mkS (SyntaxFin.mkAdv (prePrep elative "huolimatta") (mkNPSubj it_NP (mkSubj "että") (mkS pastTense (mkCl discord.subj posact.vp)))) discord.pron_s;
                    
                    variants
                        {
                            mkSSubjReverse (mkSubj "vaikka") (mkSImperfectOrPerfect (mkCl discord.subj posact.vp)) discord.pron_s;
                            SSubjS discord.s (mkSubj "vaikka") (mkSImperfectOrPerfect (mkCl discord.subj_pron posact.vp));
                        };
                    
                    mkS (mkConj "mutta") (mkSImperfectOrPerfect (mkCl discord.subj posact.vp)) discord.pron_s;
                    
                };
        
        PublicReconciliation =
            let
                a_variant : A = mkA (julkinen_AK);
            in
                variants
                {
                    let
                        n : N = variants {mkN va'li_NK;mkN suhde_NK};
                        np : NP = mkNP (SyntaxFin.mkDet possessiveSuffixPron pluralNum) (mkCN n);
                        restore_vk : VK = variants {korjata_VK;palauttaa_VK};
                        v : V = mkV restore_vk;
                        v2 : V2 = mkV2 v;
                        vp_adv : VP = mkVP (mkVP v2 (mkNP possessiveSuffixPron n)) (SyntaxFin.mkAdv a_variant);
                        vp_variant = variants {vp_adv};
                        np_nom_ap : NP = mkNP (nominalizeWithAP v np genitive (mkAP a_variant));
                        np_nom : NP = mkNP (mkCN a_variant (nominalize v np genitive));
                        np_variant : NP = variants {np_nom_ap;np_nom};
                    in
                        {np = np_variant; vp = vp_variant};
                
                    let
                        n : N = variants {mkN sopu_NK;mkN sovinto_NK};
                        np : NP = mkNP (mkCN a_variant n);
                        v : V = mkV pa'a'sta'_1_VK;
                        v2 : V2 = mkV2 v illative;
                        vp_a : VP = mkVP v2 np;
                        vp_adv : VP = mkVP (mkVP v2 (mkNP n)) (SyntaxFin.mkAdv a_variant);
                        vp_variant = variants {vp_adv;vp_a};
                        np_nom = mkNP (nominalize v np illative);
                        np_variant : NP = variants {np;np_nom};
                    in
                        {np = np_variant; vp = vp_variant};
                        
                    --"sovinnonteko" variant, this nominalization is kind of non-productive, so specify it separately
                    {np = mkNP (mkCN a_variant (mkN ((mkN sovinto_NK).s ! (R.NCase Sg R.Gen)) (mkN teko_NK))); vp = mkVP (mkV2 (mkV tehda'_VK)) (mkNP (mkCN a_variant (mkN sovinto_NK)))};
                
                    --another variant might be "päästä sopuun/sovintoon" or "välit ovat parantuneet / välien parantumisesta huolimatta"
                    let
                        n : N = variants {mkN sopu_NK;mkN sovinto_NK};
                        np : NP = mkNP (mkCN a_variant n);
                        v : V = mkV tehda'_VK;
                        v2 : V2 = mkV2 v;
                        vp_a : VP = mkVP v2 np;
                        vp_adv : VP = mkVP (mkVP v2 (mkNP n)) (SyntaxFin.mkAdv a_variant);
                        vp_variant = variants {vp_adv;vp_a};
                        np_nom = mkNP (nominalize v np genitive);
                        np_variant : NP = variants {np;np_nom};
                    in
                        {np = np_variant; vp = vp_variant};
                };
            
        --DiscordantRelationWrap discord = discord.s;
        HaveDifferences parties =
            let
                part_verb : V = caseV adessive (variants {mkPostPartV olla_V (variants {"vielä";"kuitenkin";"edelleen";"edelleenkin";"vieläkin";"yhä"})});
                significant_A : A = mkA (variants {suuri_AK;merkitta'va'_AK;huomattava_AK});
                sig_dif_cn : CN = mkCN significant_A (mkN "erimielisyys");
            in
                variants
                {
                    let
                        subj_pron = mkNP ne_Pron;
                        s : S = mkS presentTense (mkCl parties (mkVP (mkV2 (caseV adessive part_verb) partitive) (mkNP aPl_Det sig_dif_cn)));
                        pron_s : S = mkS presentTense (mkCl subj_pron (mkVP (mkV2 (caseV adessive part_verb) partitive) (mkNP aPl_Det sig_dif_cn)));
                    in
                        {s = s; pron_s = pron_s;subj = parties;subj_pron = mkNP ne_Pron};
                        --{vp = mkVP (mkV2 (caseV adessive part_verb) partitive) (mkNP aPl_Det (mkN "erimielisyys"));subj = parties;subj_pron = subj_pron};
                    --"X välillä on edelleen erimielisyyksiä"
                    let
                        subj_pron : NP = mkNP ne_Pron (mkN va'li_NK);
                        between_n2 : N2 = mkN2 (mkN va'li_NK);
                        s : S = mkS presentTense (mkCl (mkNP (mkCN between_n2 (parties))) (mkVP (mkV2 (caseV adessive part_verb) partitive) (mkNP aPl_Det sig_dif_cn)));
                        pron_s : S = mkS presentTense (mkCl subj_pron (mkVP (mkV2 (caseV adessive part_verb) partitive) (mkNP aPl_Det sig_dif_cn)));
                    in
                        {s = s; pron_s = pron_s;subj = parties;subj_pron = mkNP ne_Pron};
                };
                
        SemVar1aTwoCountries =
            let
                country_N : N = variants {mkN maa_NK;mkN valtio_NK};
                quant_variant = this_Quant;
            in
                variants
                    {
                        mkNP quant_variant (mkNum "2") country_N;
                        
                    };
                    
        SemVar2bTurkey = mkNP (mkPN (mkN "Turkki"));
        SemVar2aRussia = mkNP (mkPN (mkN "Venäjä"));
        SemVar2cUkraine = mkNP (mkPN (mkN "Ukraina"));
        SemVar2dChina = mkNP (mkPN (mkN "Kiina"));
        SemVar2eMongolia = mkNP (mkPN (mkN "Mongolia"));
        SemVar2fArmenia = mkNP (mkPN (mkN "Armenia"));
        
        SemVar1bTwoSpecificCountries c1 c2 = 
            let
                bare_NP : NP = mkNP and_Conj c1 c2;
                country_N : N = variants {mkN valtio_NK;mkN hallitus_NK};
            in variants
            {
                mkNP (SyntaxFin.mkDet (GenNP bare_NP) pluralNum) country_N;
                bare_NP
            };
        
        SemVar1cThreeSpecificCountries c1 c2 c3 _ = 
            let
                bare_NP : NP = mkNP and_Conj (mkListNP c1 (mkListNP c2 c3));
                country_N : N = variants {mkN valtio_NK;mkN hallitus_NK};
            in variants
            {
                mkNP (SyntaxFin.mkDet (GenNP bare_NP) pluralNum) country_N;
                bare_NP
            };
        
        Contrast1aSerialComma = "";
        Contrast1bNonSerialComma = "";
}