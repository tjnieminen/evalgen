#!/usr/bin/env python3
import sys
import os
import glob
import pgf
import re
import subprocess
import argparse
import logging
from nltk.metrics.distance import edit_distance
from random import shuffle,seed

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',filename='evalgen.log',level=logging.INFO,filemode='w')

class Transition():
	def __init__(self,source,dest,token,cost):
		self.source = source
		self.dest = dest
		self.token = token
		self.cost = cost
		
	def __str__(self):
		if self.dest is None:
			return str(self.source)+'\n'
		else:
			return ("%s %s %s %s\n"%(self.source,self.dest,self.token,self.cost))

def compile_fsm(txt_fst_path,syms_file_path):
	
	print("processing %s"%txt_fst_path)    
	
	edit_transducer_path = txt_fst_path.replace(".txt",".edit") 
	compiled_edit_transducer_path = txt_fst_path.replace(".txt",".edit.fst")
	compiled_fst_path = txt_fst_path.replace(".txt",".compiled.fst")
	minimized_fst_path = txt_fst_path.replace(".txt",".minimized.fst")
	sorted_fst_path = txt_fst_path.replace(".txt",".sorted.fst")
	src_variants_path = txt_fst_path.replace(".txt",".strings")


	#compile ref fst
	with open(compiled_fst_path,'w') as compiled_fst:
		subprocess.run(["fstcompile","--acceptor","--keep_isymbols=true","--keep_osymbols=true","--isymbols=%s"%syms_file_path,"--osymbols=%s"%syms_file_path,txt_fst_path],stdout=compiled_fst)

	#minimize the ref, it's a very bulky trie
	with open(minimized_fst_path,'w') as minimized_fst:
		subprocess.run(["fstminimize",compiled_fst_path],stdout=minimized_fst)

	#sort input edits, may fail otherwise
	with open(sorted_fst_path,'w') as sorted_fst:
		subprocess.run(["fstarcsort",minimized_fst_path],stdout=sorted_fst)












def get_semvar_list(tree):
	#stack contains pgf objects, they are unpacked and child pgf objects added to stack

	semvarlist = []
	stack = [tree]
	while len(stack) != 0:
		topmost = stack.pop().unpack()
		if isinstance(topmost,tuple):
			(headfun,children) = topmost
			stack.extend(children)
		else:
			headfun = topmost

		semvar_match = re.search(r"^SemVar\d+[a-z]",str(headfun))
		if semvar_match:
			semvarlist.append(semvar_match.group(0))

	return semvarlist

class SemVarSet():
	def __init__(self,index,languages,semvar_members):
		self.index = index
		self.tries = dict()
		self.nodecount = dict()
		self.finalstates = dict()
		self.gf_trees = []
		self.members = semvar_members
		
		self.strings = dict()
		
		for language in languages:
			self.finalstates[language] = set()
			self.tries[language] = [dict()]
			self.strings[language] = set()
			self.nodecount[language] = 0

			

def check_for_match(trie,string):
	tokenized_string = string.split()
	current_state = 0
	for token in tokenized_string:
		if token not in trie[current_state]:
			logging.info("Trie doesn't continue with token %s"%token)
			return False
		else:
			logging.info("Trie continues with token %s"%token)
			current_state = trie[current_state][token].dest
	return True

def process_linearizations(all_lins,current_semvarset,lang_code,string_file):
	count = 0	
	for sentence in all_lins:	
		if count%10000==0:
			logging.info("Processing lin number %s"%str(count))
		#this somehow generates string representation of some functions, skip those
		if '[' in sentence or ']' in sentence:
			continue

		#some syntax forms are marked as forbidden (easier to prune them than to define the grammar to exclude them)
		if sentence.startswith("FORBIDDEN"):
			continue
		#print(sentence)
		#if sentence in generated_sentences:
		#	continue
		#generated_sentences.add(sentence)

		if string_file is not None:
			string_file.write(sentence + '\n')

		states = current_semvarset.tries[lang_code]
		tokenized = sentence.split()
		
		#if "aioimme ostaa kasettisoittimen" in sentence:
		#	logging.info(sentence)
		#	logging.info(tokenized)
		
		current_state_index = 0
		for token in tokenized:
			current_state = states[current_state_index]
			if token in current_state:
				current_state_index = current_state[token].dest
			else:
				current_state[token] = Transition(current_state_index,len(states),token,0)
				current_state_index = len(states)
				states.append(dict())	
				if len(current_semvarset.tries[lang_code]) % 1000 == 0:
					logging.info("Nodecount: %s"%str(len(current_semvarset.tries[lang_code])))
					logging.info("Trie root: %s"%str(current_semvarset.tries[lang_code][0]))

		#add the current state index to final states if not present already
		if current_state_index not in current_semvarset.finalstates[lang_code]:
			current_semvarset.finalstates[lang_code].add(current_state_index)
		count += 1

	return count

def group_trees_by_semvarset(gr):
	
	all_trees = gr.generateAll(gr.startCat)
	logging.info("Sorting trees by semvarset")
	semvarsets = dict()
	
	for (prob,tree) in all_trees:
		#get semantic variant set in tree

		logging.info("Identifying semvarset for tree %s"%str(tree))
		semvarlist = get_semvar_list(tree)
		tree_semvarset = frozenset(semvarlist)
		
		#semvarset should not contain duplicate values, since these are usually indicative of
		#conjunctions, if you allow them you get "Mongolia and Mongolia" etc.
		duplicate_semvars = len(semvarlist) > len(tree_semvarset)
		if (duplicate_semvars):
			logging.info("Semvarset %s contains duplicates, skipping"%semvarlist)
			continue
		if tree_semvarset not in semvarsets:
			semvarsets[tree_semvarset]=SemVarSet(len(semvarsets),[x[-3:] for x in gr.languages],tree_semvarset)
			logging.info("Creating new semvarset %s"%tree_semvarset)

		semvarsets[tree_semvarset].gf_trees.append(tree)
		logging.info("Tree belongs to semvarset %s"%tree_semvarset)

	return semvarsets.values()
	

def generate_template_testset(template_grammar_path,template_index):
	logging.info("Reading pgf %s"%template_grammar_path)
	gr = pgf.readPGF(template_grammar_path)
	
	logging.info("Generating all trees for %s"%template_grammar_path)
	semvarsets = group_trees_by_semvarset(gr)

	total_sents = dict()
	for language in gr.languages:
		total_sents[language[-3:]] = dict()
	#you need to create source strings and target FSM files for each combo of 
	#semantic variants		
	for semvarset in semvarsets:
		logging.info("Generating test set for semvarset %s"%str(semvarset.members))
		for lang_code in semvarset.strings.keys():
			total_sents[lang_code][semvarset.members] = 0
		for tree in semvarset.gf_trees:
			
			logging.info("Generating linearization for tree %s"%str(tree))
			for language in gr.languages:
				logging.info("Processing language %s"%language)
				lang = gr.languages[language]
				lang_code = language[-3:]
				all_lins = lang.linearizeAll(tree)
				#record string for source lang only
				if lang_code == "Eng":
					string_file_path = os.path.join(testsetdir,"%s.%s.%s.strings"%(template_index,semvarset.index,lang_code))
					with open(string_file_path,'a+') as string_file:
						total_sents[lang_code][semvarset.members] += process_linearizations(all_lins,semvarset,lang_code,string_file)
				else:	
					total_sents[lang_code][semvarset.members] += process_linearizations(all_lins,semvarset,lang_code,None)	

						
		logging.info("Processing semvarset %s"%semvarset.members)
		for lang_code in semvarset.strings.keys():
			tree_fsm_path = os.path.join(testsetdir,"%s.%s.%s.txt"%(template_index,semvarset.index,lang_code))
			syms_file_path = tree_fsm_path.replace(".txt",".syms")
			
			#check_for_match(semvarset.tries[lang_code],"aioimme ostaa kasettisoittimen kaupasta mutta niitä ei myyty")			

			syms = set()
			with open(tree_fsm_path,'w') as txt_fsm, open(syms_file_path,'w') as syms_file:
				syms_file.write("<eps> 0\n")
				syms_file.write("<unk> 1\n")
				syms.add("<eps>")
				syms.add("<unk>")
				for (state_index,state) in enumerate(semvarset.tries[lang_code]):	
					for transition in state.values():
						if transition.token not in syms:	
							syms_file.write("%s %s\n"%(transition.token,len(syms)))
							syms.add(transition.token)
						txt_fsm.write(str(transition))
				for final_state_index in semvarset.finalstates[lang_code]:
					txt_fsm.write(str(Transition(final_state_index,None,None,None)))
			
			logging.info("Compiling fsm %s"%tree_fsm_path)
			compile_fsm(tree_fsm_path,syms_file_path)
		del semvarset.tries
		del semvarset.strings

	return total_sents

def format_sent_counts(counts,template_name):
	summary = "Sentence count summary for template %s\n"%template_name
	for lang in counts:
		summary += "Counts for language %s:\n"%lang
		total_sents = sum([v for (k,v) in counts[lang].items()])
		summary += "Total sentences: %d\n"%(total_sents)
		for (semvarset,count) in counts[lang].items():
			summary += "Semvar set %s: %d\n"%(str(semvarset),count)
		summary += "\n"
	return(summary)	

if __name__ == '__main__':
	parser = argparse.ArgumentParser(
		description=("Generates the source sentences and validation FSMs for a grammar."),
		formatter_class=argparse.ArgumentDefaultsHelpFormatter)

	parser.add_argument('-p','--pgf', dest="pgf_pattern", default="EvalGenTemplate*.pgf",type=str,help="Path of the PGF grammar")
	
	parser.add_argument('-t','--testset_dir', dest="testset_dir", default="evalgen_testsets",type=str,help="The dir where testset is created")

	args = parser.parse_args()

	testsetdir = args.testset_dir
	count_file_path = os.path.join(testsetdir,"template_sent_counts")
	if os.path.exists(count_file_path):
		os.remove(count_file_path)
	if not os.path.exists(testsetdir):
		os.makedirs(testsetdir)
	else:
		#remove string files, they'll be append/created later
		for f in os.listdir(testsetdir):
			if re.match(r"\d+\.\d+\.Eng.strings",f):
				os.remove(os.path.join(testsetdir,f))
			

	#each grammar file contains a grammar for a single template (which may have many different trees)
	#enumeration is started from 1, as that's how the pgf's are numbered
	for (template_index,template_grammar_path) in enumerate(sorted(glob.glob(args.pgf_pattern)),1):
		logging.info("Processing template %s"%template_index)
		template_sent_counts = generate_template_testset(template_grammar_path,template_index)
				
		with open(count_file_path,'a+') as count_file:
			summary = format_sent_counts(template_sent_counts,template_grammar_path)
			count_file.write(summary)

