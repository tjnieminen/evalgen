concrete EvalGenTemplate5Eng of EvalGenTemplate5 = open Prelude,LangEng,ParadigmsEng,SyntaxEng,DictEng,(ResEng = ResEng),ExtraEng in {
    param
        AllowGen = GenAllowed | GenForbidden; --Need to block some generation with genetive
    
    oper
        genCN : CN -> CN -> CN = \n1,n2-> lin CN {s = \\num,cas => (n1.s ! num ! ResEng.Gen) ++ (n2.s ! num ! cas); g = n2.g};
    lincat
        Statement = S;
        BasePhysicalObject = CN;
        PhysicalObject = {cn : CN;allow_gen : AllowGen};
        PropertyType = CN;
        PhysicalProperty = A;
        Location = CN;
        LocationRelation = Prep;
        

    lin
        
        SemVar1aBottle = mkCN (mkN "bottle");
        SemVar1bCup = mkCN (mkN "cup");
        SemVar1cPlate = mkCN (mkN "plate");
        SemVar1dMug = mkCN (mkN "mug");
        SemVar2aBarePhysicalObject o = {cn = o;allow_gen = GenAllowed};
        SemVar2bPhysicalObjectWithLocationAdverb o l rel = {cn = mkCN o (mkAdv rel (mkNP the_Det l));allow_gen = GenForbidden};
        SemVar2bPhysicalObjectWithLocationRelative o l rel =
            let
                relative_head : RP = variants {which_RP;that_RP}
            in
                {cn = mkCN o (mkRS (mkRCl relative_head (mkAdv rel (mkNP the_Det l))));allow_gen = GenForbidden};
        
        SemVar3aOnRelation = variants {on_Prep;mkPrep "on top of";mkPrep "on the top of"};
        SemVar3aOnRelationWithVerb =
            let
                prep_variant : Str = variants {"on";"on top of";"on the top of"};
                verb_variant : Str = variants {"sitting";"placed";"located"};
            in
                mkPrep (verb_variant ++ prep_variant);
        
        SemVar4aDiningTable = variants
        {
            mkCN (mkN "dining" (mkN "table"));
            mkCN (mkN "dinner" (mkN "table"))
        };
        SemVar4bSilverTray = variants
        {
            mkCN (mkN "silver" tray_N);
        };
        Color = mkCN (mkN "color");
        SemVar5aRed = mkA "red";
        SemVar5bYellow = mkA "yellow";
        SemVar5cBlue = mkA "blue";
        SemVar5dGreen = mkA "green";
        SemVar5eBlack = mkA "black";
        SemVar5fWhite = mkA "white";
        
        Subject_PhysObject1Predicative_Property2 k o p = mkS (mkCl (mkNP the_Det o.cn) p);
        Subject_PropertyName_PropertyNamePossessive_PhysObject1Predicative_Property2 k o p = mkS (mkCl (mkNP the_Det (mkCN (mkN2 k) (mkNP the_Det o.cn))) p);
        --Subject_PhysObjectGenitive_PropertyName1Predicative_Property2 k o p = mkS (mkCl (mkNP the_Det (genCN o.cn k)) p);
        Subject_PhysObjectGenitive_PropertyName1Predicative_Property2 k o p =
            table
                {
                    GenAllowed => mkS (mkCl (mkNP (GenNP (mkNP the_Det o.cn)) k) p);
                    GenForbidden => variants {}
                } ! o.allow_gen;
        Subject_PhysObject1Predicative_Property_PropertyNameInsideLocative2 k o p = mkS (mkCl (mkNP the_Det o.cn) (mkAP (mkA2 p in_Prep) (mkNP k)));
        
        --"pallo on väriltään punainen, only for Fin
        --Subject_PhysObject1Predicative_Property_PropertyNameOutsideSource2;
}   