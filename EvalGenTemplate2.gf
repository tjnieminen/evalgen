--# -coding=UTF-8
abstract EvalGenTemplate2 = {
    flags
        startcat = TestSentence;
    cat
        TestSentence;
        Effort;
        Difficulty;
        HighDifficulty;
        LowDifficulty;
        StateOfAffairs;
    fun
        EffortIsOfDifficulty : Effort -> Difficulty -> TestSentence;
        Trust : StateOfAffairs;
        Rebuilding : StateOfAffairs -> Effort;
        SemVar1bHighDifficultyWrap : HighDifficulty -> Difficulty;
        SemVar1cLowDifficultyWrap : LowDifficulty -> Difficulty;
        VeryHard : HighDifficulty;
        VeryEasy : LowDifficulty;
        SemVar1aImpossibleApposition : HighDifficulty -> Difficulty;
        
    }