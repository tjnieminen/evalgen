--# -coding=UTF-8
abstract EvalGen = {
    flags
        startcat = TestSentence;
    cat
        TestSentence;
        Effort;
        Difficulty;
        HighDifficulty;
        LowDifficulty;
        StateOfAffairs;
        Parties;
        PositiveAct;
        DiscordantRelation;
        BigEventKind;
        DelicateEventKind;
        WrappedEventKind;
        PositiveHealthAction;
        WrappedNegativeHealthOutcome;
        OutcomeType;
        NegativeHealthOutcome1;
        NegativeHealthOutcome2;
        Periodicality;
        Activity;
    fun
        SingleNegativeHealthEffect2 : NegativeHealthOutcome2 -> WrappedNegativeHealthOutcome;

        Depression : NegativeHealthOutcome2;

        RegularPeriodicalActivity : Activity -> PositiveHealthAction;

        Exercise : Activity;

        ActionReducesNegativeEffectRisk : PositiveHealthAction -> WrappedNegativeHealthOutcome -> TestSentence;
}