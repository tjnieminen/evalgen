concrete EvalGenEng of EvalGen = open Prelude,LangEng,ParadigmsEng,SyntaxEng,DictEng,(ResEng = ResEng),ExtraEng in {
    param Frontability = Frontable | NonFrontable;
    --adapted from experimental/Pred
    oper
        effort_rec : Type = {soa : NP;effort : V;effort_nom : NP};
        defaultAgr : ResEng.Agr = ResEng.AgP3Sg ResEng.Neutr ;
        
        nominalize : VP -> NP = \vpi ->
        lin NP
        {
            s = \\c => vpi.prp ++ vpi.s2 ! defaultAgr;
            a = defaultAgr
        } ;
        
        nominalizeWithAdverbMovement : VPWithAdvMovement -> NP = \vpi ->
            lin NP (
                table
                {                    
                    Frontable =>
                        variants
                        {
                            {
                                s = \\c => vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s;
                                a = defaultAgr;
                            };
                            {
                                s = \\c => vpi.adv.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr;
                                a = defaultAgr;
                            };
                        };
                    NonFrontable =>
                        {
                            s = \\c => vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s;
                            a = defaultAgr;
                        }
                } ! vpi.frontable
            );
        
        --CN variant for nominalization, in case adjectives need to be added etc.
        nominalizeCN : VP -> CN = \vpi ->
        lin CN
        {
            s = \\n,c => vpi.prp ++ vpi.s2 ! defaultAgr;
            g = ResEng.Neutr
        } ;
        
        --e.g. despite x-ing
       
        VPWithAdvMovement : Type = {vp : VP;frontable : Frontability;adv : Adv};
        AdvWithFrontability : Type = {frontable : Frontability;adv : Adv};
        
        participleAdv : Prep -> VPWithAdvMovement -> Adv = \prep,vpi ->
            lin Adv (
                table
                {                    
                    Frontable =>
                        variants
                        {
                            --adjectival adverbs of the participle can be in front or back, e.g. "publicly reconciling"/"reconciling publicly"
                            {s = prep.s ++ vpi.adv.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr;};
                            {s = prep.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s ;};
                        };
                    NonFrontable => {s = prep.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s;} --prepositional adverbs of the participle can only at the back, e.g. "reconciling in public"
                } ! vpi.frontable
            );
        
        
        
        
    lincat
        TestSentence = S;
        Effort = effort_rec;
        Difficulty = AP;
        HighDifficulty = AP;
        LowDifficulty = AP;
        StateOfAffairs = NP;
        Parties = NP;
        PositiveAct = {np: NP; vp: VPWithAdvMovement};
        DiscordantRelation = {subj : NP; s : S; pron_s : S;subj_pron : NP};
        EventType = NP;
        BigEventKind = AP;
        DelicateEventKind = AP;
        WrappedEventKind = {ap : AP; indef_ap : AP}; --indef_ap is an ap with determiner already applied. A hack to handle determiners in AP conjunction
        
        --"people/Those who exercise regularly reduce/lower their chances/chance/risk of getting/0 cancer and/or depression."
        PositiveHealthAction = {np: NP; vp_adv: VPWithAdvMovement;vp : VP};
        WrappedNegativeHealthOutcome = NP;
        NegativeHealthOutcome1 = NP;
        NegativeHealthOutcome2 = NP;
        --OutcomeType = V2;
        --Periodicality = {ap : AP; adv : AdvWithFrontability};
        Activity = {n : N;v : V};
        

    lin
        EffortIsOfDifficulty effort difficulty =
            let
                futureOrPresent : Tense = variants {presentTense;futureTense;};
                basecl : Cl = variants {
                    --it is hard to rebuild
                    (mkCl (mkVP (mkAP difficulty (mkVP (mkV2 effort.effort) effort.soa))));
                    --rebuilding is hard
                    (mkCl (effort.effort_nom) difficulty);
                    };
            in
                mkS futureOrPresent basecl;
                
        Trust =
            let
                real_variant = variants {genuine_A;real_A;true_A};
            in
                mkNP (mkCN real_variant trust_N);
        Rebuilding state_of_affairs = 
            let
                rebuild_v : V = variants {mkV "rebuild";mkV "restore";};
            in
                {soa = state_of_affairs; effort=rebuild_v;effort_nom=nominalize (mkVP rebuild_V2 state_of_affairs);};--;
                
        VeryHard =  mkAP (variants {hard_A;difficult_A});
        ImpossibleApposition highdif =
            let
                possibility_ada = variants {mkAdA "perhaps";mkAdA "maybe";mkAdA "even"};
            in
                mkAP (mkConj ",") (mkListAP highdif (mkAP possibility_ada impossible_A));
                
        HighDifficultyWrap highdif = highdif;
        
        
        --"despite public reconciliation, the two parties still have differences
        DespitePositiveAct posact discord =
            let
                mkSImperfectOrPerfect : Cl -> S = variants {\cl-> mkS presentTense anteriorAnt cl;\cl-> mkS pastTense simultaneousAnt cl};
                posact_vp = mkVP posact.vp.vp posact.vp.adv;
            in
                variants
                {
                    --despite posact 
                    let
                        adv_variant : Adv = variants
                            {
                                participleAdv despite_Prep posact.vp;
                                mkAdv despite_Prep posact.np;
                            };
                    in
                        variants
                            {
                                mkS adv_variant discord.s;
                            };
                    
                    --even though posact, still have discord/still have discord, even though posact  
                    let
                        although_variant : Subj = variants {even_though_Subj;although_Subj;};
                    in
                        variants
                            {
                                mkS (mkAdv although_variant (mkSImperfectOrPerfect (mkCl discord.subj posact_vp))) discord.pron_s;
                                SSubjS discord.s although_variant (mkSImperfectOrPerfect (mkCl discord.subj_pron posact_vp));
                            };
                    
                    
                    mkS (mkConj "but") (mkSImperfectOrPerfect (mkCl discord.subj posact_vp)) discord.pron_s;
                    
                };
        
        PublicReconciliation =
            let
                a_variant : A = public_A;
                a_n_variant : N = public_N;
            in
                let
                    n : N = reconciliation_N;
                    np : NP = mkNP (mkCN a_variant n);
                    v : V = mkV "reconcile";
                    mend_variant : V2 = variants
                        {
                            repair_V2;
                            mend_V2;
                            restore_V2;
                        };
                    relation_variant : NP = variants
                        {
                            mkNP they_Pron relationship_N;
                            mkNP (mkDet they_Pron pluralNum) (relation_N);
                        };
                    vp_variant : VP = variants
                        {
                            mkVP v;
                            mkVP mend_variant relation_variant;
                        };
                    adj_adv_variant : Adv = variants {mkAdv a_variant};
                    prep_adv_variant : Adv = variants {mkAdv in_Prep (mkNP a_n_variant)};
                    vp_adv : VPWithAdvMovement =
                        variants
                        {
                            {vp = vp_variant; frontable = NonFrontable; adv =  prep_adv_variant}; --"reconcile in public"
                            {vp = vp_variant; frontable = Frontable; adv = adj_adv_variant}; --"reconcile publicly"
                            
                        };
                    np_variant : NP = variants {np}
                in
                    {np = np_variant; vp = vp_adv};

        DiscordantRelationWrap discord = discord.s;
        HaveDifferences parties =
            let
                still_adv : AdV = mkAdV "still";
                significant_A : A = variants {major_A;significant_A;substantial_A};
                sig_dif_cn : CN = mkCN significant_A (difference_N);
            in
                variants
                {
                    let
                        subj_pron = mkNP they_Pron;
                        vp : VP = mkVP still_adv (mkVP have_V2 (mkNP aPl_Det sig_dif_cn));
                        s : S = mkS presentTense (mkCl parties vp);
                        pron_s : S = mkS presentTense (mkCl subj_pron vp);
                    in
                        {s = s; pron_s = pron_s;subj = parties;subj_pron = subj_pron};
                        
                };
                
        TwoParties =
            let
                quant_variant = variants {the_Quant;this_Quant};
            in
                variants
                {
                    mkNP quant_variant (mkNum "2");
                    mkNP quant_variant (mkNum "2") (party_N);
                };
                
        
        
        EventIsAnOperationOfKind okind =
            let
                op_variant : N = variants {operation_N;task_N;};
                relative : RS = mkRS pastTense (mkRCl which_RP okind.ap);
                predicative_variant : NP = variants
                {
                    mkNP (mkNP aSg_Det op_variant) relative;
                    mkNP aSg_Det (mkCN okind.ap op_variant);
                    mkNP (mkCN okind.indef_ap op_variant); 
                };
                cl_variant : Cl = variants{
                    mkCl (variants {this_NP;it_NP;}) predicative_variant;
                }
            in
                mkS pastTense (cl_variant);
                
                
        --This is problematic because of determiner placement in adjective conjunctions. There's multiple possibilities, e.g.
        --this was a simultaneously big and small task / this was simultaneously a big and small task / this was simultaneously a big and a small task
        --this was at the same time a big and small task / this was at the same time a big and a small task (but not "an at the same time big and small task")
        AndAtTheSameTimeOperationKind kind1 kind2 =
            let
                ap_sametime_variant : Conj = 
                    variants
                    {
                        mkConj (variants { "and at the same time";"and also";"and simultaneously"});
                        sd2 "simultaneously" "and" ** {n = plural}
                    };
                indef_ap_sametime_variant : Conj = 
                    variants
                    {
                        both7and_DConj;
                        mkConj (variants {"and at the same time";"and also";"and simultaneously"});
                        sd2 "at the same time" "and" ** {n = plural};
                        sd2 "simultaneously" "and" ** {n = plural}
                    };
                indef_kind1 = kind1 ** {s : ResEng.Agr => Str = \\agr => ResEng.artIndef ++ (kind1.s ! agr)};
                indef_kind2 = kind2 ** {s : ResEng.Agr => Str = \\agr => ResEng.artIndef ++ (kind2.s ! agr)};
            in
                {ap = mkAP ap_sametime_variant kind1 kind2;indef_ap = mkAP indef_ap_sametime_variant indef_kind1 indef_kind2};
                
        VeryBig =
            variants {
                mkAP (variants {huge_A;massive_A;gigantic_A;});
                let
                    intensifier_variant = mkAdA (variants {"very";"extremely"});
                in
                    mkAP intensifier_variant (variants {demanding_A;});
            };
        VeryDelicate =
            variants {
                let
                    intensifier_variant = mkAdA (variants {"very";"extremely"});
                in
                    mkAP intensifier_variant (variants {delicate_A;precise_A});
            };
        WrapBigEventKind kind = {ap = kind; indef_ap = kind};
        WrapDelicateEventKind kind = {ap = kind; indef_ap = kind};
        
        ActionReducesNegativeEffectRisk action effect =
            let
                reduce_variant = variants {reduce_V2;lower_V2;lessen_V2};
                chance_variant = variants {chance_N;risk_N};
                relative_head_variant : NP =
                    variants
                    {
                        mkNP aPl_Det (mkN "people" "people");
                        those_NP;
                    };
                
                relative_variant : NP = mkNP relative_head_variant (mkRS presentTense (mkRCl who_RP action.vp));
                
                --can be either "risk for" / "risk of"
                risk_cn : CN =
                    variants
                    {
                        (mkCN (mkN2 chance_variant) effect);
                        (mkCN (mkN2 chance_variant for_Prep) effect);
                    };
                
                --some constructions only work with "risk", e.g. "at a lower risk", but not "at a lower chance"
                risk_risk_cn : CN =
                    variants
                    {
                        (mkCN (mkN2 risk_N) effect);
                        (mkCN (mkN2 risk_N for_Prep) effect);
                    };
                    
                risk_np_variant : NP =
                    variants
                    {
                        mkNP risk_cn;
                        mkNP theSg_Det risk_cn;
                        mkNP thePl_Det risk_cn;
                        mkNP aPl_Det risk_cn;
                    };
                
                low_variant : A = variants {low_A;small_A};
                
                cl_variant : Cl =
                    variants
                    {
                        --"have a lower risk"
                        mkCl relative_variant (mkVP have_V2 (mkNP aSg_Det (mkCN (comparAP low_variant) risk_cn)));
                        --"are at a lower risk for
                        mkCl relative_variant (mkVP (mkAdv at_Prep (mkNP aSg_Det (mkCN (comparAP low_A) risk_risk_cn))));
                        --"puts you at a lower risk for" missing
                        
                        mkCl action.np (mkVP reduce_variant risk_np_variant);
                        mkCl (nominalizeWithAdverbMovement action.vp_adv) (mkVP reduce_variant risk_np_variant);
                        let
                            prep_variant = variants {in_Prep;for_Prep};
                        in
                            mkCl risk_np_variant (mkVP (mkVP (comparAP low_A)) (mkAdv prep_variant relative_variant));
                    };
            in
                mkS presentTense cl_variant;
                
        RegularPeriodicalActivity activity =
            let
                period_ap : AP = mkAP regular_A;
                period_adv : Adv = mkAdv regular_A;
                period_prep_adv : Adv = mkAdv on_Prep (mkNP aSg_Det (mkCN regular_A basis_N));
                
                np_variant : NP =
                    variants
                    {
                        mkNP (mkCN period_ap activity.n);
                    };
                vp_adv_variant : VPWithAdvMovement =
                    variants
                    {
                        {vp = mkVP activity.v; frontable = NonFrontable; adv=period_prep_adv};
                        {vp = mkVP activity.v; frontable = Frontable; adv=period_adv};
                    };
                --this messes word order, fix
                vp_variant : VP = variants
                {
                    mkVP (mkVP activity.v) period_adv;
                    mkVP (mkVP activity.v) period_prep_adv;
                    mkVP (mkAdV period_adv.s) (mkVP activity.v);
                }
            in
                {np = np_variant;vp_adv = vp_adv_variant;vp = vp_variant};
                
        {-Regular =
            let
                ap_variant : AP = mkAP regular_A;
                adv_variant : AdvWithFrontability = variants
                {
                    {frontable = Frontable;adv = mkAdv regular_A};
                    {frontable = NonFrontable;adv = mkAdv on_Prep (mkNP aSg_Det (mkCN regular_A basis_N))};
                };
            in
                {ap = ap_variant;adv = adv_variant}; --an interesting case would be biweekly, which can be twice a week/twice a month, also a prepositionless nonfrontable adv phrase (twice a week) -}
                
        CombinedNegativeHealthEffect12 neg1 neg2 =
            let
                conjunct : NP = variants
                {
                    mkNP and_Conj neg1 neg2;
                    --mkNP or_Conj neg1 neg2;
                }
            in
                variants
                {
                    conjunct;
                    mkNP (nominalizeCN (mkVP develop_V2 conjunct));
                };
                
        CombinedNegativeHealthEffect21 neg2 neg1 =
            let
                conjunct : NP = variants
                {
                    mkNP and_Conj neg1 neg2;
                    --mkNP or_Conj neg1 neg2;
                }
            in
                variants
                {
                    conjunct;
                    mkNP (nominalizeCN (mkVP develop_V2 conjunct));
                };
            
        SingleNegativeHealthEffect1 neg1 = variants
        {
            neg1;
            mkNP (nominalizeCN (mkVP develop_V2 neg1))
        };
        
        SingleNegativeHealthEffect2 neg2 = variants
        {
            neg2;
            mkNP (nominalizeCN (mkVP develop_V2 neg2))
        };
        
        Exercise = {n= exercise_N;v=exercise_V};
        Cancer = mkNP cancer_N;
        Depression = mkNP depression_N;
        
}   