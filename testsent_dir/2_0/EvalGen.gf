--# -coding=UTF-8
abstract EvalGen = {
    flags
        startcat = TestSentence;
    cat
        TestSentence;
        Effort;
        Difficulty;
        HighDifficulty;
        LowDifficulty;
        StateOfAffairs;
        Parties;
        PositiveAct;
        DiscordantRelation;
        BigEventKind;
        DelicateEventKind;
        WrappedEventKind;
        PositiveHealthAction;
        WrappedNegativeHealthOutcome;
        OutcomeType;
        NegativeHealthOutcome1;
        NegativeHealthOutcome2;
        Periodicality;
        Activity;
    fun
        HaveDifferences : Parties -> DiscordantRelation;

        TwoParties : Parties;

        PublicReconciliation : PositiveAct;

        DespitePositiveAct : PositiveAct -> DiscordantRelation -> TestSentence;
}