--# -coding=UTF-8
abstract EvalGenTemplate4 = {
    flags
        startcat = TestSentence;
    cat
        TestSentence;
        PositiveHealthAction;
        WrappedNegativeHealthOutcome;
        OutcomeType;
        NegativeHealthOutcome;
        Periodicality;
        Activity;
    fun
        
        ActionReducesNegativeEffectRisk : PositiveHealthAction -> WrappedNegativeHealthOutcome -> TestSentence;
        --PeriodicalActivity : Activity -> Periodicality -> PositiveHealthAction; --the first argument is now fixed, since having two args causes compilation to slow down with finnish
        RegularPeriodicalActivity : Activity -> PositiveHealthAction;
        SemVar1aCombinedNegativeHealthEffect : NegativeHealthOutcome -> NegativeHealthOutcome -> WrappedNegativeHealthOutcome;
        
        SemVar1bSingleNegativeHealthEffect : NegativeHealthOutcome -> WrappedNegativeHealthOutcome;
        SemVar2aCancer : NegativeHealthOutcome;
        SemVar2bDepression : NegativeHealthOutcome;
        --SemVar2cCardiovascularDisease : NegativeHealthOutcome;
        SemVar2cDementia : NegativeHealthOutcome;
        --Regular : Periodicality;
        Exercise : Activity;
        --Chronic, Acute : OutcomeType;
        
    }