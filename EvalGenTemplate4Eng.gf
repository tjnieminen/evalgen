concrete EvalGenTemplate4Eng of EvalGenTemplate4 = open Prelude,LangEng,ParadigmsEng,SyntaxEng,DictEng,(ResEng = ResEng),ExtraEng in {
    param Frontability = Frontable | NonFrontable;
    --adapted from experimental/Pred
    oper
        effort_rec : Type = {soa : NP;effort : V;effort_nom : NP};
        defaultAgr : ResEng.Agr = ResEng.AgP3Sg ResEng.Neutr ;
        
        nominalize : VP -> NP = \vpi ->
        lin NP
        {
            s = \\c => vpi.prp ++ vpi.s2 ! defaultAgr;
            a = defaultAgr
        } ;
        
        nominalizeWithAdverbMovement : VPWithAdvMovement -> NP = \vpi ->
            lin NP (
                table
                {                    
                    Frontable =>
                        variants
                        {
                            {
                                s = \\c => vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s;
                                a = defaultAgr;
                            };
                            {
                                s = \\c => vpi.adv.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr;
                                a = defaultAgr;
                            };
                        };
                    NonFrontable =>
                        {
                            s = \\c => vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s;
                            a = defaultAgr;
                        }
                } ! vpi.frontable
            );
        
        --CN variant for nominalization, in case adjectives need to be added etc.
        nominalizeCN : VP -> CN = \vpi ->
        lin CN
        {
            s = \\n,c => vpi.prp ++ vpi.s2 ! defaultAgr;
            g = ResEng.Neutr
        } ;
        
        --e.g. despite x-ing
       
        VPWithAdvMovement : Type = {vp : VP;frontable : Frontability;adv : Adv};
        AdvWithFrontability : Type = {frontable : Frontability;adv : Adv};
        
        participleAdv : Prep -> VPWithAdvMovement -> Adv = \prep,vpi ->
            lin Adv (
                table
                {                    
                    Frontable =>
                        variants
                        {
                            --adjectival adverbs of the participle can be in front or back, e.g. "publicly reconciling"/"reconciling publicly"
                            {s = prep.s ++ vpi.adv.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr;};
                            {s = prep.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s ;};
                        };
                    NonFrontable => {s = prep.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s;} --prepositional adverbs of the participle can only at the back, e.g. "reconciling in public"
                } ! vpi.frontable
            );
        
        
        
        
    lincat
        TestSentence = S;
        
        --"people/Those who exercise regularly reduce/lower their chances/chance/risk of getting/0 cancer and/or depression."
        PositiveHealthAction = {np: NP; vp_adv: VPWithAdvMovement;vp : VP};
        WrappedNegativeHealthOutcome = NP;
        NegativeHealthOutcome = NP;

        --OutcomeType = V2;
        --Periodicality = {ap : AP; adv : AdvWithFrontability};
        Activity = {n : N;v : V};
        

    lin
        
        ActionReducesNegativeEffectRisk action effect =
            let
                reduce_variant = variants {reduce_V2;lower_V2;lessen_V2};
                chance_variant = variants {chance_N;risk_N};
                relative_head_variant : CN =
                    variants
                    {
                        mkCN (mkN "people" "people");
                        mkCN (mkN "those" "those" "those" "those");
                    }; 
                
                relative_variant : NP = mkNP aPl_Det (mkCN relative_head_variant (mkRS presentTense (mkRCl who_RP action.vp)));
                
                --can be either "risk for" / "risk of"
                risk_cn : CN =
                    variants
                    {
                        (mkCN (mkN2 chance_variant) effect);
                        (mkCN (mkN2 chance_variant for_Prep) effect);
                    };
                
                --some constructions only work with "risk", e.g. "at a lower risk", but not "at a lower chance"
                risk_risk_cn : CN =
                    variants
                    {
                        (mkCN (mkN2 risk_N) effect);
                        (mkCN (mkN2 risk_N for_Prep) effect);
                    };
                    
                risk_np_variant : NP =
                    variants
                    {
                        mkNP risk_cn;
                        mkNP theSg_Det risk_cn;
                        mkNP thePl_Det risk_cn;
                        mkNP aPl_Det risk_cn;
                    };
                
                low_variant : A = variants {low_A;small_A};
                
                cl_variant : Cl =
                    variants
                    {
                        --"have a lower risk"
                        mkCl relative_variant (mkVP have_V2 (mkNP aSg_Det (mkCN (comparAP low_variant) risk_cn)));
                        --"are at a lower risk for
                        mkCl relative_variant (mkVP (mkAdv at_Prep (mkNP aSg_Det (mkCN (comparAP low_A) risk_risk_cn))));
                        --"puts you at a lower risk for" missing
                        
                        mkCl action.np (mkVP reduce_variant risk_np_variant);
                        mkCl (nominalizeWithAdverbMovement action.vp_adv) (mkVP reduce_variant risk_np_variant);
                        let
                            prep_variant = variants {in_Prep;for_Prep};
                        in
                            mkCl risk_np_variant (mkVP (mkVP (comparAP low_A)) (mkAdv prep_variant relative_variant));
                    };
            in
                mkS presentTense cl_variant;
                
        RegularPeriodicalActivity activity =
            let
                period_ap : AP = mkAP regular_A;
                period_adv : Adv = mkAdv regular_A;
                period_prep_adv : Adv = mkAdv on_Prep (mkNP aSg_Det (mkCN regular_A basis_N));
                
                np_variant : NP =
                    variants
                    {
                        mkNP (mkCN period_ap activity.n);
                    };
                vp_adv_variant : VPWithAdvMovement =
                    variants
                    {
                        {vp = mkVP activity.v; frontable = NonFrontable; adv=period_prep_adv};
                        {vp = mkVP activity.v; frontable = Frontable; adv=period_adv};
                    };
                --this messes word order, fix
                vp_variant : VP = variants
                {
                    mkVP (mkVP activity.v) period_adv;
                    mkVP (mkVP activity.v) period_prep_adv;
                    mkVP (mkAdV period_adv.s) (mkVP activity.v);
                }
            in
                {np = np_variant;vp_adv = vp_adv_variant;vp = vp_variant};
                
        {-Regular =
            let
                ap_variant : AP = mkAP regular_A;
                adv_variant : AdvWithFrontability = variants
                {
                    {frontable = Frontable;adv = mkAdv regular_A};
                    {frontable = NonFrontable;adv = mkAdv on_Prep (mkNP aSg_Det (mkCN regular_A basis_N))};
                };
            in
                {ap = ap_variant;adv = adv_variant}; --an interesting case would be biweekly, which can be twice a week/twice a month, also a prepositionless nonfrontable adv phrase (twice a week) -}
                
        SemVar1aCombinedNegativeHealthEffect neg1 neg2 =
            let
                conjunct : NP = variants
                {
                    mkNP and_Conj neg1 neg2;
                    --mkNP or_Conj neg1 neg2;
                }
            in
                variants
                {
                    conjunct;
                    mkNP (nominalizeCN (mkVP develop_V2 conjunct));
                };
                
        SemVar1bSingleNegativeHealthEffect neg1 = variants
        {
            neg1;
            mkNP (nominalizeCN (mkVP develop_V2 neg1))
        };
        
        
        Exercise = {n= exercise_N;v=exercise_V};
        SemVar2aCancer = mkNP cancer_N;
        SemVar2bDepression = mkNP depression_N;
        {-SemVar2cCardiovascularDisease = variants
        {
            mkNP (mkN "heart" disease_N);
            mkNP aPl_Det (mkN "heart" disease_N);
            mkNP (mkN "cardiovascular" disease_N);
            mkNP aPl_Det (mkN "cardiovascular" disease_N);
        };-}
        SemVar2cDementia = mkNP dementia_N;
}   