// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Copyright 2012 Aix-Marseille Univ.
// Author: benoit.favre@lif.univ-mrs.fr (Benoit Favre)

#include <fst/fstlib.h>
#include <fst/script/print.h>
#include <fst/script/shortest-path.h>
#include <iostream>
#include <fstream>

using namespace fst;
using namespace fst::script;

int main(int argc, char** argv) {

  namespace s = fst::script;
    if(argc != 5) {
        std::cerr << "usage: " << argv[0] << " <input fst> <edit fst> <reference fst> <nbest file>\n";
        return 1;
    }
    StdVectorFst *input = StdVectorFst::Read(argv[1]);
    StdVectorFst *edit = StdVectorFst::Read(argv[2]);
    StdVectorFst *reference = StdVectorFst::Read(argv[3]);

    // step 3: compose
    //StdVectorFst halfCompose1;
    //StdComposeFst halfCompose;
    std::cerr << "sort edit\n";
    ArcSort(edit, StdOLabelCompare());
    std::cerr << "compose input edit\n"; 
    StdComposeFst halfCompose(*input, *edit);
    std::cerr << "sort ref\n"; 
    ArcSort(reference, StdILabelCompare());
    //StdVectorFst oracle;
    //StdComposeFst oracStdComposeFst orac(halfC
    std::cerr << "compose ref\n"; 
    StdComposeFst oracle(halfCompose, *reference);
    //StdComposeFst oracle(*edit, *reference);

    std::cerr << "find nbest\n"; 
    StdVectorFst nbest;
    
    ShortestPath(oracle,&nbest,100,false,false);

    //ostream *ostrm = &cout;
    //string dest = "standard output";
    std::ofstream nbestfile;
    nbestfile.open(argv[4]);
    
    const fst::SymbolTable* outputSymbols = edit -> OutputSymbols();
    const fst::SymbolTable* inputSymbols = edit -> InputSymbols();
    const SymbolTable *ssyms = 0;
    
    nbest.Write(nbestfile,FstWriteOptions());//,"",inputSymbols,outputSymbols);

    std::cerr << "find shortest\n"; 

    StdVectorFst result;    
    ShortestPath(nbest,&result,1,false,false);
    //result.Write("");
    ostream *ostrm = &cout;
    //string dest = "standard output";
 
    //FstClass* result_class = new FstClass(result);
    
    PrintFst(result,cout,"",inputSymbols,outputSymbols);
    
    delete input;
    delete edit;
    delete reference;

}
