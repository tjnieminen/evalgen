concrete EvalGenTemplate1Eng of EvalGenTemplate1 = open Prelude,LangEng,ParadigmsEng,SyntaxEng,DictEng,(ResEng = ResEng),ExtraEng in {
    param Frontability = Frontable | NonFrontable;
    --adapted from experimental/Pred
    oper
        effort_rec : Type = {soa : NP;effort : V;effort_nom : NP};
        defaultAgr : ResEng.Agr = ResEng.AgP3Sg ResEng.Neutr ;
        
        nominalize : VP -> NP = \vpi ->
        lin NP
        {
            s = \\c => vpi.prp ++ vpi.s2 ! defaultAgr;
            a = defaultAgr
        } ;
        
        nominalizeWithAdverbMovement : VPWithAdvMovement -> NP = \vpi ->
            lin NP (
                table
                {                    
                    Frontable =>
                        variants
                        {
                            {
                                s = \\c => vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s;
                                a = defaultAgr;
                            };
                            {
                                s = \\c => vpi.adv.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr;
                                a = defaultAgr;
                            };
                        };
                    NonFrontable =>
                        {
                            s = \\c => vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s;
                            a = defaultAgr;
                        }
                } ! vpi.frontable
            );
        
        --CN variant for nominalization, in case adjectives need to be added etc.
        nominalizeCN : VP -> CN = \vpi ->
        lin CN
        {
            s = \\n,c => vpi.prp ++ vpi.s2 ! defaultAgr;
            g = ResEng.Neutr
        } ;
        
        --e.g. despite x-ing
       
        VPWithAdvMovement : Type = {vp : VP;frontable : Frontability;adv : Adv};
        AdvWithFrontability : Type = {frontable : Frontability;adv : Adv};
        
        participleAdv : Prep -> VPWithAdvMovement -> Adv = \prep,vpi ->
            lin Adv (
                table
                {                    
                    Frontable =>
                        variants
                        {
                            --adjectival adverbs of the participle can be in front or back, e.g. "publicly reconciling"/"reconciling publicly"
                            {s = prep.s ++ vpi.adv.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr;};
                            {s = prep.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s ;};
                        };
                    NonFrontable => {s = prep.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s;} --prepositional adverbs of the participle can only at the back, e.g. "reconciling in public"
                } ! vpi.frontable
            );
        
        
        
        
    lincat
        TestSentence = S;
        EventType = NP;
        BigEventKind = AP;
        PreciseEventKind = AP;
        WrappedEventKind = {ap : AP; indef_ap : AP}; --indef_ap is an ap with determiner already applied. A hack to handle determiners in AP conjunction
    lin
        EventIsAnOperationOfKind okind =
            let
                op_variant : N = variants {operation_N;task_N;};
                relative : RS = mkRS pastTense (mkRCl which_RP okind.ap);
                predicative_variant : NP = variants
                {
                    mkNP (mkNP aSg_Det op_variant) relative;
                    mkNP aSg_Det (mkCN okind.ap op_variant);
                    mkNP (mkCN okind.indef_ap op_variant);
                };
                cl_variant : Cl = variants{
                    mkCl (variants {this_NP;it_NP;}) predicative_variant;
                }
            in
                mkS pastTense (cl_variant);
                
                
        --This is problematic because of determiner placement in adjective conjunctions. There's multiple possibilities, e.g.
        --this was a simultaneously big and small task / this was simultaneously a big and small task / this was simultaneously a big and a small task
        --this was at the same time a big and small task / this was at the same time a big and a small task (but not "an at the same time big and small task")
        SemVar1aAndAtTheSameTimeOperationKind kind1 kind2 =
            let
                ap_sametime_variant : Conj = 
                    variants
                    {
                        mkConj (variants { "and at the same time";"and also";"and simultaneously"});
                        sd2 "simultaneously" "and" ** {n = plural}
                    };
                indef_ap_sametime_variant : Conj = 
                    variants
                    {
                        both7and_DConj;
                        mkConj (variants {"and at the same time";"and also";"and simultaneously"});
                        sd2 "at the same time" "and" ** {n = plural};
                        sd2 "simultaneously" "and" ** {n = plural}
                    };
                indef_kind1 = kind1 ** {s : ResEng.Agr => Str = \\agr => ResEng.artIndef ++ (kind1.s ! agr)};
                indef_kind2 = kind2 ** {s : ResEng.Agr => Str = \\agr => ResEng.artIndef ++ (kind2.s ! agr)};
            in
                {ap = mkAP ap_sametime_variant kind1 kind2;indef_ap = mkAP indef_ap_sametime_variant indef_kind1 indef_kind2};
                
        VeryBig =
            variants {
                mkAP (variants {huge_A;massive_A;gigantic_A;});
                let
                    intensifier_variant = mkAdA (variants {"very";"extremely"});
                in
                    mkAP intensifier_variant (variants {demanding_A;});
            };
        VeryPrecise =
            variants {
                let
                    intensifier_variant = mkAdA (variants {"very";"extremely"});
                in
                    mkAP intensifier_variant (variants {precise_A});
            };
        SemVar1bWrapBigEventKind kind = {ap = kind; indef_ap = kind ** {s : ResEng.Agr => Str = \\agr => ResEng.artIndef ++ (kind.s ! agr)}};
        SemVar1cWrapPreciseEventKind kind = {ap = kind; indef_ap = kind ** {s : ResEng.Agr => Str = \\agr => ResEng.artIndef ++ (kind.s ! agr)}};
        
       
}   