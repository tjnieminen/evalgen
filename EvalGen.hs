module EvalGen where

import PGF hiding (Tree)
import qualified PGF
----------------------------------------------------
-- automatic translation from GF to Haskell
----------------------------------------------------

class Gf a where
  gf :: a -> PGF.Tree
  fg :: PGF.Tree -> a

newtype GString = GString String  deriving Show

instance Gf GString where
  gf (GString x) = mkStr x
  fg t =
    case unStr t of
      Just x  ->  GString x
      Nothing -> error ("no GString " ++ show t)

newtype GInt = GInt Int  deriving Show

instance Gf GInt where
  gf (GInt x) = mkInt x
  fg t =
    case unInt t of
      Just x  ->  GInt x
      Nothing -> error ("no GInt " ++ show t)

newtype GFloat = GFloat Double  deriving Show

instance Gf GFloat where
  gf (GFloat x) = mkDouble x
  fg t =
    case unDouble t of
      Just x  ->  GFloat x
      Nothing -> error ("no GFloat " ++ show t)

----------------------------------------------------
-- below this line machine-generated
----------------------------------------------------

data GBigEventKind = GVeryBig 
  deriving Show

data GDelicateEventKind = GVeryDelicate 
  deriving Show

data GDifficulty =
   GHighDifficultyWrap GHighDifficulty 
 | GImpossibleApposition GHighDifficulty 
 | GLowDifficultyWrap GLowDifficulty 
  deriving Show

data GDiscordantRelation = GHaveDifferences GParties 
  deriving Show

data GEffort = GRebuilding GStateOfAffairs 
  deriving Show

data GHighDifficulty = GVeryHard 
  deriving Show

data GParties = GTwoParties 
  deriving Show

data GPositiveAct = GPublicReconciliation 
  deriving Show

data GStateOfAffairs = GTrust 
  deriving Show

data GTestSentence =
   GDespitePositiveAct GPositiveAct GDiscordantRelation 
 | GDiscordantRelationWrap GDiscordantRelation 
 | GEffortIsOfDifficulty GEffort GDifficulty 
 | GEventIsAnOperationOfKind GWrappedEventKind 
  deriving Show

data GWrappedEventKind =
   GAndAtTheSameTimeOperationKind GBigEventKind GDelicateEventKind 
 | GWrapBigEventKind GBigEventKind 
 | GWrapDelicateEventKind GDelicateEventKind 
  deriving Show

data GEventKindSpecifier

data GEventType

data GLowDifficulty


instance Gf GBigEventKind where
  gf GVeryBig = mkApp (mkCId "VeryBig") []

  fg t =
    case unApp t of
      Just (i,[]) | i == mkCId "VeryBig" -> GVeryBig 


      _ -> error ("no BigEventKind " ++ show t)

instance Gf GDelicateEventKind where
  gf GVeryDelicate = mkApp (mkCId "VeryDelicate") []

  fg t =
    case unApp t of
      Just (i,[]) | i == mkCId "VeryDelicate" -> GVeryDelicate 


      _ -> error ("no DelicateEventKind " ++ show t)

instance Gf GDifficulty where
  gf (GHighDifficultyWrap x1) = mkApp (mkCId "HighDifficultyWrap") [gf x1]
  gf (GImpossibleApposition x1) = mkApp (mkCId "ImpossibleApposition") [gf x1]
  gf (GLowDifficultyWrap x1) = mkApp (mkCId "LowDifficultyWrap") [gf x1]

  fg t =
    case unApp t of
      Just (i,[x1]) | i == mkCId "HighDifficultyWrap" -> GHighDifficultyWrap (fg x1)
      Just (i,[x1]) | i == mkCId "ImpossibleApposition" -> GImpossibleApposition (fg x1)
      Just (i,[x1]) | i == mkCId "LowDifficultyWrap" -> GLowDifficultyWrap (fg x1)


      _ -> error ("no Difficulty " ++ show t)

instance Gf GDiscordantRelation where
  gf (GHaveDifferences x1) = mkApp (mkCId "HaveDifferences") [gf x1]

  fg t =
    case unApp t of
      Just (i,[x1]) | i == mkCId "HaveDifferences" -> GHaveDifferences (fg x1)


      _ -> error ("no DiscordantRelation " ++ show t)

instance Gf GEffort where
  gf (GRebuilding x1) = mkApp (mkCId "Rebuilding") [gf x1]

  fg t =
    case unApp t of
      Just (i,[x1]) | i == mkCId "Rebuilding" -> GRebuilding (fg x1)


      _ -> error ("no Effort " ++ show t)

instance Gf GHighDifficulty where
  gf GVeryHard = mkApp (mkCId "VeryHard") []

  fg t =
    case unApp t of
      Just (i,[]) | i == mkCId "VeryHard" -> GVeryHard 


      _ -> error ("no HighDifficulty " ++ show t)

instance Gf GParties where
  gf GTwoParties = mkApp (mkCId "TwoParties") []

  fg t =
    case unApp t of
      Just (i,[]) | i == mkCId "TwoParties" -> GTwoParties 


      _ -> error ("no Parties " ++ show t)

instance Gf GPositiveAct where
  gf GPublicReconciliation = mkApp (mkCId "PublicReconciliation") []

  fg t =
    case unApp t of
      Just (i,[]) | i == mkCId "PublicReconciliation" -> GPublicReconciliation 


      _ -> error ("no PositiveAct " ++ show t)

instance Gf GStateOfAffairs where
  gf GTrust = mkApp (mkCId "Trust") []

  fg t =
    case unApp t of
      Just (i,[]) | i == mkCId "Trust" -> GTrust 


      _ -> error ("no StateOfAffairs " ++ show t)

instance Gf GTestSentence where
  gf (GDespitePositiveAct x1 x2) = mkApp (mkCId "DespitePositiveAct") [gf x1, gf x2]
  gf (GDiscordantRelationWrap x1) = mkApp (mkCId "DiscordantRelationWrap") [gf x1]
  gf (GEffortIsOfDifficulty x1 x2) = mkApp (mkCId "EffortIsOfDifficulty") [gf x1, gf x2]
  gf (GEventIsAnOperationOfKind x1) = mkApp (mkCId "EventIsAnOperationOfKind") [gf x1]

  fg t =
    case unApp t of
      Just (i,[x1,x2]) | i == mkCId "DespitePositiveAct" -> GDespitePositiveAct (fg x1) (fg x2)
      Just (i,[x1]) | i == mkCId "DiscordantRelationWrap" -> GDiscordantRelationWrap (fg x1)
      Just (i,[x1,x2]) | i == mkCId "EffortIsOfDifficulty" -> GEffortIsOfDifficulty (fg x1) (fg x2)
      Just (i,[x1]) | i == mkCId "EventIsAnOperationOfKind" -> GEventIsAnOperationOfKind (fg x1)


      _ -> error ("no TestSentence " ++ show t)

instance Gf GWrappedEventKind where
  gf (GAndAtTheSameTimeOperationKind x1 x2) = mkApp (mkCId "AndAtTheSameTimeOperationKind") [gf x1, gf x2]
  gf (GWrapBigEventKind x1) = mkApp (mkCId "WrapBigEventKind") [gf x1]
  gf (GWrapDelicateEventKind x1) = mkApp (mkCId "WrapDelicateEventKind") [gf x1]

  fg t =
    case unApp t of
      Just (i,[x1,x2]) | i == mkCId "AndAtTheSameTimeOperationKind" -> GAndAtTheSameTimeOperationKind (fg x1) (fg x2)
      Just (i,[x1]) | i == mkCId "WrapBigEventKind" -> GWrapBigEventKind (fg x1)
      Just (i,[x1]) | i == mkCId "WrapDelicateEventKind" -> GWrapDelicateEventKind (fg x1)


      _ -> error ("no WrappedEventKind " ++ show t)

instance Show GEventKindSpecifier

instance Gf GEventKindSpecifier where
  gf _ = undefined
  fg _ = undefined



instance Show GEventType

instance Gf GEventType where
  gf _ = undefined
  fg _ = undefined



instance Show GLowDifficulty

instance Gf GLowDifficulty where
  gf _ = undefined
  fg _ = undefined




