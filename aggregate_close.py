#!/usr/bin/env python3
import glob
import logging
import argparse
import re
import random
import math
import ast

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',filename='close_error.log',level=logging.INFO,filemode='w')

def parse_errors_counts(analysis_files):
		close_error_re = re.compile(r"close edits by error count: (.*)\n")
		aggregated_error_counts = [[] for x in range(0,50)]
		for analysis_file_path in analysis_files:	
			with open(analysis_file_path,'r') as analysis_file:
				analysis_string = analysis_file.read()
			close_error_match = close_error_re.search(analysis_string)
			close_errors = ast.literal_eval(close_error_match.group(1))
			for error_count_index in range(0,50):	
				aggregated_error_counts[error_count_index] += close_errors[error_count_index]
		
		average_errors = [(i,len(x),[(sum([y[0] for y in x])/len(x)),(sum([y[1] for y in x])/len(x)),(sum([y[2] for y in x])/len(x))]) for i,x in enumerate(aggregated_error_counts) if len(x) > 100]
		return average_errors

if __name__ == '__main__':
		parser = argparse.ArgumentParser(
		description=("Parses close error count data from analyses for Latex"),
		formatter_class=argparse.ArgumentDefaultsHelpFormatter)

		parser.add_argument('-a','--analysis_dir', dest="analysis_dir", default=".",type=str,help="The dir containing the analyses to generate close error data for")

		parser.add_argument('-d','--data_file_path', dest="data_file_path",default="close_error.data",type=str,help="File where the data will be written.")
		args = parser.parse_args()

		analysis_files = glob.glob(args.analysis_dir+"/*analysis")

		close_errors = parse_errors_counts(analysis_files)
		print(close_errors)
		with open(args.data_file_path,'w') as data_file:
			for (index,count,closes) in close_errors:
				data_file.write("%d %d %s\n"%(index,count," ".join([str(x) for x in closes])))
