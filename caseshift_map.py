#!/usr/bin/env python3
import glob
import logging
import argparse
import re
import random
import math

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',filename='caseshift_map.log',level=logging.INFO,filemode='w')

heatmap_cases = ["NOM","GEN","PAR","INE","ELA","ILL","ADE","ABL", "ALL", "ESS","TRA", "ABE"]

def parse_caseshifts(analysis_files):
	caseshifts = dict()
	caseshift_re = re.compile(r"CASESHIFT-\w{3}-\w{3}: \d+")
	for analysis_file_path in analysis_files:
		caseshifts[analysis_file_path] = {k: {k2: "0" for k2 in heatmap_cases} for k in heatmap_cases}

		with open(analysis_file_path,'r') as analysis_file:
			analysis_string = analysis_file.read()
		caseshift_match = caseshift_re.findall(analysis_string)
		for caseshift in caseshift_match:
			shift,count = caseshift.split(": ")
			_,fromcase,tocase = shift.split("-")
			if fromcase not in heatmap_cases or tocase not in heatmap_cases:
				continue
			caseshifts[analysis_file_path][fromcase][tocase] = count
	
	return caseshifts

def convert_to_heatmap(caseshifts):
	heatmap = []
	for (fromindex,fromcase) in enumerate(heatmap_cases):
		fromcasematrix = ""
		for (toindex,tocase) in enumerate(heatmap_cases):
			tocaserow = "%s %s %s\n"%(toindex,fromindex,caseshifts[fromcase][tocase])
			fromcasematrix += tocaserow

		heatmap.append(fromcasematrix)

	return "\n".join(heatmap)
		
	

if __name__ == '__main__':
	parser = argparse.ArgumentParser(
		description=("Parses case shifts from analyses into heatmap data for Latex"),
		formatter_class=argparse.ArgumentDefaultsHelpFormatter)

	parser.add_argument('-a','--analysis_dir', dest="analysis_dir", default=".",type=str,help="The dir containing the analyses to generate heatmaps for")

	args = parser.parse_args()

	analysis_files = glob.glob(args.analysis_dir+"/*analysis")
	
	caseshifts = parse_caseshifts(analysis_files)
	for (analysisname,shifts) in caseshifts.items():
		analysis_heatmap = convert_to_heatmap(shifts)
		with open(analysisname+".heatmap",'w') as heatmap_file:
			heatmap_file.write(analysis_heatmap)

