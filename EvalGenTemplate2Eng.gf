concrete EvalGenTemplate2Eng of EvalGenTemplate2 = open Prelude,LangEng,ParadigmsEng,SyntaxEng,DictEng,(ResEng = ResEng),ExtraEng in {
    param Frontability = Frontable | NonFrontable;
    --adapted from experimental/Pred
    oper
        effort_rec : Type = {soa : NP;effort : V;effort_nom : NP};
        defaultAgr : ResEng.Agr = ResEng.AgP3Sg ResEng.Neutr ;
        
        nominalize : VP -> NP = \vpi ->
        lin NP
        {
            s = \\c => vpi.prp ++ vpi.s2 ! defaultAgr;
            a = defaultAgr
        } ;
        
        nominalizeWithAdverbMovement : VPWithAdvMovement -> NP = \vpi ->
            lin NP (
                table
                {                    
                    Frontable =>
                        variants
                        {
                            {
                                s = \\c => vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s;
                                a = defaultAgr;
                            };
                            {
                                s = \\c => vpi.adv.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr;
                                a = defaultAgr;
                            };
                        };
                    NonFrontable =>
                        {
                            s = \\c => vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s;
                            a = defaultAgr;
                        }
                } ! vpi.frontable
            );
        
        --CN variant for nominalization, in case adjectives need to be added etc.
        nominalizeCN : VP -> CN = \vpi ->
        lin CN
        {
            s = \\n,c => vpi.prp ++ vpi.s2 ! defaultAgr;
            g = ResEng.Neutr
        } ;
        
        --e.g. despite x-ing
       
        VPWithAdvMovement : Type = {vp : VP;frontable : Frontability;adv : Adv};
        AdvWithFrontability : Type = {frontable : Frontability;adv : Adv};
        
        participleAdv : Prep -> VPWithAdvMovement -> Adv = \prep,vpi ->
            lin Adv (
                table
                {                    
                    Frontable =>
                        variants
                        {
                            --adjectival adverbs of the participle can be in front or back, e.g. "publicly reconciling"/"reconciling publicly"
                            {s = prep.s ++ vpi.adv.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr;};
                            {s = prep.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s ;};
                        };
                    NonFrontable => {s = prep.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s;} --prepositional adverbs of the participle can only at the back, e.g. "reconciling in public"
                } ! vpi.frontable
            );
        
        
        
        
    lincat
        TestSentence = S;
        Effort = effort_rec;
        Difficulty = AP;
        HighDifficulty = AP;
        LowDifficulty = AP;
        StateOfAffairs = NP;

        

    lin
        EffortIsOfDifficulty effort difficulty =
            let
                futureOrPresent : Tense = variants {presentTense;futureTense;};
                basecl : Cl = variants {
                    --it is hard to rebuild
                    (mkCl (mkVP (mkAP difficulty (mkVP (mkV2 effort.effort) effort.soa))));
                    --rebuilding is hard
                    (mkCl (effort.effort_nom) difficulty);
                    };
            in
                mkS futureOrPresent basecl;
                
        Trust =
            let
                real_variant = variants {genuine_A;real_A;true_A};
            in
                mkNP (mkCN real_variant trust_N);
        Rebuilding state_of_affairs = 
            let
                rebuild_v : V = variants {mkV "rebuild";mkV "restore";};
            in
                {soa = state_of_affairs; effort=rebuild_v;effort_nom=nominalize (mkVP rebuild_V2 state_of_affairs);};
                
        VeryHard =  mkAP (variants {hard_A;difficult_A});
        VeryEasy=  mkAP (variants {easy_A});
        SemVar1aImpossibleApposition highdif =
            let
                possibility_ada = variants {mkAdA "perhaps";mkAdA "maybe";mkAdA "even"};
            in
                mkAP (mkConj ",") (mkListAP highdif (mkAP possibility_ada impossible_A));
                
        SemVar1bHighDifficultyWrap highdif = highdif;
        SemVar1cLowDifficultyWrap lowdif = lowdif;
        
        
        
}   