#!/usr/bin/env python3
import sys
import os
import glob
import pgf
import re
import subprocess
import argparse
import logging
import itertools
from itertools import chain
from nltk.metrics.distance import edit_distance
from random import shuffle,seed

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',filename='select_testset.log',level=logging.INFO,filemode='w')

"""too_similar = False
for selected_sentence in current_semvarset.strings["Eng"]:
	dist = edit_distance(sentence,selected_sentence)/len(sentence)
	if dist < 0.3:
		too_similar = True
		break
"""
if __name__ == '__main__':
	parser = argparse.ArgumentParser(
		description=("Generates a test set from all source sentences of semvars."),
		formatter_class=argparse.ArgumentDefaultsHelpFormatter)

	parser.add_argument('-t','--testset_dir', dest="testset_dir", default="evalgen_testsets",type=str,help="The dir where the fsm are and where the test set will be created")

	parser.add_argument('-e','--testset_name', dest="testset_name", default="testset_source",type=str,help="The base name used for the source string and id files")

	parser.add_argument('-n','--sentence_number', dest="sentence_number", default=100,type=int,help="How many sentences to include in test set")

	parser.add_argument('-m','--max_similarity', dest="max_similarity", default=0.7,type=float,help="How different does the sentence have to be from other sentences in the test set")
	
	parser.add_argument('-r','--rand_seed', dest="rand_seed", default=0,type=int,help="Seed to shuffle strings with")

	parser.add_argument('-s','--sample_number', dest="sample_number", default=100,type=int,help="How many dissimilar semvar string to process at most")
	args = parser.parse_args()
	
	selected_sentences = dict()
	selected_sentence_count = 0
	all_strings = dict()
	string_file_paths = [os.path.join(args.testset_dir,x) for x in os.listdir(args.testset_dir) if re.match(r"\d+\.\d+\.Eng.strings",x)]
	for string_file_path in string_file_paths:
		logging.info("reading lines from %s"%(string_file_path))
		#the string file format is shown above in the regex
		name_split = (string_file_path.split('/')[-1]).split('.')
		with open(string_file_path,'r') as string_file:
			all_strings[(name_split[0],name_split[1])] = string_file.readlines()

	for strings in all_strings.values():	
		shuffle(strings)

	#iterate the strings until enough sentences or remaining sentences not different enough
	iteration = 0
	#the key is template index
	#mix the list of semvarset to prevent uniformity
	template_groups = {k: list(v) for k,v in itertools.groupby(sorted(all_strings.keys(),key=lambda x: x[0]),key=lambda x: x[0])}
	for semvarsetlist in template_groups.values():
		shuffle(semvarsetlist)
	while selected_sentence_count < args.sentence_number or len(all_strings) == 0:
		iteration += 1
		logging.info("iteration %s, selected sentences %s"%(iteration,selected_sentence_count))

		#iterate template index groups
		for template_index in template_groups.keys():
			#list of semvar sets for the template list
			id_tuple_list = template_groups[template_index]

			#no semvar sets left, continue
			if len(id_tuple_list) == 0:
				continue
			
			#get the top one
			id_tuple = id_tuple_list.pop()
			
			#keep popping empty semvars out
			while id_tuple not in all_strings and len(id_tuple_list) > 0:
				id_tuple = id_tuple_list.pop()
				
			#if no non-empty semvar, continue
			if len(id_tuple_list) == 0:
				continue

			#stick the non-empty semvar to the start of the list
			id_tuple_list.insert(0,id_tuple)
				
			logging.info("Next id tuple: %s"%str(id_tuple))
				
			semvar_strings = all_strings[id_tuple]
			most_dissimilar = None
			removed_strings = 0
			dissimilars = 0
			logging.info("%s strings remaining for this semvar"%len(semvar_strings))
			for (string_index,semvar_string) in enumerate(semvar_strings):
				distance_to_closest_selected = 1
				#only do the edit distance calculation for sentences from same template.
				template_selected_sentences = list(chain.from_iterable([v for ((keytemplate,keysemvar),v) in selected_sentences.items() if keytemplate == template_index]))
				for selected_sentence in template_selected_sentences:	
					dist = edit_distance(semvar_string,selected_sentence)/len(semvar_string)
					#logging.info("comparing against %s, distance%s"%(selected_sentence,dist))
					distance_to_closest_selected = min(dist,distance_to_closest_selected)
				#if the distance is less than threshold, this will never be used, so remove it
				if distance_to_closest_selected < 1-args.max_similarity:
					#logging.info("removing string %s, too similar"%semvar_string)
					all_strings[id_tuple].remove(semvar_string)
					removed_strings += 1
				else:
					dissimilars += 1
				if most_dissimilar is None or most_dissimilar[0] < distance_to_closest_selected:
					most_dissimilar = (distance_to_closest_selected,semvar_string)
				#don't go through all the strings, takes too much time
				#if dissimilars > args.sample_number:
				#	logging.info("found enough dissimilar samples, adding the most dissimilar")
				#	break

				if string_index > args.sample_number:
					logging.info("processed specified number of samples (%s), moving on"%args.sample_number)
					break

			logging.info("removed %s strings for being too similar"%removed_strings)
			logging.info("%s strings remain for semvar"%len(all_strings[id_tuple]))
			logging.info("most dissimilar %s/%s"%most_dissimilar)
			if most_dissimilar[0] > 1-args.max_similarity:
				logging.info("adding %s to selected sentences, distance to closest %f"%(most_dissimilar[1],most_dissimilar[0]))
				if id_tuple not in selected_sentences:
					selected_sentences[id_tuple] = []
				selected_sentences[id_tuple].append(most_dissimilar[1])
				selected_sentence_count += 1
				if selected_sentence_count == args.sentence_number:
					break
			else:
				logging.info("removing %s from processing, all strings too similar"%str(id_tuple))
				del all_strings[id_tuple]

	source_strings_path = os.path.join(args.testset_dir,args.testset_name) + ".source.tok"	
	source_sentences_path = os.path.join(args.testset_dir,args.testset_name) + ".source.txt"	
	source_id_path = os.path.join(args.testset_dir,args.testset_name) + ".source.ids"
	#shuffle the sentence so similar sentences won't appear next to each other for the human translator
	testset_lines = []
	for (template_index,semvar_index),sentences in selected_sentences.items():
		for sentence in sentences:
			#the only punctuation is commas
			detok_sentence = sentence.replace(" ,",",")
			sentence_with_cap = sentence[0].upper()+detok_sentence[1:]
			period_sent = sentence_with_cap.replace("\n",".\n")
			source_id = template_index + '\t' + semvar_index + '\n'
			testset_lines.append((sentence,period_sent,source_id))
	shuffle(testset_lines)
	with open(source_strings_path,'w') as source_string_file, open(source_sentences_path,'w') as source_sents_file, \
		open(source_id_path,'w') as source_id_path:
		for (sentence,period_sent,source_id) in testset_lines:
			source_string_file.write(sentence)
			#the only punctuation is commas
			source_sents_file.write(period_sent)
			source_id_path.write(source_id)
		
	#strings = [(x,open(x,'r').readlines

		
		
