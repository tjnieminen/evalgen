#!/usr/bin/env python3
import glob
import logging
import argparse
import re
import random
import math
import os
import itertools

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',filename='error_scatter.log',level=logging.INFO,filemode='w')

def parse_errorlist(analysis_files):
	errors_per_system = dict()
	edit_re = re.compile(r"total score.*\nerror list\n(.*)",re.DOTALL)
	for analysis_file_path in analysis_files:
		print(analysis_file_path)
		with open(analysis_file_path,'r') as analysis_file:
			analysis_string = analysis_file.read()
		all_errors_search = edit_re.search(analysis_string)
		if all_errors_search:
			all_errors = {x[0]: int(x[1]) for x in [y.split(":") for y in all_errors_search.group(1).split("\n") if ":" in y]}	
			errors_per_system[os.path.basename(analysis_file_path)] = all_errors
		
	return errors_per_system	

def aggregate_errors(error_data):
	errors_per_system = {k: {"flu": 0,"ade": 0,"ins": 0,"del": 0,"sub": 0,"move": 0} for (k,v) in error_data.items()}
	fluency_edits = ["lemma_sub","comp_lemma_join","caseshift"]
	adequacy_edits = ["casenum_sub","tensenum_sub"]
	for (system,all_errors) in error_data.items():
		for fluency_edit_key in [x for x in all_errors.keys() if x in fluency_edits]:
			errors_per_system[system]["flu"] += all_errors[fluency_edit_key] 
		for adequacy_edit_key in [x for x in adequacy_edits if x in all_errors]:
			errors_per_system[system]["ade"] += all_errors[adequacy_edit_key]
		for del_edit in [x for x in ["delete","unkdel"] if x in all_errors]:
			errors_per_system[system]["del"] += all_errors[del_edit]
		for sub_edit in [x for x in ["sub","unksub"] if x in all_errors]:
			errors_per_system[system]["sub"] += all_errors[sub_edit]
		if "insert" in all_errors:	
			errors_per_system[system]["ins"] += all_errors["insert"]	
		if "move" in all_errors:	
			errors_per_system[system]["move"] += all_errors["move"]
		errors_per_system[system]["insdel"]=errors_per_system[system]["ins"]+errors_per_system[system]["del"]
	return errors_per_system	

def create_edit_table(edit_data,edit_table_path):
	sorted_edit_list = sorted(edit_data.items(),key=lambda x: x[0])
	for (system,system_edits) in sorted_edit_list:
		all_case_shift_count = sum([v for k,v in system_edits.items() if k.startswith("CASESHIFT")])
		system_edits["caseshift"] = all_case_shift_count
	#get all error types for y labels
	edit_types = set(itertools.chain.from_iterable([v.keys() for k,v in sorted_edit_list]))

	with open(edit_table_path,'w') as edit_table_file:
		#header row		
		header_row  = ",".join([""]+[x[0] for x in sorted_edit_list])
		edit_table_file.write(header_row+"\n")
		for edit in edit_types:
			#don't record specific case shifts here
			if not edit.startswith("CASESHIFT"):	
				edit_row = edit + "," + ",".join([str(x[1][edit]) if edit in x[1] else "0" for x in sorted_edit_list])
				edit_table_file.write(edit_row+"\n")
			

if __name__ == '__main__':
	parser = argparse.ArgumentParser(
		description=("Parses error lists from analyses into scatter plot data for Latex"),
		formatter_class=argparse.ArgumentDefaultsHelpFormatter)

	parser.add_argument('-a','--analysis_dir', dest="analysis_dir", default=".",type=str,help="The dir containing the analyses to generate error tables for")

	parser.add_argument('-d','--data_file_path', dest="data_file_path", default="scatter_plot.data",type=str,help="The file where the scatter plot data is stored")

	parser.add_argument('-e','--edit_table_path', dest="edit_table_path", default="edit_table.data",type=str,help="The file where the table of edits per system is stored")
	
	parser.add_argument('-g','--analysis_file_glob', dest="analysis_file_glob", default="*500.o3.m3.analysis",type=str,help="Pattern for analyses to include")
	args = parser.parse_args()

	analysis_files = glob.glob(args.analysis_dir+"/"+args.analysis_file_glob)
	
	error_data = parse_errorlist(analysis_files)
	create_edit_table(error_data,args.edit_table_path)
	aggregated = aggregate_errors(error_data)
	
	with open(args.data_file_path,'w') as data_file:
		write_header = True
		for system,errors in aggregated.items():
			sorted_errors = sorted(errors.items(),key=lambda x: x[0])	
			if write_header:
				data_file.write("name %s\n"%(" ".join([str(x) for (x,y) in sorted_errors])))
				write_header = False
			data_file.write("%s %s\n"%(system," ".join([str(y) for (x,y) in sorted_errors])))
