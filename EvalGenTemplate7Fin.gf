concrete EvalGenTemplate7Fin of EvalGenTemplate7 = open Prelude,LangFin,ParadigmsFin,SyntaxFin,ExtraFin,DictFin,(ResFin=ResFin),(StemFin=StemFin),StructuralFin in {
    param
        --This is to restrict the specification of the store, if already specified in matrix sentence
        Specificity = Specific|Unspecific;
        --This is block the double use of "yhtään", as "yhtään/niitä ei ollut myynnissä yhtään"
        Nonity = Nony|NotNone;
    oper
        compoundCN : CN -> CN -> CN = \cn1,cn2-> lin CN {
            s = \\nf => cn1.s ! ResFin.NCase ResFin.Sg ResFin.Nom ++ BIND ++ cn2.s ! nf ;
            h = cn2.h
            } ;
        storeref_rec : Type = {np : NP;specific : Specificity};
        itemref_rec : Type = {np : NP;none : Nonity};
        sub_rec : Type = {sent : S;specific : Specificity};
        but_Subj = mkSubj "mutta";
        
        selection_np : NP = variants
        {
            mkNP aSg_Det (mkN valikoima_NK);
            mkNP aPl_Det (mkN valikoima_NK);
            mkNP aSg_Det (mkN "tuote" (mkN valikoima_NK))
        };
        
        
        --TODO: use the nonity parameter to define the "yhtään" trailing adverbs
        
        generate_none_np_variants : NP -> NP = \np->
            variants
            {
                np ** {s = \\npform=> "yhtään" ++ np.s ! npform };
                np ** {s = \\npform=> np.s ! npform ++ "yhtään"};
                np
            };
            
        generate_none_adv_variants : Adv -> Nonity -> Adv = \adv,nonity->
            table
            {
                Nony => adv;
                NotNone => variants
                {
                    adv ** {s = "yhtään" ++ adv.s};
                    adv ** {s = adv.s ++ "yhtään"};
                    adv
                }
            } ! nonity;
            
        generate_none_adv2_variants : Adv -> Adv -> Nonity -> Adv = \adv1,adv2,nonity->
            table
            {
                Nony => variants {adv1 ** {s = adv1.s ++ adv2.s};adv2 ** {s = adv2.s ++ adv1.s}};
                NotNone => variants
                {
                    adv1 ** {s = "yhtään" ++ adv1.s ++ adv2.s};
                    adv1 ** {s = adv1.s ++ "yhtään" ++ adv2.s};
                    adv1 ** {s = adv1.s ++ adv2.s ++ "yhtään"};
                    adv1 ** {s = "yhtään" ++ adv2.s ++ adv1.s};
                    adv1 ** {s = adv2.s ++ "yhtään" ++ adv1.s};
                    adv1 ** {s = adv2.s ++ adv1.s ++ "yhtään"};
                    adv1 ** {s = adv1.s ++ adv2.s};
                    adv2 ** {s = adv2.s ++ adv1.s}
                }
            } ! nonity;
            
        
        --"kauppa ei myynyt niitä"
        plain_subclause : {np : NP;specific : Specificity} -> V2 -> {np : NP;none : Nonity} -> {sent : S;specific : Specificity} = \ref,v2,itemref->
            let
                np : NP = table
                {
                    Nony => itemref.np;
                    NotNone => generate_none_np_variants itemref.np
                } ! itemref.none;
                vp : VP = mkVP v2 np;
                sent : S = mkS pastTense simultaneousAnt negativePol (mkCl ref.np vp);
            in
                {sent = sent;specific = ref.specific};
        
        --"ne eivät kuuluneet sen valikoimaan" NOTE: no none_adv in here
        intrans_plain_subclause : {np : NP;none : Nonity} -> VP -> {np : NP;specific : Specificity} -> Pol -> Case-> {sent : S;specific : Specificity}= \itemref,vp,ref,pol,objcase->
            let
                selection_plain_adv : Adv = variants
                {
                    mkAdv (casePrep objcase) selection_np;
                };
                selection_gen_adv : Adv = selection_plain_adv ** {s = ref.np.s ! ResFin.NPCase ResFin.Gen ++ selection_plain_adv.s};
                selection_adv : Adv = table {
                    Unspecific => variants {selection_plain_adv;selection_gen_adv};
                    _ => selection_gen_adv
                } ! ref.specific;
                
                selection_adv_variant : Adv = variants
                {
                    selection_adv ** {s = selection_adv.s ++ "kokonaan"};
                    selection_adv ** {s = "kokonaan" ++ selection_adv.s};
                    selection_adv
                };
                
                sent : S = table
                {
                    NotNone => mkS pastTense simultaneousAnt pol (mkCl itemref.np (mkVP vp selection_adv_variant));
                    _ => variants {}
                } ! itemref.none;
            in
                {sent = sent;specific = ref.specific};
                
        add_gen_specifier_np : NP -> NP -> NP = \np1,np2-> np1 ** {s = \\x => np2.s ! ResFin.NPCase ResFin.Gen ++ np1.s ! x};
                
        
        intrans_reverse_subclause : {np : NP;none : Nonity} -> VP -> {np : NP;specific : Specificity} -> Pol -> Case-> {sent : S;specific : Specificity}= \itemref,vp,ref,pol,objcase->
            let
                singular_selection_np : NP = variants
                {
                    mkNP aSg_Det (mkN valikoima_NK);
                    mkNP aSg_Det (mkN "tuote" (mkN valikoima_NK))
                };
                
                plural_selection_np : NP = variants
                {
                    mkNP aPl_Det (mkN valikoima_NK);
                };
                
                singular_gen : NP = add_gen_specifier_np singular_selection_np ref.np;
                
                plural_gen : NP = add_gen_specifier_np plural_selection_np ref.np;
                
                
                singular_np : NP = table {
                    Unspecific => variants {singular_gen;singular_selection_np};
                    _ => singular_gen
                } ! ref.specific;
                
                plural_np : NP = table {
                    Unspecific => variants {plural_gen;plural_selection_np};
                    _ => plural_gen
                } ! ref.specific;
                
                itemref_adv : Adv = generate_none_adv_variants (mkAdv (casePrep objcase) itemref.np) itemref.none;
                
                --Some weird bug causes verb agreement to mess up if different sg/pl variants used
                sent : S = variants
                {
                    mkS pastTense simultaneousAnt pol (mkCl plural_np (mkVP vp itemref_adv));
                    mkS pastTense simultaneousAnt pol (mkCl singular_np (mkVP vp itemref_adv))
                };
            in
                {sent = sent;specific = ref.specific};
         
         
        forsale_adverb_subclause : {np : NP;specific : Specificity} -> VP -> {np : NP;none : Nonity} -> Adv -> {sent : S;specific : Specificity} = \ref,vp,itemref,forsaleadv->
            let
                itemref_adv : Adv = mkAdv (casePrep partitive) itemref.np;
                trailing_itemref_adv = generate_none_adv2_variants itemref_adv forsaleadv itemref.none;
                itemref_vp : VP = mkVP vp trailing_itemref_adv;
                
                shop_adv : Adv = table {
                    Unspecific => there_Adv;
                    _ => mkAdv (casePrep inessive) ref.np
                } ! ref.specific;
                
                trailing_shop_adv = generate_none_adv2_variants shop_adv forsaleadv itemref.none;
                shop_vp : VP = mkVP vp trailing_shop_adv;
                
                
                trailing_bare_adv = generate_none_adv_variants forsaleadv itemref.none;
                
                bare_vp : VP = mkVP vp trailing_bare_adv;
                sent : S = table {
                    Unspecific => variants 
                    {
                        mkS (itemref_adv) (mkS pastTense simultaneousAnt negativePol (mkCl bare_vp));
                        mkS (itemref_adv) (mkS pastTense simultaneousAnt negativePol (mkCl shop_vp));
                        mkS (shop_adv) (mkS pastTense simultaneousAnt negativePol (mkCl itemref_vp));
                    };
                    _ => variants 
                    {
                        mkS (itemref_adv) (mkS pastTense simultaneousAnt negativePol (mkCl shop_vp));
                        mkS (shop_adv) (mkS pastTense simultaneousAnt negativePol (mkCl itemref_vp));
                    }
                } ! ref.specific;
                
            in
                {sent = sent;specific = ref.specific};
                
        
        inselection_adverb_subclause : {np : NP;specific : Specificity} -> VP -> {np : NP;none : Nonity} -> Adv -> {sent : S;specific : Specificity} = \ref,vp,itemref,forsaleadv->
            let
                itemref_adv : Adv = mkAdv (casePrep partitive) itemref.np;
                trailing_itemref_adv : Adv = generate_none_adv_variants itemref_adv itemref.none;
                shop_adv : Adv = lin Adv {s = ref.np.s ! ResFin.NPCase ResFin.Gen ++ forsaleadv.s};
                trailing_shop_adv : Adv = generate_none_adv_variants shop_adv itemref.none;
                
                itemref_vp : VP = mkVP vp itemref_adv;
                sent : S = variants
                {
                    mkS (itemref_adv) (mkS pastTense simultaneousAnt negativePol (mkCl (mkVP vp trailing_shop_adv)));
                    mkS (shop_adv) (mkS pastTense simultaneousAnt negativePol (mkCl (mkVP vp trailing_itemref_adv)));
                    
                };
            in
                {sent = sent;specific = ref.specific};
                
        
        generate_vp_adv_variants : VP -> Adv -> Adv -> VP = \vp,adv1,adv2->
            let
                newvp : VP = variants
                {
                    mkVP (mkVP vp adv1) adv2;
                    mkVP (mkVP vp adv2) adv1;
                };
            in
                newvp;
        
        none_adv : Adv = variants
        {
            ParadigmsFin.mkAdv "yhtään";
            ParadigmsFin.mkAdv "lainkaan"
        };
        
        
        adverb_subclause : {np : NP;specific : Specificity} -> VP -> {np : NP;none : Nonity} -> {sent : S;specific : Specificity} = \ref,vp,itemref->
            let
                itemref_adv : Adv = mkAdv (casePrep partitive) itemref.np;
                trailing_itemref_adv : Adv = generate_none_adv_variants itemref_adv itemref.none;
                
                
                shop_adv : Adv = table {
                    Unspecific => there_Adv;
                    _ => mkAdv (casePrep inessive) ref.np
                } ! ref.specific;
                trailing_shop_adv : Adv = generate_none_adv_variants shop_adv itemref.none;
                
                shop_vp : VP = mkVP vp shop_adv;
                itemref_vp : VP = mkVP vp itemref_adv;
                
                sent : S = table {
                    Unspecific => variants
                    {
                        mkS (itemref_adv) (mkS pastTense simultaneousAnt negativePol (mkCl (mkVP vp trailing_shop_adv)));
                        mkS (shop_adv) (mkS pastTense simultaneousAnt negativePol (mkCl (mkVP vp trailing_itemref_adv)));
                        mkS (itemref_adv) (mkS pastTense simultaneousAnt negativePol (mkCl vp));
                    };
                    _ => variants
                    {
                        mkS (itemref_adv) (mkS pastTense simultaneousAnt negativePol (mkCl (mkVP vp trailing_shop_adv)));
                        mkS (shop_adv) (mkS pastTense simultaneousAnt negativePol (mkCl (mkVP vp trailing_itemref_adv)));
                    }
                } ! ref.specific;
            in
                {sent = sent;specific = ref.specific};
                
        passive_subclause : {np : NP;specific : Specificity} -> VP -> {np : NP;none : Nonity} -> {sent : S;specific : Specificity} = \ref,vp,itemref->
            let
                itemref_adv : Adv = mkAdv (casePrep partitive) itemref.np;
                shop_adv : Adv = table {
                    Unspecific => there_Adv;
                    _ => mkAdv (casePrep inessive) ref.np
                } ! ref.specific;
                
                trailing_shop_adv = generate_none_adv_variants shop_adv itemref.none;
                trailing_itemref_adv = generate_none_adv_variants itemref_adv itemref.none;
                
                sent : S = table
                {
                    Specific => variants
                    {
                        mkS itemref_adv (mkS pastTense simultaneousAnt negativePol (mkCl (mkVP vp trailing_shop_adv)));
                        mkS shop_adv (mkS pastTense simultaneousAnt negativePol (mkCl (mkVP vp trailing_itemref_adv)))
                    };
                    Unspecific => variants
                    {
                        mkS itemref_adv (mkS pastTense simultaneousAnt negativePol (mkCl vp)); --no need to specify with unspecific
                        mkS itemref_adv (mkS pastTense simultaneousAnt negativePol (mkCl (mkVP vp trailing_shop_adv)));
                        mkS shop_adv (mkS pastTense simultaneousAnt negativePol (mkCl (mkVP vp trailing_itemref_adv)))
                    }
                } ! ref.specific;
            in
                {sent = sent;specific = ref.specific};

        impersonal_subclause : {np : NP;specific : Specificity} -> VP -> {np : NP;none : Nonity} -> {sent : S;specific : Specificity} = \ref,vp,itemref->
            let
                itemref_adv : Adv = mkAdv (casePrep partitive) itemref.np;
                trailing_itemref_adv = generate_none_adv_variants itemref_adv itemref.none;
                
                shop_adv : Adv = table {
                    Unspecific => there7from_Adv;
                    _ => mkAdv (casePrep elative) ref.np
                } ! ref.specific;
                trailing_shop_adv = generate_none_adv_variants shop_adv itemref.none;
                
                sent : S = variants {
                    mkS itemref_adv (mkS pastTense simultaneousAnt negativePol (mkCl (mkVP vp trailing_shop_adv)));
                    mkS shop_adv (mkS pastTense simultaneousAnt negativePol (mkCl (mkVP vp trailing_itemref_adv)))
                };
            in
                {sent = sent;specific = ref.specific};
                
        --"niitä ei kuulunut"
        impersonal_inclusion_subclause : {np : NP;specific : Specificity} -> VP -> {np : NP;none : Nonity} -> {sent : S;specific : Specificity} = \ref,vp,itemref->
            let    
                selection_plain_adv : Adv = variants
                {
                    mkAdv (casePrep illative) selection_np;
                };
                selection_gen_adv : Adv = selection_plain_adv ** {s = ref.np.s ! ResFin.NPCase ResFin.Gen ++ selection_plain_adv.s};
                selection_adv : Adv = table {
                    Unspecific => variants {selection_plain_adv;selection_gen_adv};
                    _ => selection_gen_adv
                } ! ref.specific;
                
                trailing_selection_adv : Adv = generate_none_adv_variants selection_adv itemref.none;
                itemref_adv : Adv = mkAdv (casePrep partitive) itemref.np;
                trailing_itemref_adv : Adv = generate_none_adv_variants itemref_adv itemref.none;
                sent : S = variants {
                    mkS itemref_adv (mkS pastTense simultaneousAnt negativePol (mkCl (mkVP vp trailing_selection_adv)));
                    mkS selection_adv (mkS pastTense simultaneousAnt negativePol (mkCl (mkVP vp trailing_itemref_adv)))
                };
            in
                {sent = sent;specific = ref.specific};
                
        generate_for_variants : NP -> NP -> {np : NP;trailing_adv : Adv} = \np1,np2-> variants
        {
            {np = np1; trailing_adv = mkAdv (casePrep partitive) (mkNP np2 (ParadigmsFin.mkAdv "varten"))};
            {np = np1; trailing_adv = mkAdv for_Prep np2};
        };
                
        empty_adv = lin Adv {s = ""}; 
                
    lincat
        IndexedStatement = Str;
        VolitionNoun = CN;
        VolitionVerb = VV;
        Statement = S;
        Customer = {np : NP;poss : Quant};
        Store = NP;
        Item = {np : NP; trailing_adv : Adv};
        StoreReferent = storeref_rec;
        Subclause = sub_rec;
        ItemReferent = itemref_rec;
        ForSaleVerb = V2;
        AvailableVerb = V;
        ForSaleAdverb = Adv;
        ForSaleWrapper = Adv;
        HaveVerb = V;
        Media = CN;
        MediaOwner = Quant;
        InclusionVerb = V;
        ExclusionVerb = V;
        IncludeVerb = V;
        Drink = CN;
        JuiceBase = CN;
        
    lin
        SemVar3dDrinkCan drink =
            let
                np : NP = variants
                {
                    mkNP aSg_Det (compoundCN drink (mkCN (mkN to'lkki_NK)));
                    mkNP (mkNP (mkN to'lkki_NK)) (mkAdv (casePrep partitive) (mkNP drink));
                    mkNP (mkNP (mkN "tölkillinen")) (mkAdv (casePrep partitive) (mkNP drink));
                };
            in
                {np = np;trailing_adv = empty_adv};
        SemVar7aApple = mkCN (mkN omena_NK);
        SemVar7bOrange = mkCN (mkN appelsiini_NK);
        SemVar7cGrape = variants {mkCN (mkN rypa'le_NK);compoundCN (mkCN (mkN viini_1_NK)) (mkCN (mkN rypa'le_NK))};
        SemVar7dCarrot = mkCN (mkN (porkkana_NK));
        SemVar7ePear = mkCN (mkN (pa'a'ryna'_NK));
        SemVar6bJuiceFrom base = compoundCN base (mkCN (mkN mehu_NK));
        SemVar6aSoda = mkCN (mkN "virvoitusjuoma");
        AddStatementIndex i s = i.s ++ s.s;
        Include = mkV sisa'lta'a'_VK;
        Miss = mkV puuttua_VK;
        Intention = variants
        {
            mkCN (mkN aikomus_NK);
            mkCN (mkN tarkoitus_NK)
        };
        Belong = variants
        {
            mkV kuulua_VK;
            mkV sisa'ltya'_VK
        };
        WrapForSale forsale = forsale;
        Want = mkVV (mkV haluta_VK);
        BeAvailable = mkV saada_VK;
        Intend = mkVV (mkV aikoa_VK);
        InStock = mkAdv in_Prep (mkNP (mkN varasto_NK));
        InSelection = mkAdv in_Prep selection_np;
        ForSale = variants
        {
            mkAdv in_Prep (mkNP (mkN myynti_NK));
            mkAdv (casePrep genitive) (mkNP (mkN kauppa_NK));
            mkAdv (casePrep essive) (mkNP (mkN "myytävä"));
        };
        Them =
            let
                itemref_variant : {np :NP;none : Nonity} = variants
                {
                    {np = mkNP aSg_Det (mkN sellainen_NK); none = Nony};
                    {np = mkNP aPl_Det (mkN sellainen_NK); none = NotNone};
                    {np = mkNP ne_Pron; none = NotNone}
                    
                };
            in
                itemref_variant;
        
        None =
            let
                str_variant : Str = variants {"yhtäkään";"yhtään"};
                np : NP = lin NP {s = \\_=> str_variant; a = ResFin.Ag singular ResFin.P3; isPron = False; isNeg = False};
            in
                {np = np;none = Nony};
        NoneOfThem =
            let
                str_variant : Str = variants {"yhtäkään";"yhtään"};
                np : NP = lin NP {s = \\_=> str_variant ++ "sellaista"; a = ResFin.Ag singular ResFin.P3; isPron = False; isNeg = False};
            in
                {np = np;none = Nony};
        
        
        Have = vOlla;
        Sell = mkV2 myyda'_VK;
        --Carry = carry_V2;
        SemVar1aGenericStore = variants
        {
            mkNP the_Det (mkN (kauppa_NK));
            mkNP the_Det (mkN (liike_NK));
            mkNP the_Det (mkN (myyma'la'_NK))
        };
        StoreToStoreReferent store = {np = store;specific = Specific};
        --StoreToStoreReferentAdverb store = {np = store;specific = AdverbSpecified};
        UnspecifiedStore = {np = it_NP;specific=Unspecific};
        SemVar2aWe = variants 
        {
            {np = mkNP (ProDrop we_Pron);poss = mkQuant we_Pron};
            {np = we_NP; poss = ProDropPoss we_Pron}
        };
        SemVar3aCassettePlayer =
            let
                empty_adv = lin Adv {s = ""};
            in
                variants
                {
                    generate_for_variants (mkNP (mkN soitin_NK)) (mkNP aPl_Det (mkN kasetti_NK));
                    {np = mkNP (mkN "kasetti" (mkN soitin_NK));trailing_adv=empty_adv};
                    {np = mkNP (mkN "kasetti" (mkN nauhuri_NK));trailing_adv=empty_adv};
                    {np = mkNP (mkN "ääni" (mkN nauhuri_NK));trailing_adv=empty_adv};
                    {np = mkNP (GenNP (mkNP aPl_Det (mkN kasetti_NK))) (mkN "toistolaite");trailing_adv=empty_adv}
                };
        SemVar3bPlayerFor media mediaowner = generate_for_variants (mkNP aSg_Det (mkCN (mkCN (mkN soitin_NK)))) (mkNP mediaowner pluralNum media);
        SemVar3cPlayerForOld media mediaowner =
            let
                old_media : CN = mkCN (mkA vanha_NK) media;
            in
                generate_for_variants (mkNP aSg_Det (mkCN (mkCN (mkN soitin_NK)))) (mkNP  mediaowner pluralNum old_media);
        
        SemVar4aCassetteMedia = mkCN (mkN kasetti_NK);
        SemVar5aMe = variants
        {
            (ProDropPoss i_Pron);
            (GenNP i_NP)
        };
        
        --"halusimme/aioimme"
        ActiveTransitiveVolitionVerbInfinitivalSubordinateClause0Subject_Customer1Verb_Volition2Verb3InfinitivalObject_Item4Source_Store3Subordinate_StoreSubclause5 customer volition_verb item store sub =
            let
                buy_VK : VK = variants {ostaa_VK;hankkia_VK};
                joined_item_np : NP = mkNP item.np item.trailing_adv;
                inner_vp : VP = mkVP (mkV2 (mkV buy_VK)) joined_item_np;
                inner_stored_vp : VP = variants
                {
                    mkVP (mkVP (mkVP (mkV buy_VK)) (mkAdv (casePrep elative) store)) (mkAdv (casePrep genitive) joined_item_np);
                    mkVP (mkVP (mkV2 (mkV buy_VK)) joined_item_np) (mkAdv from_Prep store);
                    mkVP (mkVP (mkVP (mkV2 (mkV buy_VK)) item.np) (mkAdv from_Prep store)) item.trailing_adv
                };
                
                outer_vp : VP = mkVP volition_verb inner_vp;
                outer_stored_vp : VP = mkVP volition_verb inner_stored_vp;
                
                subbed_vp_stored_adv : VP = mkVP outer_stored_vp (mkAdv but_Subj sub.sent);
                subbed_vp : VP = mkVP outer_vp (mkAdv but_Subj sub.sent);
                
            in
                table
                {
                    Unspecific => mkS pastTense simultaneousAnt positivePol(mkCl customer.np subbed_vp_stored_adv);
                    _ => mkS pastTense simultaneousAnt positivePol(mkCl customer.np subbed_vp)
                } ! sub.specific;
                
        --meidän oli määrä
        ActiveTransitiveSpecialFinnishVolitionNounInfinitivalSubordinateClause0Subject_Customer1Verb2InfinitivalObject_Item4Source_Store3Subordinate_StoreSubclause5
        customer item store sub =
            let
                buy_VK : VK = variants {ostaa_VK;hankkia_VK};
                joined_item_np : NP = mkNP item.np item.trailing_adv;
                inner_vp : VP = mkVP (mkV2 (mkV buy_VK) nominative) joined_item_np;
                inner_stored_vp : VP = variants
                {
                    mkVP (mkVP (mkVP (mkV buy_VK)) (mkAdv (casePrep elative) store)) (mkAdv (casePrep nominative) joined_item_np);
                    mkVP (mkVP (mkV2 (mkV buy_VK) nominative) joined_item_np) (mkAdv from_Prep store);
                    mkVP (mkVP (mkVP (mkV2 (mkV buy_VK) nominative) item.np) (mkAdv from_Prep store)) item.trailing_adv
                };
                
                volition_n : N = variants
                {
                    mkN ma'a'ra'_NK;
                    mkN tarkoitus_NK
                };
                
                outer_vp : VP = mkVP (mkNP aSg_Det (mkCN volition_n inner_vp));
                outer_stored_vp : VP = mkVP (mkNP aSg_Det (mkCN volition_n inner_stored_vp));
                
                subbed_vp_stored_adv : VP = mkVP outer_stored_vp (mkAdv but_Subj sub.sent);
                subbed_vp : VP = mkVP outer_vp (mkAdv but_Subj sub.sent);
                
                gen_subj_adv : Adv = mkAdv (casePrep genitive) customer.np;
                
            in
                table
                {
                    Unspecific => mkS gen_subj_adv (mkS pastTense simultaneousAnt positivePol(mkCl subbed_vp_stored_adv));
                    _ => mkS gen_subj_adv (mkS pastTense simultaneousAnt positivePol(mkCl subbed_vp))
                } ! sub.specific;
        
        --"meillä oli tarkoitus"
        ActiveTransitiveVolitionVerbInfinitivalSubordinateClause0Subject_Customer1Noun_Volition2Verb3InfinitivalObject_Item4Source_Store3Subordinate_StoreSubclause5 customer volition_noun item store sub =
            let
                buy_VK : VK = variants {ostaa_VK;hankkia_VK};
                joined_item_np : NP = mkNP item.np item.trailing_adv;
                inner_vp : VP = mkVP (mkV2 (mkV buy_VK) nominative) joined_item_np;
                inner_stored_vp : VP = variants
                {
                    mkVP (mkVP (mkVP (mkV buy_VK)) (mkAdv (casePrep elative) store)) (mkAdv (casePrep nominative) joined_item_np);
                    mkVP (mkVP (mkV2 (mkV buy_VK) nominative) joined_item_np) (mkAdv from_Prep store);
                    mkVP (mkVP (mkVP (mkV2 (mkV buy_VK) nominative) item.np) (mkAdv from_Prep store)) item.trailing_adv
                };
                
                outer_vp : VP = mkVP have_V2 (mkNP aSg_Det (mkCN volition_noun inner_vp));
                outer_stored_vp : VP = mkVP have_V2 (mkNP aSg_Det (mkCN volition_noun inner_stored_vp));
                
                subbed_vp_stored_adv : VP = mkVP outer_stored_vp (mkAdv but_Subj sub.sent);
                subbed_vp : VP = mkVP outer_vp (mkAdv but_Subj sub.sent);
                
            in
                table
                {
                    Unspecific => mkS pastTense simultaneousAnt positivePol(mkCl customer.np subbed_vp_stored_adv);
                    _ => mkS pastTense simultaneousAnt positivePol(mkCl customer.np subbed_vp)
                } ! sub.specific;
        
        --"tarkoituksemme oli"
        ActiveTransitiveVolitionVerbInfinitivalSubordinateClause0Possessive_Customer1Noun_Volition2Verb3InfinitivalObject_Item4Source_Store3Subordinate_StoreSubclause5 customer volition_noun item store sub =
            let
                buy_VK : VK = variants {ostaa_VK;hankkia_VK};
                joined_item_np : NP = mkNP item.np item.trailing_adv;
                inner_vp : VP = mkVP (mkV2 (mkV buy_VK) nominative) joined_item_np;
                inner_stored_vp : VP = variants
                {
                    mkVP (mkVP (mkVP (mkV buy_VK)) (mkAdv (casePrep elative) store)) (mkAdv (casePrep nominative) joined_item_np);
                    mkVP (mkVP (mkV2 (mkV buy_VK) nominative) joined_item_np) (mkAdv from_Prep store);
                    mkVP (mkVP (mkVP (mkV2 (mkV buy_VK) nominative) item.np) (mkAdv from_Prep store)) item.trailing_adv
                };
                
                subbed_vp_stored_adv : VP = mkVP inner_stored_vp (mkAdv but_Subj sub.sent);
                subbed_vp : VP = mkVP inner_vp (mkAdv but_Subj sub.sent);
                
                poss_customer_intention : NP = mkNP customer.poss volition_noun;
            in
                table
                {
                    Unspecific => mkS pastTense simultaneousAnt positivePol(mkCl poss_customer_intention (mkVV vOlla) subbed_vp_stored_adv);
                    _ => mkS pastTense simultaneousAnt positivePol(mkCl poss_customer_intention (mkVV vOlla) subbed_vp)
                } ! sub.specific;
        
        --"kauppa ei myynyt niitä"
        SubclauseActiveTransitive0Subject_Store1Verb_ForSaleVerb2Object_ItemReferent3 ref verb itemref =
            plain_subclause ref verb itemref;
        
        --"ne eivät kuuluneet valikoimaan"
        SubclauseActiveIntransitive0Subject_ItemReferent1Verb_InclusionVerb2Adverb_Store3 itemref verb ref =
            intrans_plain_subclause itemref (mkVP verb) ref negativePol illative;
        
        --"niitä ei myyty siellä"
        SubclausePassiveTransitive0Adverbial_Store1Verb_ForSaleVerb2Object_ItemReferent3 ref verb itemref =
            passive_subclause ref (passiveVP verb) itemref;
        
        --"niitä ei ollut myynnissä/valikoimassa/varastossa kaupassa"
        --This is the generic case for ForSaleAdverbs, dependent type functions are used for specific cases, e.g. "kaupan valikoimassa", which applies only to InSelection.
        SubclauseActiveTransitive0Subject_Store1Verb_HaveVerb2Object_ItemReferent3Adverb_ForSaleAdverb4 ref verb itemref forsaleadverb =
            forsale_adverb_subclause ref (mkVP verb) itemref forsaleadverb;
        
        --"kaupan valikoimassa ei ollut niitä", which applies only to InSelection.
        SubclauseActiveTransitive0Subject_Store1Verb_HaveVerb2Object_ItemReferent3Adverb_InSelectionAdverb4 ref verb itemref forsaleadverb =
            inselection_adverb_subclause ref (mkVP verb) itemref forsaleadverb;
            
        --"kaupassa ei ollut niitä"
        SubclauseActiveTransitive0Subject_Store1Verb_HaveVerb2Object_ItemReferent3 ref verb itemref =
            adverb_subclause ref (mkVP verb) itemref;
        
        --"niitä ei saanut sieltä"
        SubclausePassiveTransitive0Adverbial_Store1Verb_AvailableVerb2Object_ItemReferent3 ref verb itemref =
            impersonal_subclause ref (mkVP verb) itemref;
        
        --"ne puuttuivat valikoimasta"
        SubclauseActiveTransitive0Subject_ItemReferent1Verb_ExclusionVerb2Adverb_Store3 itemref verb ref =
            intrans_plain_subclause itemref (mkVP verb) ref positivePol elative;
        
        
        
        --"valikoima ei sisältänyt"
        SubclauseActiveIntransitive0Verb_InclusionVerb1Object_ItemReferent2Adverb_Store3 verb itemref ref =
            intrans_reverse_subclause itemref (mkVP verb) ref negativePol partitive;
            
        SubclausePassiveTransitive0Adverbial_Store1Verb_InclusionVerb2Object_ItemReferent3 ref verb itemref =
            impersonal_inclusion_subclause ref (mkVP verb) itemref;
        
       }   