#!/usr/bin/env python3
import glob
import logging
import argparse
import re
import random
import math
import sys

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',filename='error_quiz.log',level=logging.INFO,filemode='w')

class Segment():
	def __init__(self,segment_id,source,hyp,edits,ref,score,errors):
		self.segment_id = segment_id
		self.source = source
		self.hyp = hyp.replace(", ","").replace(" -","")
		self.hyp_tokens = self.hyp.split()
		self.edits = edits
		self.insert_count = len([x for x in self.edits.split() if x.startswith("[insert:")])
		self.full_edits = re.findall(r"\[[^]]+\]",self.edits) 
		self.edit_tokens = re.findall(r"\[[^:]+: ([^/]+)[^]]+\]",self.edits)
		self.edit_token_indices = [self.hyp.split().index(x) for x in self.edit_tokens if x in self.hyp.split()]
		self.ref = ref
		self.score = score
		self.errors = int(re.sub(r"\..+$","",errors))

	def __str__(self):
		return self.segment_id

class Score():
	def __init__(self):
		self.truepos = 0
		self.falsepos = 0
		self.falseneg = 0

	def increment(self,truepos,falsepos,falseneg):
		self.truepos += truepos
		self.falsepos += falsepos
		self.falseneg += falseneg

	def __str__(self):
		posneg = "truepos: %s. falsepos: %s. falseneg: %s"%(self.truepos,self.falsepos,self.falseneg)
		precision = self.truepos/(self.truepos+self.falsepos)
		recall = self.truepos/(self.truepos+self.falseneg)
		precrec = "precision: %.3f recall: %.3f"%(precision,recall)
		return "%s\n%s"%(posneg,precrec)

def parse_segment(analysis_files):
	segment_re = re.compile(
		r"segment_id: (?P<segmentid>.+)\n" +
		r"source: (?P<sourcetext>.+)\n" +
		r"hyp: (?P<hypothesis>.+)\n" +
		r"edits: (?P<edits>.+)\n" +
		r"ref: (?P<ref>.+)\n" +
		r"score: (?P<score>.+)\n" +
		r"sentence errors: (?P<errors>.+)\n")
	segments = []
	for analysis_file_path in analysis_files:
		with open(analysis_file_path,'r') as analysis_file:
			analysis_string = analysis_file.read()
			for match in segment_re.finditer(analysis_string):
				segments.append(Segment(*match.groups()))
	return segments

def get_quiz_set(segments,minerr,maxerr,amount):
	random.shuffle(segments)
	used_edits = set()
	chosen_segments = []
	valid_segments = [x for x in segments if x.errors <= maxerr and x.errors >= minerr and x.insert_count > 0]

	#only include segments with different edits
	for segment in valid_segments:
		#skip segmetns with moves
		if [x for x in segment.full_edits if x.startswith("[move")]: 
			logging.info("Skipping segment due to move edits: %s"%(str(segment.full_edits)))
			continue
		if not used_edits & set(segment.full_edits):
			chosen_segments.append(segment)
			if len(chosen_segments) == amount:
				break
			used_edits |= set(segment.full_edits)
		else:
			logging.info("Skipping segment due to repeated edits: %s"%(str(segment.full_edits)))

	return chosen_segments

def make_index_string(token,index):
	space_string = [" "] * len(token)
	#with even token length it's better to have the index on the left, looks better
	is_odd = not (len(token) % 2)
	space_string[math.floor(len(token)/2)-int(is_odd)] = str(index)
	joined_string = "".join(space_string)
	#truncate to length of token
	return joined_string[0:len(token)]

def display_segment(segment):	
	print("\033[H\033[J")
	print()
	hypothesis_indices = " ".join([make_index_string(x,k) for k,x in enumerate(segment.hyp.split())])
	source_line = "source sentence: " + segment.source
	hypothesis_line = "hypothesis: " + segment.hyp
	logging.info(source_line)
	logging.info(hypothesis_line)
	print(source_line)
	print("")
	print("-" * (max(len(hypothesis_indices),len(source_line))))
	print()
	print(hypothesis_line)
	print((" " * len("hypothesis: ")) + hypothesis_indices)

def score_segment(segment,user_response):	
	truepos,falsepos,falseneg = 0,0,0
	error_indices = user_response.split() 
	user_inserts = len([x for x in error_indices if x == "i"])
	edit_token_indices = set(segment.edit_token_indices)
	for error_indice in [x for x in error_indices if x.isdigit()]:
		logging.info("Marked position is %s and the token is %s"%(error_indice,segment.hyp_tokens[int(error_indice)]))
		if int(error_indice) in edit_token_indices:
			edit_token_indices.remove(int(error_indice))
			logging.info("Marked position is correct")
			truepos += 1
		else:
			logging.info("Marked position is incorrect")
			falsepos += 1
	
	insert_mismatch = int(user_inserts)-segment.insert_count
	if insert_mismatch < 0:
		logging.info("%d inserts unmarked"%abs(insert_mismatch))
		falseneg -= insert_mismatch
	elif insert_mismatch > 0:
		logging.info("%d extra inserts marked"%abs(insert_mismatch))
		falsepos += insert_mismatch
	
	logging.info("%d inserts marked correctly"%(max(0,user_inserts-abs(insert_mismatch))))
	truepos += max(0,user_inserts - abs(insert_mismatch))
	
	logging.info("%d sub or del edits remain unmarked"%len(edit_token_indices))
	falseneg += len(edit_token_indices)
	precision = truepos/(truepos+falsepos)
	total_edits = truepos + falseneg
	recall = truepos/(len(segment.edit_token_indices)+segment.insert_count)
	logging.info("precision: %.3f recall: %.3f number of edits: %d"%(precision,recall,total_edits))
	logging.info(user_response)
	logging.info(segment.edits)
	return (truepos,falsepos,falseneg)

def validate_input(user_input,segment):
	correct_format = re.fullmatch(r"^((\d+|i) *)*",user_input)
	if not correct_format:
		return (False,"input format not correct")
	else:
		error_indices = [x for x in user_input.split() if x != " " and not x == "i"]
		if [x for x in error_indices if int(x) > len(segment.hyp_tokens)-1]:
			return (False,"error index too high")
	return (True,None)
		

def run_quiz(quiz_set):	
	print("\033[H\033[J")
	input("Instructions: Please enter the indices of the words that should be changed or removed in order to make the sentence correct. If words should be added to the sentence to correct it, specify the number of added words by entering letter 'i's. For instance, if 1 word should be added, write 'i', if two words should be added write 'i i'. Press any key to start.")
	score = Score()	
	for segment in quiz_set:
		user_response = None
		while user_response is None:
			display_segment(segment)
			user_response = input()
			(valid,error) = validate_input(user_response,segment)
			if valid:
				score.increment(*score_segment(segment,user_response))
				input()
			else:
				input(error)
				user_response = None

	logging.info(str(score))
				

if __name__ == '__main__':
	parser = argparse.ArgumentParser(
		description=("Runs a quiz where the user is asked to mark error loci in MT hypotheses, in which EvalGen has detected n to m errors."),
		formatter_class=argparse.ArgumentDefaultsHelpFormatter)

	parser.add_argument('-a','--analysis_dir', dest="analysis_dir", default="quiz_analyses",type=str,help="The dir containing the analyses where the quiz materials will be extracted")

	parser.add_argument('-r','--result_file', dest="result_file", default="error_quiz.results",type=str,help="The name of the file in which the results of the quiz are stored (precision and recall per sentence, and summary)")

	parser.add_argument('-n','--min_errors', dest="min_errors", default=1,type=int,help="How many EvalGen errors at a minimum should the hypotheses contain")

	parser.add_argument('-m','--max_errors', dest="max_errors", default=3,type=int,help="How many EvalGen errors at a maximum should the hypotheses contain")
	
	parser.add_argument('-s','--amount', dest="amount", default=10,type=int,help="How many hypotheses to quiz")
	
	parser.add_argument('-e','--seed', dest="seed", default=0,type=int,help="Random seed for quiz set order")
	args = parser.parse_args()

	random.seed(args.seed)
	analysis_files = glob.glob(args.analysis_dir+"/*analysis")
	
	segments = parse_segment(analysis_files)
	quiz_set = get_quiz_set(segments,args.min_errors,args.max_errors,args.amount)

	run_quiz(quiz_set)
