concrete EvalGenTemplate4Fin of EvalGenTemplate4 = open Prelude,ParamX,(M = MorphoFin),SyntaxFin,ParadigmsFin,DictFin,SentenceFin,NounFin,RelativeFin,(StemFin = StemFin),(R = ResFin) in {
    param
        VerbinessOfNoun = Minen | NoStiAdverbs | Va; --noun derived from verbs have a variabled degree of verb features
        
        NegativeHealthOutcomeParam = NameNp | InchoativeNp;
        AdverbOrder = FreeAdverbial | BackAdverbial;
    oper
        sverb2nounPresPartAct : StemFin.SVerb1 -> StemFin.SNoun = \v ->  -- syövä
            let teke = Predef.tk 5 (v.s ! R.Inf R.Inf3Iness) in 
            StemFin.nforms2snoun (StemFin.dLava (teke + "v" + last (v.s ! ResFin.Inf ResFin.Inf1)));
            
        relative_head_variant : CN =
                    mkCN 
                        (mkN
                            (variants
                                {
                                    ihminen_NK;
                                    henkilo'_NK;
                                }
                            )
                        );
        --the relative constructor in the library slows down linking immensely, this is a simpler version
        simple_relative : NP -> VP -> NP = \np,v->
            lin NP
            {
                s = case np.a of
                    {
                        R.Ag Sg _ => \\npform => np.s ! npform ++ "," ++ "joka" ++ v.s.s ! R.Presn Sg P3 ++ v.adv ! Pos ++ ",";
                        R.Ag Pl _ => \\npform => np.s ! npform ++ "," ++ "jotka" ++ v.s.s ! R.Presn Pl P3 ++ v.adv ! Pos ++ ",";
                        _ => np.s
                    };
                a=np.a;
                isPron=np.isPron;
                isNeg=np.isNeg;
            };
        
        negative_health_np : Type = {np : NP;neg_param : NegativeHealthOutcomeParam; compound_prefix : Str};
        verby_noun : Type = {verbiness : VerbinessOfNoun;cn : CN;v : V};
        effort_rec : Type = {soa : NP;effort : V;effort_nom : NP};
        --got an error when trying to extract complement from VP, so pass it as an argument
        mkN2IsPre : N -> Prep -> Bool -> N2 = \n,c,p -> n ** {c2 = c ; isPre = p ; lock_N2 = <>} ; --normal N2 constructor always places adv last, this has switch
        
        nominalize : V -> NP -> Case -> CN = \vpi,comp,cas->
            let
                infnom : N = mkN vpi;
                n2 : N2 = mkN2IsPre infnom (casePrep cas) False;
            in
                mkCN (mkCN n2 comp) (ParadigmsFin.mkAdv vpi.p);--the adv is a potential verb particle, it won't mostly exist                    
            
        --this version takes an AP, it's for cases where the AP can be on either side of fronted complement
        nominalizeWithAP : V -> NP -> Case -> AP -> CN = \vpi,comp,cas,ap->
            let
                infnom : N = mkN vpi;
            in
                variants
                {
                    --mkCN (mkCN ap (mkCN (mkN2IsPre infnom (casePrep cas) False) comp)) (ParadigmsFin.mkAdv vpi.p); --the adv is a potential verb particle, it won't mostly exist
                    --Need an mkCN constructor that adds the AP in front of the head, so N2 -> AP -> NP, "väliensä julkisesta korjaamisesta"
                    mkCN (mkCNN2AP (mkN2IsPre infnom (casePrep cas) False) comp ap) (mkAdv vpi.p);
                    --.mkCN (mkAdvCNReverse (SyntaxFin.mkAdv (casePrep cas) comp) (mkCN ap infnom)) (mkAdv vpi.p);
                };
                
        
        mkSSubjReverse : Subj -> S -> S -> S = \subj,b,a-> lin S {s = subj.s ++ b.s ++ "," ++ a.s};
        mkAdvCNReverse : Adv -> CN -> CN = \ad,cn-> lin CN {s = \\nf => ad.s ++ cn.s ! nf ; h = cn.h};
        
        mkSPostAdv : S -> Adv -> S = \s,a-> lin S {s = s.s ++ a.s};
        
        --this is an adverbial clause containing a subjunctive, e.g. "siitä huolimatta, että X"
        mkAdvSubj : Str -> Subj -> S -> Adv = \head,subj,s -> lin Adv {
            s = head ++ "," ++ subj.s ++ s.s ++ ",";
        };
        possessiveSuffixPron : {s : ResFin.NPForm => Str ; a : ResFin.Agr ; hasPoss : Bool ; poss : Str} = 
            {s = table {_=>[]};a = ResFin.Ag Pl P3; hasPoss = True; poss = []};
        
        vpToAdv : R.InfForm -> VP -> Adv = \inff,vp-> lin Adv {
            s = vp.s.s ! R.Inf inff ++ vp.s2 ! False ! ParamX.Pos ! ResFin.Ag Sg P3 ++ vp.adv ! ParamX.Pos;
        };
        
        predVPVariants : AdverbOrder -> NP -> VP -> Cl = \advorder,np,vp-> mkClauseVariants advorder (orB np.isNeg vp.vptyp.isNeg) (R.subjForm np vp.s.sc) np.a vp;
        
        --This is mkClause with a possibility to define restrictions on phrase order variants
        mkClauseVariants : AdverbOrder -> Bool -> (Polarity -> Str) -> R.Agr -> VP -> R.Clause =
            \advorder,isNeg,sub,agr,vp -> {
              s = \\t,a,b => 
                 let
                   pol = case isNeg of {
                    True => Neg ; 
                    _ => b
                    } ; 
                   c = (StemFin.mkClausePlus sub agr vp).s ! t ! a ! pol 
                 in 
                 table {
                   SDecl  => case advorder of
                        {
                            FreeAdverbial =>  variants
                                {
                                    c.subj ++ c.adv ++ c.fin ++ c.inf ++ c.compl ++ c.ext;
                                    c.subj ++ c.fin ++ c.inf ++ c.compl ++ c.adv ++ c.ext;
                                    c.subj ++ c.fin ++ c.inf ++ c.adv ++ c.compl ++ c.ext;
                                };
                            BackAdverbial => c.subj ++ c.fin ++ c.inf ++ c.compl ++ c.adv ++ c.ext
                        };
                   SQuest => c.fin ++ BIND ++ R.questPart c.h ++ c.subj ++ c.inf ++ c.compl ++ c.adv ++ c.ext
                   }
              };
        
        --this attaches the ap between the two complements of the n2 (normally adjective is attached in front of both)
        mkCNN2AP : N2 -> NP -> AP -> CN = \f,x,ap-> lin CN {
            s = \\nf => preOrPost f.isPre (ap.s ! True ! nf ++ (StemFin.snoun2nounSep f).s ! nf) (R.appCompl True Pos f.c2 x) ;
            h = f.h } ;
        
                
    lincat
        TestSentence = S;
        PositiveHealthAction = {act_np : NP;agent_np : NP;act_vp_adv : Adv};
        WrappedNegativeHealthOutcome = {np : negative_health_np};
        NegativeHealthOutcome = negative_health_np;
        
        Activity = {vn: verby_noun;agent_cn : CN;activity_v : V};
        
    lin
       
        ActionReducesNegativeEffectRisk action effect =
            let
                --Different words for risk have different legal complements. Main difference is between "risk" and "possibility"/"danger", "risk" can be lower etc., the others not so
                risk : N = mkN riski_1_NK;
                risk_reduce : V2 = mkV2 (mkV (variants {alentaa_VK;va'henta'a'_VK;pienenta'a'_VK})) partitive; --maybe "laskee" also?
                risk_low_variant : A = variants {mkA matala_NK;mkA pieni_NK;mkA alhainen_AK;mkA (mkN "alhainen") "alempi" "alin";mkA "vähäinen"};
                
                risk_alt : N = mkN (variants{vaara_1_NK;mahdollisuus_NK});
                risk_alt_reduce : V2 = mkV2 (mkV (variants {va'henta'a'_VK;pienenta'a'_VK})) partitive;
                risk_alt_low_variant : A = variants {mkA pieni_NK;mkA "vähäinen"};
                
                compound_risk_n : N = lin N {s = \\nf=> effect.np.compound_prefix ++ BIND ++ (risk.s ! nf);h = risk.h;lock_N = <>};
                compound_risk : CN = mkCN compound_risk_n;
                
                outcome_np : NP = case effect.np.neg_param of
                    {
                        NameNp => variants
                        {
                            effect.np.np;
                            mkNP (mkCN (mkN2 (mkN synty_NK)) effect.np.np); -- X:n synty
                            mkNP (mkCN (mkN2 (mkN (mkV kehittya'_VK))) effect.np.np); -- X:n kehittyminen
                            mkNP (mkCN (mkN2IsPre (mkN (mkV sairastua_VK)) (casePrep illative) False) effect.np.np); --X:ään sairastuminen
                        };
                        InchoativeNp => variants
                        {
                            effect.np.np;
                        }
                    };
                
                prevent_v2_variant : V2 = variants
                {
                    mkV2 (mkV ehka'ista'_VK) partitive;
                    mkV2 (mkV "ennaltaehkäistä") partitive;
                };
                
                
                
                generate_risk_forms : N -> CN = \risk_n ->
                    case effect.np.neg_param of
                    {
                        NameNp => variants
                        {
                            mkCN (mkN2 risk_n) outcome_np; --x:n riski, safe
                            mkCN (mkN2 risk_n (casePrep illative)) outcome_np; --riski X:ään, iffier
                            mkCN (mkN2 risk_n (casePrep allative)) outcome_np; --riski X:lle, also iffier
                            mkCN (mkN2 risk_n (casePrep elative)) outcome_np; --riski X:stä, even iffier
                            mkCN (mkCN risk_n) (mkVP (mkV2 (mkV sairastua_VK) illative) outcome_np); --riski sairastua X:ään
                        };
                        InchoativeNp => variants
                        {
                            mkCN (mkN2 risk_n) effect.np.np; --x:n riski, safe
                            mkCN (mkN2 risk_n (casePrep illative)) outcome_np; --riski X:ään, iffier
                            mkCN (mkN2 risk_n (casePrep allative)) outcome_np; --riski X:stä, even iffier
                            mkCN (mkN2 risk_n (casePrep elative)) outcome_np --riski X:lle, also iffier
                        }
                    };
                
                all_risks : CN = generate_risk_forms risk;
                
                all_risk_alts : CN = generate_risk_forms risk_alt;
                
                cl_variant : Cl = variants
                    {
                        mkCl action.act_np (mkVP risk_reduce (mkNP compound_risk)); --alentaa X-riskiä
                        --add: riski on pienempi niillä/ihmisillä/henkilöillä, jotka
                        predVPVariants BackAdverbial (mkNP all_risks) (mkVP (mkVP (comparAP risk_low_variant)) (SyntaxFin.mkAdv (casePrep adessive) action.agent_np));
                        predVPVariants BackAdverbial (mkNP all_risk_alts) (mkVP (mkVP (comparAP risk_alt_low_variant)) (SyntaxFin.mkAdv (casePrep adessive) action.agent_np));
                        
                        --risk for y can lessened doing x
                        predVPVariants BackAdverbial (mkNP all_risks) (mkVP (mkVP can_VV (passiveVP risk_reduce)) action.act_vp_adv);
                        predVPVariants BackAdverbial (mkNP all_risk_alts) (mkVP (mkVP can_VV (passiveVP risk_alt_reduce)) action.act_vp_adv);
                            
                        mkCl action.act_np (mkVP prevent_v2_variant outcome_np); --ehkäisee X:n syntyä
                        mkCl action.act_np (mkVP risk_reduce (mkNP all_risks)); --alentaa X:n synnyn riskiä
                        mkCl action.act_np (mkVP risk_alt_reduce (mkNP all_risk_alts)); --alentaa X:n synnyn riskiä
                        
                        mkCl action.agent_np (mkVP have_V2 (mkNP (mkCN (comparAP risk_low_variant) all_risks))); --liikuntaa harrastavilla on alempi riski
                        mkCl action.agent_np (mkVP have_V2 (mkNP (mkCN (comparAP risk_alt_low_variant) all_risk_alts))); --liikuntaa harrastavilla on alempi riski
                        
                        --risk for y can lessened doing x
                        predVPVariants BackAdverbial (mkNP all_risks) (mkVP (mkVP can_VV (passiveVP risk_reduce)) action.act_vp_adv);
                        predVPVariants BackAdverbial (mkNP all_risk_alts) (mkVP (mkVP can_VV (passiveVP risk_alt_reduce)) action.act_vp_adv);
                        
                        --gf auxiliary passives are null person structures, needs a hack for genuine aux passive
                        let
                            hack_passive : V = lin V {s : StemFin.SVForm => Str = \\vf => "voidaan" ++ risk_alt_reduce.s ! (R.Inf infFirst);sc=R.SCPart;h=R.Front;p = ""};
                        in
                            predVPVariants BackAdverbial (mkNP all_risks) (mkVP (mkVP hack_passive) action.act_vp_adv);
                        
                        --gf auxiliary passives are null person structures, needs a hack for genuine aux passive
                        let
                            hack_passive : V = lin V {s : StemFin.SVForm => Str = \\vf => "voidaan" ++ risk_reduce.s ! (R.Inf infFirst);sc=R.SCPart;h=R.Front;p = ""};
                        in
                            predVPVariants BackAdverbial (mkNP all_risk_alts) (mkVP (mkVP hack_passive) action.act_vp_adv);
                    }
            in
                mkS presentTense cl_variant;
                
        RegularPeriodicalActivity activity =
            let
                regular_a : A = mkA sa'a'nno'llinen_AK;
                regular_ap : AP = mkAP regular_a;
                regular_adv : Adv = SyntaxFin.mkAdv regular_a;
                regular_v : V = mkV harrastaa_VK;
                
                regular_v_n2 : N2 = mkN2IsPre (mkN regular_v) (casePrep genitive) False;
                
                verby_regular_agent : N2 = mkN2IsPre (mkN "harrastava" "harrastavia") (casePrep partitive) False; --harrastava, buggy original: (S.sverb2nounPresPartAct (mkV harrastaa_VK))**{lock_N = <>}
                nouny_regular_agent : N2 = mkN2IsPre (mkN harrastaja_NK) (casePrep genitive) False;
                                
                noun_period_cn : CN = mkCN regular_v_n2 (mkNP activity.vn.cn);
                noun_period_cn_with_period_ap = mkCNN2AP regular_v_n2 (mkNP activity.vn.cn) regular_ap;
                
                act_vp_adv_variant : Adv = variants
                    {
                        --todo: add "säännöllisellä liikunnalla"
                        vpToAdv R.Inf3Adess (mkVP (mkV2 regular_v partitive) (mkNP activity.vn.cn));
                        vpToAdv R.Inf3Adess (mkVP (mkVP (mkV2 regular_v partitive) (mkNP activity.vn.cn)) regular_adv);
                        vpToAdv R.Inf3Adess (mkVP (mkVP activity.activity_v) regular_adv);
                        SyntaxFin.mkAdv (casePrep adessive) (mkNP (mkCN regular_ap activity.vn.cn));
                        SyntaxFin.mkAdv (casePrep adessive) (mkNP noun_period_cn_with_period_ap);
                        SyntaxFin.mkAdv (casePrep adessive) (mkNP noun_period_cn);
                        SyntaxFin.mkAdv (casePrep adessive) (mkNP (mkCN regular_ap noun_period_cn));
                    };
                
                common_variants : NP = 
                    variants
                    {
                        mkNP noun_period_cn; --Note that trying to create the v_n2 from period v results in an error, seems there's a bug with record fields
                        mkNP noun_period_cn_with_period_ap;
                        mkNP (mkCN regular_ap noun_period_cn);
                        mkNP (mkCN regular_ap activity.vn.cn);
                        mkNP (mkNP noun_period_cn) regular_adv;
                    };
                    
                activity_variant : NP = table
                {
                    Minen => variants
                        {
                            common_variants;
                            mkNP (mkAdvCNReverse regular_adv activity.vn.cn);
                        };
                    _ => common_variants
                } ! activity.vn.verbiness;
                
                verby_activity_agent : CN = mkCN verby_regular_agent (mkNP activity.vn.cn);
                nouny_activity_agent : CN = mkCN nouny_regular_agent (mkNP activity.vn.cn);
                
                
                
                
                agent_variant : CN = variants {activity.agent_cn;verby_activity_agent};
                
                agent_phrase_variant : NP = variants
                {
                    mkNP (mkAdvCNReverse regular_adv agent_variant);
                    mkNP aPl_Det (mkAdvCNReverse regular_adv agent_variant);
                    
                    mkNP aSg_Det verby_activity_agent;
                    mkNP aSg_Det nouny_activity_agent;
                    
                    mkNP aPl_Det verby_activity_agent;
                    mkNP aPl_Det nouny_activity_agent;
                    
                    --the relative function in the API slows compilation completely, this is a workaround
                    --mkNP ( (mkRS presentTense (RelVP which_RP (mkVP activity.vn.v)))); --this won't compile in reasonable time for some reason
                    
                    simple_relative (mkNP (those_Det ** {s1 = M.pronNe.s}) relative_head_variant) (mkVP (mkVP (activity.vn.v)) regular_adv);
                    simple_relative (mkNP aSg_Det relative_head_variant) (mkVP (mkVP (activity.vn.v)) regular_adv);
                    simple_relative (mkNP aPl_Det relative_head_variant) (mkVP (mkVP (activity.vn.v)) regular_adv);
                    simple_relative (mkNP ne_Pron) (mkVP (mkVP (activity.vn.v)) regular_adv);
                    
                }
                
            in
                {act_np = activity_variant;agent_np = agent_phrase_variant;act_vp_adv = act_vp_adv_variant}; 
                
                
        SemVar1aCombinedNegativeHealthEffect neg1 neg2 =
            let
                np : NP = mkNP and_Conj neg1.np neg2.np;
                compound_prefix = neg1.compound_prefix ++ BIND ++ "-" ++ "ja" ++ neg2.compound_prefix;
            in
                {np = {np = np;neg_param = neg2.neg_param;compound_prefix=compound_prefix}};
        
        SemVar1bSingleNegativeHealthEffect neg1 = variants
        {
            {np = neg1;compound_prefix = neg1.compound_prefix};
            
        };
        
        
        Exercise =
            let
                exercise_V : V = mkV liikkua_VK;
                agent : CN = mkCN ((sverb2nounPresPartAct exercise_V) ** {lock_N = <>}); --liikkuva
                
                --this is the activity agent participle used as an active with agent head, liikkuva ihminen, henkilö
                adj_agent : CN = mkCN (mkA liikkuva_AK) relative_head_variant;
                
                agent_variant = variants {agent;adj_agent};
                
                vn_variant : verby_noun = variants
                {
                    {verbiness = Minen; cn = mkCN (mkN exercise_V);v = exercise_V}; --liikkuminen
                    {verbiness = NoStiAdverbs; cn = mkCN (mkN liikunta_NK);v = exercise_V}; --liikunta
                };
            in
                {vn = vn_variant; agent_cn = agent_variant;activity_v = exercise_V};
                
        SemVar2aCancer =
            variants {
                {np = mkNP (mkN syo'pa'_NK);neg_param = NameNp;compound_prefix = "syöpä"};
                {np = mkNP (mkN "syöpä" (mkN sairaus_NK));neg_param = NameNp;compound_prefix = "syöpäsairaus"};
                {np = mkNP aPl_Det (mkN "syöpä" (mkN sairaus_NK));neg_param = NameNp;compound_prefix = "syöpäsairaus"}
            };
        SemVar2bDepression = 
            variants
            {
                {np = mkNP (mkN masennus_NK);neg_param = NameNp;compound_prefix = "masennus"};
                {np = mkNP (mkN (mkV masentua_VK));neg_param = InchoativeNp;compound_prefix = "masentumis"};
            };
 
        {-This destroys generation speed
        SemVar2cCardiovascularDisease =
            variants
            {
                {np = mkNP (mkN "sydän" (mkN sairaus_NK));neg_param = NameNp;compound_prefix = "sydänsairaus"};
                {np = mkNP (mkN "sydäntauti");neg_param = NameNp;compound_prefix = "sydäntauti"};
                {np = mkNP (mkN "sydän- ja verenkiertotauti");neg_param = NameNp;compound_prefix = "sydän- ja verenkiertotauti"};
                {np = mkNP (mkN "sydän- ja verenkierto" (mkN sairaus_NK));neg_param = NameNp;compound_prefix = "sydän- ja verenkiertosairaus"};
                {np = mkNP (mkN "sydän- ja verisuonitauti");neg_param = NameNp;compound_prefix = "sydän- ja verisuonitauti"};
                {np = mkNP (mkN "sydämen ja verenkierron tauti");neg_param = NameNp;compound_prefix = "sydän- ja verenkiertosairaus"};
                {np = mkNP (mkN "sydämen ja verenkiertoelimistön tauti");neg_param = NameNp;compound_prefix = "sydän- ja verisuonitauti"};
                {np = mkNP (mkN "sydämen ja verenkierron " (mkN sairaus_NK));neg_param = NameNp;compound_prefix = "sydän- ja verenkiertosairaus"};
                {np = mkNP (mkN "sydämen ja verenkiertoelimistön " (mkN sairaus_NK));neg_param = NameNp;compound_prefix = "sydän- ja verisuonitauti"};
                {np = mkNP aPl_Det (mkN "sydän"  (mkN sairaus_NK));neg_param = NameNp;compound_prefix = "sydänsairaus"};
                {np = mkNP aPl_Det (mkN "sydäntauti");neg_param = NameNp;compound_prefix = "sydäntauti"};
                {np = mkNP aPl_Det (mkN "sydän- ja verenkiertotauti");neg_param = NameNp;compound_prefix = "sydän- ja verenkiertotauti"};
                {np = mkNP aPl_Det (mkN "sydän- ja verenkierto" (mkN sairaus_NK));neg_param = NameNp;compound_prefix = "sydän- ja verenkiertosairaus"};
                {np = mkNP aPl_Det (mkN "sydän- ja verisuonitauti");neg_param = NameNp;compound_prefix = "sydän- ja verisuonitauti"};
                {np = mkNP aPl_Det (mkN "sydämen ja verenkierron tauti");neg_param = NameNp;compound_prefix = "sydän- ja verenkiertosairaus"};
                {np = mkNP aPl_Det (mkN "sydämen ja verenkiertoelimistön tauti");neg_param = NameNp;compound_prefix = "sydän- ja verisuonitauti"};
                {np = mkNP aPl_Det (mkN "sydämen ja verenkierron " (mkN sairaus_NK));neg_param = NameNp;compound_prefix = "sydän- ja verenkiertosairaus"};
                {np = mkNP aPl_Det (mkN "sydämen ja verenkiertoelimistön " (mkN sairaus_NK));neg_param = NameNp;compound_prefix = "sydän- ja verenkiertotauti"};
                
            };-}
        SemVar2cDementia = {np = mkNP (mkN dementia_NK);neg_param = NameNp;compound_prefix = "dementia"};
        
}