--# -coding=UTF-8
abstract EvalGenTemplate6 = {


    flags
        startcat = Statement;
    cat
        --Semantic cats
        PastForm;
        Statement;
        TimePeriod;
        GeoArea;
        MetPhen;
        ComplexMetPhen;
        UndergoVerb;
        TimeUnit;
        
    fun
        SemVar1aWeek, SemVar1bDay, SemVar1cHour : TimeUnit;
        SemVar2aSeveral : TimeUnit -> TimePeriod;
        SemVar3aAlps,SemVar3bLapland,SemVar3cSiberia,SemVar3dAndes,SemVar3eGreenland,SemVar3fSweden : GeoArea;
        SemVar4aSnow : MetPhen;
        SemVar4bHeavySnow : ComplexMetPhen;
        SemVar5aPresentPerfect, SemVar5bPastPerfect, SemVar5cSimplePast : PastForm;
        Experience, See, Have : UndergoVerb; 
        --Semantic funs
        
        SimplifyComplexMetPhen : ComplexMetPhen -> MetPhen;
        --"it has been snowing in the alps for weeks
        PleonasticItTransitive0Predicate_MetPhen1InsideLocative_GeoArea2Benefactive_TimePeriod3 : PastForm -> MetPhen -> GeoArea -> TimePeriod -> Statement;
        
        --"there has been snowfall in the alps for weeks
        PleonasticThere0Object_MetPhen1InsideLocative_GeoArea2Benefactive_TimePeriod3 : PastForm -> MetPhen -> GeoArea -> TimePeriod -> Statement;
        
        --"there have been weeks of snowfall in the alps
        PleonasticThere0Object_TimePeriod1ObjectOfGenitive_MetPhen1InsideLocative_GeoArea2 : PastForm -> MetPhen -> GeoArea -> TimePeriod -> Statement;
        
        --"the alps have experienced/seen snowfall for weeks
        Transitive0Verb_Undergo1Subject_GeoArea2Object_MetPhen3_Benefactive_TimePeriod4 : PastForm -> UndergoVerb -> GeoArea -> MetPhen -> TimePeriod -> Statement;
        
        --"the alps have experienced/seen weeks of snowfall
        Transitive0UndergoVerb1Subject_GeoArea2TimePeriod3ObjectOfGenitive_MetPhen4 : PastForm -> UndergoVerb -> GeoArea -> MetPhen -> TimePeriod -> Statement; 
        
        --"snowfall/snowing has continued for weeks in the alps
        IntransitiveTimeVerb0Subject_MetPhen1Object_MetPhen1InsideLocative_GeoArea2Benefactive_TimePeriod3 : PastForm -> MetPhen -> GeoArea -> TimePeriod -> Statement; 
        
        --"snow has been falling for weeks in the alps
        IntransitiveMovementVerb0Subject_MetPhen1Object_MetPhen1InsideLocative_GeoArea2Benefactive_TimePeriod3 : PastForm -> MetPhen -> GeoArea -> TimePeriod -> Statement;
        
        --"snow has been falling for weeks in the alps
        IntransitiveMovementVerb0Subject_ComplexMetPhen1Object_MetPhen1InsideLocative_GeoArea2Benefactive_TimePeriod3 : PastForm -> ComplexMetPhen -> GeoArea -> TimePeriod -> Statement;
        
    
    }