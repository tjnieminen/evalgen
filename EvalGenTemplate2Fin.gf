concrete EvalGenTemplate2Fin of EvalGenTemplate2 = open Prelude,ParamX,(M = MorphoFin),SyntaxFin,ParadigmsFin,DictFin,SentenceFin,NounFin,RelativeFin,(S = StemFin),(R = ResFin) in {
    param
        VerbinessOfNoun = Minen | NoStiAdverbs | Va; --noun derived from verbs have a variabled degree of verb features
        
        NegativeHealthOutcomeParam = NameNp | InchoativeNp;
        AdverbOrder = FreeAdverbial | BackAdverbial;
    oper
    
        relative_head_variant : CN =
                    mkCN 
                        (mkN
                            (variants
                                {
                                    ihminen_NK;
                                    henkilo'_NK;
                                }
                            )
                        );
        --the relative constructor in the library slows down linking immensely, this is a simpler version
        simple_relative : NP -> VP -> NP = \np,v->
            lin NP
            {
                s = case np.a of
                    {
                        R.Ag Sg _ => \\npform => np.s ! npform ++ "," ++ "joka" ++ v.s.s ! R.Presn Sg P3 ++ v.adv ! Pos ++ ",";
                        R.Ag Pl _ => \\npform => np.s ! npform ++ "," ++ "jotka" ++ v.s.s ! R.Presn Pl P3 ++ v.adv ! Pos ++ ",";
                        _ => np.s
                    };
                a=np.a;
                isPron=np.isPron;
                isNeg=np.isNeg;
            };
        
        negative_health_np : Type = {np : NP;neg_param : NegativeHealthOutcomeParam; compound_prefix : Str};
        verby_noun : Type = {verbiness : VerbinessOfNoun;cn : CN;v : V};
        effort_rec : Type = {soa : NP;effort : V;effort_nom : NP};
        --got an error when trying to extract complement from VP, so pass it as an argument
        mkN2IsPre : N -> Prep -> Bool -> N2 = \n,c,p -> n ** {c2 = c ; isPre = p ; lock_N2 = <>} ; --normal N2 constructor always places adv last, this has switch
        
        nominalize : V -> NP -> Case -> CN = \vpi,comp,cas->
            let
                infnom : N = mkN vpi;
                n2 : N2 = mkN2IsPre infnom (casePrep cas) False;
            in
                mkCN (mkCN n2 comp) (mkAdv vpi.p);--the adv is a potential verb particle, it won't mostly exist                    
            
        --this version takes an AP, it's for cases where the AP can be on either side of fronted complement
        nominalizeWithAP : V -> NP -> Case -> AP -> CN = \vpi,comp,cas,ap->
            let
                infnom : N = mkN vpi;
            in
                variants
                {
                    --mkCN (mkCN ap (mkCN (mkN2IsPre infnom (casePrep cas) False) comp)) (mkAdv vpi.p); --the adv is a potential verb particle, it won't mostly exist
                    --Need an mkCN constructor that adds the AP in front of the head, so N2 -> AP -> NP, "väliensä julkisesta korjaamisesta"
                    mkCN (mkCNN2AP (mkN2IsPre infnom (casePrep cas) False) comp ap) (mkAdv vpi.p);
                    --.mkCN (mkAdvCNReverse (SyntaxFin.mkAdv (casePrep cas) comp) (mkCN ap infnom)) (mkAdv vpi.p);
                };
                
        
        mkSSubjReverse : Subj -> S -> S -> S = \subj,b,a-> lin S {s = subj.s ++ b.s ++ "," ++ a.s};
        mkAdvCNReverse : Adv -> CN -> CN = \ad,cn-> lin CN {s = \\nf => ad.s ++ cn.s ! nf ; h = cn.h};
        
        mkSPostAdv : S -> Adv -> S = \s,a-> lin S {s = s.s ++ a.s};
        
        --this is an adverbial clause containing a subjunctive, e.g. "siitä huolimatta, että X"
        mkAdvSubj : Str -> Subj -> S -> Adv = \head,subj,s -> lin Adv {
            s = head ++ "," ++ subj.s ++ s.s ++ ",";
        };
        possessiveSuffixPron : {s : ResFin.NPForm => Str ; a : ResFin.Agr ; hasPoss : Bool ; poss : Str} = 
            {s = table {_=>[]};a = ResFin.Ag Pl P3; hasPoss = True; poss = []};
        
        vpToAdv : R.InfForm -> VP -> Adv = \inff,vp-> lin Adv {
            s = vp.s.s ! R.Inf inff ++ vp.s2 ! False ! ParamX.Pos ! ResFin.Ag Sg P3 ++ vp.adv ! ParamX.Pos;
        };
        
        predVPVariants : AdverbOrder -> NP -> VP -> Cl = \advorder,np,vp-> mkClauseVariants advorder (orB np.isNeg vp.vptyp.isNeg) (R.subjForm np vp.s.sc) np.a vp;
        
        --This is mkClause with a possibility to define restrictions on phrase order variants
        mkClauseVariants : AdverbOrder -> Bool -> (Polarity -> Str) -> R.Agr -> VP -> R.Clause =
            \advorder,isNeg,sub,agr,vp -> {
              s = \\t,a,b => 
                 let
                   pol = case isNeg of {
                    True => Neg ; 
                    _ => b
                    } ; 
                   c = (S.mkClausePlus sub agr vp).s ! t ! a ! pol 
                 in 
                 table {
                   SDecl  => case advorder of
                        {
                            FreeAdverbial =>  variants
                                {
                                    c.subj ++ c.adv ++ c.fin ++ c.inf ++ c.compl ++ c.ext;
                                    c.subj ++ c.fin ++ c.inf ++ c.compl ++ c.adv ++ c.ext;
                                    c.subj ++ c.fin ++ c.inf ++ c.adv ++ c.compl ++ c.ext;
                                };
                            BackAdverbial => c.subj ++ c.fin ++ c.inf ++ c.compl ++ c.adv ++ c.ext
                        };
                   SQuest => c.fin ++ BIND ++ R.questPart c.h ++ c.subj ++ c.inf ++ c.compl ++ c.adv ++ c.ext
                   }
              };
        
        --this attaches the ap between the two complements of the n2 (normally adjective is attached in front of both)
        mkCNN2AP : N2 -> NP -> AP -> CN = \f,x,ap-> lin CN {
            s = \\nf => preOrPost f.isPre (ap.s ! True ! nf ++ (S.snoun2nounSep f).s ! nf) (R.appCompl True Pos f.c2 x) ;
            h = f.h } ;
        
                
    lincat
        TestSentence = S;
        Effort = effort_rec;
        Difficulty = AP;
        HighDifficulty = AP;
        LowDifficulty = AP;
        StateOfAffairs = NP;
    lin
        
        EffortIsOfDifficulty effort difficulty =
            let
                futureOrPresent : Tense = variants {futureTense;presentTense};
                basecl : Cl =
                    let
                        case_variant : Case = variants {nominative;partitive};
                        tulla_V : V = mkV tulla_VK;
                        auxfutbe_V : V = tulla_V ** {s = table
                            {
                                x => tulla_V.s ! x ++ "olemaan"
                            };
                        }
                    in
                        variants
                        {
                            (mkCl effort.soa (mkVP (mkVA olla_V (mkPrep case_variant)) (mkAP difficulty (mkVP effort.effort))));
                            (mkCl effort.soa (mkVP (mkVA (caseV partitive olla_V) (mkPrep case_variant)) (mkAP difficulty (mkVP effort.effort))));
                            (mkCl (mkVP (mkVP (mkVA olla_V (mkPrep case_variant)) (mkAP difficulty (mkVP effort.effort))) (SyntaxFin.mkAdv (mkPrep case_variant) effort.soa)));
                            (mkCl (effort.effort_nom) (mkVA olla_V (mkPrep partitive)) difficulty);
                            (mkCl effort.soa (mkVP (mkVA auxfutbe_V (mkPrep case_variant)) (mkAP difficulty (mkVP effort.effort))));
                            (mkCl effort.soa (mkVP (mkVA (caseV partitive auxfutbe_V) (mkPrep case_variant)) (mkAP difficulty (mkVP effort.effort))));
                            (mkCl (mkVP (mkVP (mkVA auxfutbe_V (mkPrep case_variant)) (mkAP difficulty (mkVP effort.effort))) (SyntaxFin.mkAdv (mkPrep case_variant) effort.soa)));
                            (mkCl (effort.effort_nom) (mkVA auxfutbe_V (mkPrep partitive)) difficulty)
                        };
            in
                mkS futureOrPresent basecl;
                
        Trust = mkNP (mkCN (mkA (variants {"aito";"todellinen";"oikea"})) (mkN "luottamus"));
        Rebuilding state_of_affairs =
            let
                rebuild_v : V = variants {mkV "uudelleenrakentaa";mkV "jälleenrakentaa";mkV (mkV "rakentaa") "uudelleen";mkV "palauttaa";mkV (mkV "luoda") "uudelleen"};
            in
                {soa = state_of_affairs; effort=rebuild_v;effort_nom=mkNP (nominalize rebuild_v state_of_affairs genitive);};
        VeryHard =  mkAP (mkA (variants {"hankala";"vaikea"}));
        VeryEasy =  mkAP (mkA (variants {"helppo";"vaivaton"}));
        SemVar1aImpossibleApposition highdif =
            let
                possibility_ada = variants {mkAdA "ehkä";mkAdA "kenties";mkAdA "jopa"};
            in
                mkAP (mkConj ",") (mkListAP highdif (mkAP possibility_ada (mkA "mahdoton")));
        SemVar1bHighDifficultyWrap highdif = highdif;
        SemVar1cLowDifficultyWrap lowdif = lowdif;
        
}