--# -coding=UTF-8
--TOO COMPLEX AND EASY FOR MT: THIS WILL BE "the final cost was x instead of the initially estimated Y"
--TOO COMPLEX AND EASY FOR MT: AFTER THIS, MAYBE ALSO DO "the experiment was a success, according to the project lead N.N"
--TOO MESSY: instead, Jack and Jill wanted to get spaghetti, but the restaurant was out of stock.
--try conditional with this, "we would have eaten..."?
--INSTEAD: The customer wished to buy a new cassette player from the store, but it carried none.
abstract EvalGenTemplate7 = {


    flags
        startcat = Statement;
    cat
        --Semantic cats
        Statement;
        Customer;
        Store;
        Item;
        StoreReferent;
        ItemReferent;
        Subclause;
        ForSaleVerb;
        AvailableVerb;
        ForSaleAdverb;
        ForSaleWrapper ForSaleAdverb;
        HaveVerb;
        Media;
        MediaOwner;
        VolitionVerb;
        VolitionNoun;
        AvailableVerb;
        InclusionVerb;
        IncludeVerb;
        ExclusionVerb;
        IndexedStatement;
        Drink;
        JuiceBase;
    fun
        AddStatementIndex : Int -> Statement -> IndexedStatement;
        Include : IncludeVerb;
        Miss : ExclusionVerb;
        Intention : VolitionNoun;
        WrapForSale : (f : ForSaleAdverb) -> ForSaleWrapper f;
        Want,Intend : VolitionVerb;
        Sell, Carry : ForSaleVerb;
        Have : HaveVerb;
        BeAvailable : AvailableVerb;
        Belong : InclusionVerb;
        InStock,ForSale,InSelection : ForSaleAdverb;
        Them,One,Any,None,AnyOfThem,NoneOfThem: ItemReferent;
        SemVar1aGenericStore : Store;
        StoreToStoreReferent : Store -> StoreReferent;
        StoreToStoreReferentAdverb : Store -> StoreReferent;
        UnspecifiedStore : StoreReferent;
        SemVar2aWe : Customer;
        SemVar3aCassettePlayer : Item;
        SemVar3bPlayerFor : Media -> MediaOwner -> Item;
        SemVar3cPlayerForOld : Media -> MediaOwner -> Item;
        SemVar3dDrinkCan : Drink -> Item;
        SemVar6aSoda : Drink;
        SemVar6bJuiceFrom : JuiceBase -> Drink;
        SemVar7aApple,SemVar7bOrange,SemVar7cGrape,SemVar7dCarrot,SemVar7ePear : JuiceBase;
        SemVar4aCassetteMedia : Media;
        SemVar5aMe : MediaOwner;
        --"we wanted to buy an X from the Y, but they didn't sell it
        ActiveTransitiveVolitionVerbInfinitivalSubordinateClause0Subject_Customer1Verb_Volition2Verb3InfinitivalObject_Item4Source_Store3Subordinate_StoreSubclause5 : Customer -> VolitionVerb -> Item -> Store -> Subclause -> Statement;

        --"meidän oli määrä", this is for a semiproductive Finnish construction
        ActiveTransitiveSpecialFinnishVolitionNounInfinitivalSubordinateClause0Subject_Customer1Verb2InfinitivalObject_Item4Source_Store3Subordinate_StoreSubclause5 : Customer -> Item -> Store -> Subclause -> Statement;
        
        --"we had the intention to buy X from Y"
        ActiveTransitiveVolitionVerbInfinitivalSubordinateClause0Subject_Customer1Noun_Volition2Verb3InfinitivalObject_Item4Source_Store3Subordinate_StoreSubclause5 : Customer -> VolitionNoun -> Item -> Store -> Subclause -> Statement;
        
        --"our intention was to buy X from Y"
        ActiveTransitiveVolitionVerbInfinitivalSubordinateClause0Possessive_Customer1Noun_Volition2Verb3InfinitivalObject_Item4Source_Store3Subordinate_StoreSubclause5 : Customer -> VolitionNoun -> Item -> Store -> Subclause -> Statement;
        
        
        --"they didn't have them for sale"
        SubclauseActiveTransitive0Subject_Store1Verb_HaveVerb2Object_ItemReferent3Adverb_ForSaleAdverb4 : StoreReferent -> HaveVerb -> ItemReferent -> ForSaleAdverb -> Subclause;
       
        --"they didn't sell them"
        SubclauseActiveTransitive0Subject_Store1Verb_ForSaleVerb2Object_ItemReferent3 : StoreReferent -> ForSaleVerb -> ItemReferent -> Subclause;
        
        --"ne eivät kuuluneet valikoimaan"
        SubclauseActiveIntransitive0Subject_ItemReferent1Verb_InclusionVerb2Adverb_Store3 : ItemReferent -> InclusionVerb -> StoreReferent -> Subclause;
        
        --"valikoima ei sisältänyt niitä"
        SubclauseActiveIntransitive0Verb_InclusionVerb1Object_ItemReferent2Adverb_Store3 : IncludeVerb -> ItemReferent -> StoreReferent -> Subclause;
        
        
        --"ne puuttuivat valikoimasta"
        SubclauseActiveTransitive0Subject_ItemReferent1Verb_ExclusionVerb2Adverb_Store3 : ItemReferent -> ExclusionVerb -> StoreReferent -> Subclause;
        
        --"kaupassa ei ollut niitä"
        --"they didn't have them"
        SubclauseActiveTransitive0Subject_Store1Verb_HaveVerb2Object_ItemReferent3 : StoreReferent -> HaveVerb -> ItemReferent -> Subclause;
        
        
        
        
        
        
        SubclauseActiveTransitive0Subject_Store1Verb_HaveVerb2Object_ItemReferent3Adverb_InSelectionAdverb4 : StoreReferent -> HaveVerb -> ItemReferent -> ForSaleWrapper InSelection -> Subclause;
        
        --"there weren't any for sale"
        SubclauseNeoplasticThere0Adverbial_Store1Verb_HaveVerb2Object_ItemReferent3Adverb_ForSaleAdverb4  : StoreReferent -> ItemReferent -> ForSaleAdverb -> Subclause;
        
        --"siellä ei myyty niitä"
        --"they weren't sold there"
        --"they weren't sold by the store"
        --"they aren't sold there"
        --"they weren't stocked by the store"
        SubclausePassiveTransitive0Adverbial_Store1Verb_ForSaleVerb2Object_ItemReferent3 : StoreReferent -> ForSaleVerb -> ItemReferent -> Subclause;

        --"niitä ei saanut sieltä"
        SubclausePassiveTransitive0Adverbial_Store1Verb_AvailableVerb2Object_ItemReferent3 : StoreReferent -> AvailableVerb -> ItemReferent -> Subclause;
        
        --"niitä ei kuulunut valikoimaan"
        SubclausePassiveTransitive0Adverbial_Store1Verb_InclusionVerb2Object_ItemReferent3 : StoreReferent -> InclusionVerb -> ItemReferent -> Subclause;
        
    }