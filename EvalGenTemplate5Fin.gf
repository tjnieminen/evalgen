concrete EvalGenTemplate5Fin of EvalGenTemplate5 = open Prelude,LangFin,ParadigmsFin,SyntaxFin,DictFin,(ResFin = ResFin),ExtraFin,(AdjectiveFin=AdjectiveFin) in {
    param
        NomiOrGeni = Nomi | Geni;
        BackOrFront = Backed | Fronted;
    oper
        sijaita_V = mkV "sijaita" "sijaitsen" "sijaitsee" "sijaitsevat" "sijaitkaa" "sijaitaan" "sijaitsin" "sijaitsi" "sijaitsisi" "sijainnut" "sijainnut" "sijainnee";
        proDropPossInanimate : Quant = lin Quant
            {
                s1 : Number => Case => Str = \\_,_ => [] ;
                sp : Number => Case => Str = \\_,_ => [] ;
                s2 : ResFin.Harmony => Str = table {
                        Front => BIND ++ "än" ; 
                        Back  => BIND ++ "an" } ;
                isNum = False ;
                isPoss = True ;
                isDef = True ;  --- "minun kolme autoani ovat" ; thus "...on" is missing
                isNeg = False
            } ;
    lincat
        IndexedStatement = Str;
        Statement = S;
        BasePhysicalObject = CN;
        PhysicalObject = NomiOrGeni => CN;
        PropertyType = CN;
        PhysicalProperty = A;
        Location = NP;
        LocationRelation = {nomgen : NomiOrGeni => {prep : Prep;backed : BackOrFront};vpprep : {vp1: VP;prep1 : Prep}};

    lin
        AddStatementIndex i s = i.s ++ s.s;
        SemVar4aDiningTable = variants
        {
            mkNP the_Det (mkN "ruokapöytä");
            mkNP the_Det (mkN "ruokailupöytä");
        };
        SemVar4bSilverTray = variants
        {
            mkNP the_Det (mkN "hopeatarjotin");
            mkNP the_Det (mkCN (mkA "hopeinen") (mkN "tarjotin"));
        };
        SemVar1aBottle = mkCN (mkN "pullo");
        SemVar1bCup = mkCN (mkN "kuppi");
        SemVar1cPlate = mkCN (mkN "lautanen");
        SemVar1dMug = mkCN (mkN muki_NK);
        
        SemVar2aBarePhysicalObject bo = \\_ => bo;
        SemVar2bPhysicalObjectWithLocationAdverb bo l lr = 
            \\x => case (lr.nomgen ! x).backed of
            {
                Backed => mkCN bo (mkAdv (lr.nomgen ! x).prep l);
                Fronted => bo ** {s = \\y=> (mkAdv (lr.nomgen ! x).prep l).s ++ bo.s ! y}
            };
            
        SemVar2bPhysicalObjectWithLocationRelative bo l lr =
            \\x => mkCN bo (mkRS (mkRCl which_RP (mkVP lr.vpprep.vp1 (mkAdv (lr.vpprep.prep1) l))));
        
        --pullo, joka on pöydän päällä
        --pullo, joka on asetettu pöydälle/pöydän päälle
        
        SemVar3aOnRelation =
        {
            nomgen = table
            {
                Nomi => variants
                {
                    {prep = postPrep genitive "päällä";backed = Backed};
                    {prep = on_Prep;backed = Backed};
                };
                Geni => {prep = mkPrep "FORBIDDED";backed = Backed} --no gen with this plain prep
            };
            vpprep = variants 
            {
                {vp1 = mkVP vOlla;prep1 = postPrep genitive "päällä"};
                {vp1 = mkVP vOlla;prep1 = on_Prep};
            }
        };
        
        SemVar3aOnRelationWithVerb =
        {
            nomgen = table
            {
                Nomi => variants
                {
                    {prep = postPrep genitive "päällä oleva";backed = Fronted};
                    {prep = postPrep genitive "päälle asetettu";backed = Fronted};
                    {prep = postPrep genitive "päällä sijaitseva";backed = Fronted};
                    {prep = postPrep allative "asetettu";backed = Fronted};
                    {prep = postPrep adessive "oleva";backed = Fronted};
                    {prep = postPrep adessive "sijaitseva";backed = Fronted};
                };
                Geni => variants
                {
                    {prep = postPrep genitive "päällä olevan";backed = Fronted};
                    {prep = postPrep genitive "päälle asetetun";backed = Fronted};
                    {prep = postPrep genitive "päällä sijaitsevan";backed = Fronted};
                    {prep = postPrep allative "asetetun";backed = Fronted};
                    {prep = postPrep adessive "olevan";backed = Fronted};
                    {prep = postPrep adessive "sijaitsevan";backed = Fronted};
                }
            };
            vpprep = variants
            {
                {vp1 = mkVP (mkV vOlla "asetettu");prep1 = casePrep allative};
                {vp1 = mkVP sijaita_V;prep1 = casePrep adessive};
                {vp1 = mkVP (mkV vOlla "asetettu");prep1 = postPrep genitive "päälle"};
                {vp1 = mkVP sijaita_V;prep1 = postPrep genitive "päällä"};
            }
            
        };
        Color = mkCN (mkN "väri");
        SemVar5aRed = mkA "punainen";
        SemVar5bYellow = mkA "keltainen";
        SemVar5cBlue = mkA "sininen";
        SemVar5dGreen = mkA "vihreä";
        SemVar5eBlack = mkA "musta";
        SemVar5fWhite = mkA "valkoinen";
        Subject_PhysObject1Predicative_Property2 k o p =
            let
                nom_object = o ! Nomi;
            in
                mkS (mkCl (mkNP the_Det nom_object) p);
        
        --Only for English, "color of the"
        --Subject_PropertyName_PropertyNamePossessive_PhysObject1Predicative_Property2 k o p = mkS (mkCl (mkNP the_Det (mkCN (mkN2 k) (mkNP the_Det o.cn))) p);
        
        Subject_PhysObjectGenitive_PropertyName1Predicative_Property2 k o p =
            let
                gen_object = o ! Geni;
            in
                mkS (mkCl (mkNP (GenNP (mkNP the_Det gen_object)) k) p);
        
        
        --"pallo on väriltään punainen, only for Fin
        Subject_PhysObject1Predicative_Property_PropertyNameOutsideSource2 k o p =
            let
                nom_object = o ! Nomi;
                ap_variant : AP = variants {
                    mkAP (mkA2 p (casePrep ablative)) (mkNP proDropPossInanimate k);
                    AdjectiveFin.AdvAP (mkAP p) (mkAdv (casePrep ablative) (mkNP proDropPossInanimate k));
                }
            in
                mkS (mkCl (mkNP the_Det nom_object) ap_variant);
        
        
            
}   