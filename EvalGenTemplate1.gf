--# -coding=UTF-8
abstract EvalGenTemplate1 = {
    flags
        startcat = TestSentence;
    cat
        TestSentence;
        BigEventKind;
        PreciseEventKind;
        WrappedEventKind;
    fun
        EventIsAnOperationOfKind: WrappedEventKind -> TestSentence;
        SemVar1aAndAtTheSameTimeOperationKind : BigEventKind -> PreciseEventKind -> WrappedEventKind;
        SemVar1bWrapBigEventKind : BigEventKind -> WrappedEventKind;
        SemVar1cWrapPreciseEventKind : PreciseEventKind -> WrappedEventKind;
        VeryBig : BigEventKind;
        VeryPrecise : PreciseEventKind;
    }