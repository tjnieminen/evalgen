concrete EvalGenTemplate6Fin of EvalGenTemplate6 = open Prelude,LangFin,ParadigmsFin,SyntaxFin,ExtraFin,DictFin,(ResFin=ResFin),(StemFin=StemFin) in {
    param
        LocativeCase = InesLoc | AdesLoc;
    oper
        --metphen_rec : Type = {act_vp : VP;act_np : NP;mov_np : NP; mov_v : V};
        
        metphen_rec : Type = {event_cn : CN;act_cn : CN;mov_cn : CN;mov_v : V};
        complex_metphen_rec : Type = metphen_rec ** {mov_adv : Adv;mov_a : A};
        
        --The rules appear to be like this:
        --1. Verb or auxiliary cannot start the sentence, has to be adjunct/complement
        --2. There's a hierarchy, which determines how far the adjunct/complement can be from the verb. NOT: "Alpeilla viikkoja satoi" or "Lunta Alpeilla satoi", YES:
        --"viikkoja Alpeilla satoi" and "Alpeilla lunta satoi". But these are all borderline cases. Hierarchy THEME > LOCATION > DURATION
        --3. 
        
        loc_case_to_prep : LocativeCase -> Case = \loc -> table
            {
                InesLoc => inessive;
                AdesLoc => adessive
            } ! loc;
            
        
        generate_time_adv : NP -> Adv = \t->
            let
                aika_n : N = mkN "aika" "ajan" "aikoja";
            in
                variants 
                {
                    SyntaxFin.mkAdv (casePrep partitive) t;
                    SyntaxFin.mkAdv (casePrep genitive) (mkNP (GenNP t) (aika_n))
                };
        
        generate_vpadvadv_variants : VP -> Adv -> Adv -> VP = \vp,adv1,adv2 ->
            variants 
            {
              mkVP (embed_adv_in_vp vp adv1) adv2;
              mkVP (embed_adv_in_vp vp adv2) adv1;
            };
          

        --adv3 isn't embedded, it's an intensifier for the verb and only comes after verb
        generate_vpadvadvadv_variants : VP -> Adv -> Adv -> Adv -> VP = \vp,adv1,adv2,adv3 ->
            variants 
            {
                mkVP (mkVP (embed_adv_in_vp vp adv1) adv2) adv3;
                mkVP (mkVP (embed_adv_in_vp vp adv1) adv3) adv2;
                mkVP (mkVP (embed_adv_in_vp vp adv2) adv1) adv3;
                mkVP (mkVP (embed_adv_in_vp vp adv2) adv3) adv1;
                
                mkVP (embed_adv_intens_in_vp vp adv1 adv3) adv2;
                --This is invalid due to the adverb hierarchy, not "alpeilla satoi runsaasti viikkoja lunta"
                --mkVP (embed_adv_intens_in_vp vp adv2 adv3) adv1;
                
            };
        --This embeds an ADV into a VP. It's required for anterior aspects, which use an auxiliary in Finnish. The adverb may go between the finite and infinite parts of the verb phrase. Bit of a hack.
        embed_adv_in_vp : VP -> Adv -> VP = \vp,adv ->
            let
                updated_sverb1 : ResFin.VForm => Str = table
                {
                    --make variant paths for aux version only
                    ResFin.PastPartAct n => variants
                      {vp.s.s ! (ResFin.PastPartAct n) ++ adv.s;adv.s ++ vp.s.s ! (ResFin.PastPartAct n)};
                    x => vp.s.s ! x ++ adv.s
                };
            in
                vp ** {s = vp.s ** {s = updated_sverb1}};
        
        generate_adv_variants_for_geo_period_and_object : Temp -> NP -> VP -> {np : NP; loc_case : LocativeCase} -> NP -> S = \temp,np,vp,g,t ->
            let
                geo_adv : Adv = SyntaxFin.mkAdv (casePrep (loc_case_to_prep g.loc_case)) g.np;
                
                period_adv : Adv = generate_time_adv t;
                 
                object_adv : Adv = SyntaxFin.mkAdv (casePrep partitive) np;
                
                geo_object_vp : VP = generate_vpadvadv_variants vp geo_adv object_adv;
                geo_period_vp : VP = generate_vpadvadv_variants vp geo_adv period_adv;
                object_period_vp : VP = generate_vpadvadv_variants vp object_adv period_adv;
                
                geo_embedded_vp : VP = embed_adv_in_vp vp geo_adv;
                object_embedded_vp : VP = embed_adv_in_vp vp object_adv;
                period_embedded_vp : VP = embed_adv_in_vp vp period_adv;
                
                
                
            in
                variants
                    {
                        mkS period_adv (mkS temp positivePol (mkCl geo_object_vp));
                        mkS object_adv (mkS temp positivePol (mkCl geo_period_vp));
                        mkS geo_adv (mkS temp positivePol (mkCl object_period_vp));
                        mkS period_adv (mkS geo_adv (mkS temp positivePol (mkCl object_embedded_vp)));
                        mkS period_adv (mkS object_adv (mkS temp positivePol (mkCl geo_embedded_vp)));
                        mkS geo_adv (mkS object_adv (mkS temp positivePol (mkCl period_embedded_vp)));
                        
                    };
                    
        embed_adv_intens_in_vp : VP -> Adv -> Adv-> VP = \vp,adv,intens ->
            let
                adv_combo : Adv = 
                  variants
                  {
                    lin Adv {s = adv.s ++ intens.s};
                    lin Adv {s = intens.s ++ adv.s}
                  };
                
                
                updated_sverb1 : ResFin.VForm => Str = table
                {
                    --for aux version only
                    ResFin.PastPartAct n => variants
                      {
                        vp.s.s ! (ResFin.PastPartAct n) ++ adv_combo.s;
                        adv.s ++ vp.s.s ! (ResFin.PastPartAct n) ++ intens.s
                      };
                    x => vp.s.s ! x ++ adv_combo.s
                };
            in
                vp ** {s = vp.s ** {s = updated_sverb1}};
                    
        generate_adv_variants_for_geo_period_object_and_intensifier : Temp -> NP -> VP -> {np : NP;loc_case : LocativeCase} -> NP -> Adv -> S = \temp,np,vp,g,t,adv1 ->
            let
                geo_adv : Adv = SyntaxFin.mkAdv (casePrep (loc_case_to_prep g.loc_case)) g.np;
                period_adv : Adv = generate_time_adv t;
                object_adv : Adv = SyntaxFin.mkAdv (casePrep partitive) np;
                
                geo_object_vp : VP = generate_vpadvadvadv_variants vp geo_adv object_adv adv1;
                geo_period_vp : VP = generate_vpadvadvadv_variants vp geo_adv period_adv adv1;
                object_period_vp : VP = generate_vpadvadvadv_variants vp object_adv period_adv adv1;
                
                geo_embedded_vp : VP = embed_adv_intens_in_vp vp geo_adv adv1;
                object_embedded_vp : VP = embed_adv_intens_in_vp vp object_adv adv1;
                period_embedded_vp : VP = embed_adv_intens_in_vp vp period_adv adv1;
                
                
                
            in
                variants
                    {
                        --in these the intensier is added as normal adverb
                        mkS period_adv (mkS temp positivePol (mkCl geo_object_vp));
                        mkS object_adv (mkS temp positivePol (mkCl geo_period_vp));
                        mkS geo_adv (mkS temp positivePol (mkCl object_period_vp));
                        
                        --here the intensier goes in the embedded vp
                        mkS period_adv (mkS geo_adv (mkS temp positivePol (mkCl object_embedded_vp)));
                        mkS period_adv (mkS object_adv (mkS temp positivePol (mkCl geo_embedded_vp)));
                        mkS geo_adv (mkS object_adv (mkS temp positivePol (mkCl period_embedded_vp)));
                        
                    };
                    
        generate_adv_variants_for_geo_period_and_subject : Temp -> NP -> VP -> {np : NP;loc_case : LocativeCase} -> NP -> S = \temp,np,vp,g,t ->
            let
                geo_adv : Adv = SyntaxFin.mkAdv (casePrep (loc_case_to_prep g.loc_case)) g.np;
                period_adv : Adv = SyntaxFin.mkAdv (casePrep partitive) t;
                subject_adv : Adv = SyntaxFin.mkAdv (casePrep nominative) np;
                
                geo_subject_vp : VP = generate_vpadvadv_variants vp geo_adv subject_adv;
                geo_period_vp : VP = generate_vpadvadv_variants vp geo_adv period_adv;
                subject_period_vp : VP = generate_vpadvadv_variants vp subject_adv period_adv;
                
                geo_embedded_vp : VP = embed_adv_in_vp vp geo_adv;
                subject_embedded_vp : VP = embed_adv_in_vp vp subject_adv;
                period_embedded_vp : VP = embed_adv_in_vp vp period_adv;
                
                
                
            in
                variants
                    {
                        mkS period_adv (mkS temp positivePol (mkCl geo_subject_vp));
                        mkS subject_adv (mkS temp positivePol (mkCl geo_period_vp));
                        mkS geo_adv (mkS temp positivePol (mkCl subject_period_vp));
                        mkS period_adv (mkS geo_adv (mkS temp positivePol (mkCl subject_embedded_vp)));
                        mkS period_adv (mkS subject_adv (mkS temp positivePol (mkCl geo_embedded_vp)));
                        mkS geo_adv (mkS subject_adv (mkS temp positivePol (mkCl period_embedded_vp)));
                        mkS subject_adv (mkS geo_adv (mkS temp positivePol (mkCl period_embedded_vp)));
                        
                    };    
        
    lincat
        Statement = S;
        TimePeriod = NP;
        TimeUnit = {n : N;period_N : N};
        GeoArea = {np : NP;loc_case : LocativeCase};
        MetPhen = metphen_rec;
        ComplexMetPhen = complex_metphen_rec;
        UndergoVerb = V2;
        PastForm = Temp;

    lin
        SimplifyComplexMetPhen compl =
            let
                rain_variant = variants {mkV "sataa";mkV "tulla"};
            in
                {
                    event_cn = mkCN compl.mov_a (mkN "lumentulo");
                    act_cn = mkCN compl.mov_a (mkN "lumisade");
                    mov_cn = mkCN (mkN "lumi" "lumen" "lumia" "lunta");
                    mov_v = mkV rain_variant compl.mov_adv.s
                };
        
        SemVar5aPresentPerfect = mkTemp presentTense anteriorAnt;
        SemVar5bPastPerfect = mkTemp pastTense anteriorAnt;
        SemVar5cSimplePast = mkTemp pastTense simultaneousAnt;
        
        SemVar1aWeek = {n = mkN viikko_NK;period_N = mkN "viikko" (mkN kausi_NK)};
        SemVar1bDay = {n = mkN pa'iva'_NK;period_N = mkN "päivä" (mkN kausi_NK)};
        SemVar1cHour = {n = mkN tunti_NK;period_N = mkN "tunti" (mkN kausi_NK)};
        SemVar2aSeveral timeunit =
            variants
            {
                mkNP aPl_Det timeunit.n;
                mkNP (ParadigmsFin.mkDet plural (mkN usea_AK)) timeunit.n;
                mkNP (ParadigmsFin.mkDet plural (mkN moni_AK)) timeunit.n;
                mkNP (ParadigmsFin.mkDet singular (mkN moni_AK)) timeunit.n;
                mkNP aPl_Det timeunit.period_N;
            };
        SemVar3aAlps = {np = mkNP thePl_Det (mkN "Alppi");loc_case = AdesLoc}; --easiest to treat the Alps as normal noun, bit complicated to set agreement for plural proper noun
        SemVar3bLapland = {np = mkNP (mkPN "Lappi");loc_case = InesLoc};
        SemVar3cSiberia = {np = mkNP (mkPN "Siperia");loc_case = InesLoc};
        SemVar3dAndes = {np = mkNP thePl_Det (mkN "Andi");loc_case = AdesLoc};
        SemVar3eGreenland = {np = mkNP (mkPN "Grönlanti");loc_case = InesLoc};
        SemVar3fSweden = {np = mkNP (mkPN "Ruotsi");loc_case = InesLoc};
        SemVar4aSnow =
          {
            event_cn = mkCN (mkN "lumentulo");
            act_cn = mkCN (mkN "lumisade");
            mov_cn = mkCN (mkN "lumi" "lumen" "lumia" "lunta");
            mov_v = variants {mkV "sataa";mkV "tulla"};
          };
        
        SemVar4bHeavySnow =
            let
                mov_adv_variant : Adv = variants
                  {
                    ParadigmsFin.mkAdv "runsaasti";
                    ParadigmsFin.mkAdv "paljon";
                    ParadigmsFin.mkAdv "sankasti";
                    ParadigmsFin.mkAdv "sakeasti";
                    ParadigmsFin.mkAdv "paljon";
                    ParadigmsFin.mkAdv "voimakkaasti";
                    ParadigmsFin.mkAdv "kovasti";
                    ParadigmsFin.mkAdv "raskaasti";
                  }
            in
                {
                    event_cn = mkCN (mkN "lumentulo");
                    act_cn = mkCN (mkN "lumisade");
                    mov_cn = mkCN (mkN "lumi" "lumen" "lumia" "lunta");
                    mov_v = variants {mkV "sataa";mkV "tulla"};
                    mov_adv = mov_adv_variant;
                    mov_a = variants
                    {
                        mkA sankka_NK;
                        mkA sakea_NK;
                        mkA voimakas_AK;
                        mkA kova_AK;
                        mkA raskas_AK;
                        mkA runsas_AK;
                    };
                };
        
        IntransitiveMovementVerb0Subject_ComplexMetPhen1Object_MetPhen1InsideLocative_GeoArea2Benefactive_TimePeriod3 pform m g t =
            generate_adv_variants_for_geo_period_object_and_intensifier pform (mkNP m.mov_cn) (mkVP m.mov_v) g t (m.mov_adv);
            
        
        --"lunta on satanut (runsaasti) viikkoja (runsaasti) alpeilla (runsaasti)"
        --"alpeilla on satanut (runsaasti) lunta (runsaasti) viikkoja (runsaasti)"
        --"viikkoja on lunta satanut (runsaasti) alpeilla (runsaasti)"
        --"viikkoja on satanut (runsaasti) alpeilla (runsaasti) lunta (runsaasti)"
        --"alpeilla on viikkoja satanut (runsaasti) lunta (runsaasti)"
        --"alpeilla on viikkoja satanut (runsaasti) lunta (runsaasti)"
        --NOT "alpeilla viikkoja on satanut lunta"
        --NOT "
        --Object is pretty movable in Finnish, so makes sense to implement as adverb
        IntransitiveMovementVerb0Subject_MetPhen1Object_MetPhen1InsideLocative_GeoArea2Benefactive_TimePeriod3 pform m g t =
            generate_adv_variants_for_geo_period_and_object pform (mkNP m.mov_cn) (mkVP m.mov_v) g t;
            
        
            
        --"alpeilla on tullut/ollut lumisateita viikkoja"
        --"lumisateita on viikkoja ollut alpeilla"
        --"lumisateita on alpeilla ollut viikkoja"
        --"viikkoja on alpeilla ollut lumisateita"
        PleonasticThere0Object_MetPhen1InsideLocative_GeoArea2Benefactive_TimePeriod3 pform m g t =
            let
                act_np : NP = variants {mkNP m.act_cn;mkNP aPl_Det m.act_cn};
                act_vp : VP = variants {mkVP vOlla;mkVP (mkV tulla_VK)};
                
            in
                generate_adv_variants_for_geo_period_and_object pform act_np act_vp g t;
                
        --"lumisade/lumisateet/lumentulo on jatkunut alpeilla viikkoja"
        --"alpeilla lumisade on jatkunut viikkoja"
        --"viikkoja lumisade on jatkunut alpeilla"
        --"lumisade on viikkoja jatkunut alpeilla"
        --"lumisade on alpeilla jatkunut viikkoja"
        --NOTE: "lumisade alpeilla on jatkunut viikkoja" IS valid, since "alpeilla" can be seen as modifier of "lumisade"
        --"
        IntransitiveTimeVerb0Subject_MetPhen1Object_MetPhen1InsideLocative_GeoArea2Benefactive_TimePeriod3 pform m g t =
            let
                event_np : NP = variants
                {
                    mkNP m.event_cn;
                    mkNP m.act_cn;
                    mkNP aPl_Det m.act_cn
                };
            in
                generate_adv_variants_for_geo_period_and_subject pform event_np (mkVP (mkV "jatkua")) g t;
        
}   