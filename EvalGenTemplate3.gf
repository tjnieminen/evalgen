--# -coding=UTF-8
abstract EvalGenTemplate3 = {
    flags
        startcat = TestSentence;
    cat
        TestSentence;
        Parties;
        PositiveAct;
        DiscordantRelation;
        Country;
        SerialCommaType;
    fun
        --"despite x, the parties still have y"
        DespitePositiveAct : PositiveAct -> DiscordantRelation -> TestSentence;
        HaveDifferences : Parties -> DiscordantRelation;
        PublicReconciliation : PositiveAct;
        SemVar1aTwoCountries : Parties;
        SemVar1bTwoSpecificCountries : Country -> Country -> Parties;
        SemVar1cThreeSpecificCountries : Country -> Country -> Country -> SerialCommaType -> Parties;
        SemVar2aRussia,SemVar2bTurkey,SemVar2cUkraine,SemVar2dChina,SemVar2eMongolia,SemVar2fArmenia : Country;
        Contrast1aSerialComma,Contrast1bNonSerialComma : SerialCommaType;
    }