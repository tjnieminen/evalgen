import sys

# &#949; = lowercase epsilon
alphabet = []

with open(sys.argv[1],'r') as syms:
    for line in syms.readlines():
        alphabet.append(line.split()[0])

weight = {
    "delete": 1.0,
    "insert": 1.0,
    "sub": 1.0
}

# No edit
for l in alphabet:
    print("0 0 %s %s %.3f" % (l, l, 0))

# Substitutions: input one character, output another
for l in alphabet:
    for r in alphabet:
        if l is not r and r != "<unk>":
            print("0 0 %s %s %.3f" % (l, r, weight["sub"]))

# Final state
print(0)