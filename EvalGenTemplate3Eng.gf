concrete EvalGenTemplate3Eng of EvalGenTemplate3 = open Prelude,LangEng,ParadigmsEng,SyntaxEng,DictEng,(ResEng = ResEng),ExtraEng in {
    param
        Frontability = Frontable | NonFrontable;
        SerialCommaParam = Ser | NonSer;
    
    --adapted from experimental/Pred
    oper
        effort_rec : Type = {soa : NP;effort : V;effort_nom : NP};
        defaultAgr : ResEng.Agr = ResEng.AgP3Sg ResEng.Neutr ;
        
        nominalize : VP -> NP = \vpi ->
        lin NP
        {
            s = \\c => vpi.prp ++ vpi.s2 ! defaultAgr;
            a = defaultAgr
        } ;
        
        nominalizeWithAdverbMovement : VPWithAdvMovement -> NP = \vpi ->
            lin NP (
                table
                {                    
                    Frontable =>
                        variants
                        {
                            {
                                s = \\c => vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s;
                                a = defaultAgr;
                            };
                            {
                                s = \\c => vpi.adv.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr;
                                a = defaultAgr;
                            };
                        };
                    NonFrontable =>
                        {
                            s = \\c => vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s;
                            a = defaultAgr;
                        }
                } ! vpi.frontable
            );
        
        --CN variant for nominalization, in case adjectives need to be added etc.
        nominalizeCN : VP -> CN = \vpi ->
        lin CN
        {
            s = \\n,c => vpi.prp ++ vpi.s2 ! defaultAgr;
            g = ResEng.Neutr
        } ;
        
        --e.g. despite x-ing
       
        VPWithAdvMovement : Type = {vp : VP;frontable : Frontability;adv : Adv};
        AdvWithFrontability : Type = {frontable : Frontability;adv : Adv};
        
        participleAdv : Prep -> VPWithAdvMovement -> Adv = \prep,vpi ->
            lin Adv (
                table
                {                    
                    Frontable =>
                        variants
                        {
                            --adjectival adverbs of the participle can be in front or back, e.g. "publicly reconciling"/"reconciling publicly"
                            {s = prep.s ++ vpi.adv.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr;};
                            {s = prep.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s ;};
                        };
                    NonFrontable => {s = prep.s ++ vpi.vp.prp ++ vpi.vp.s2 ! defaultAgr ++ vpi.adv.s;} --prepositional adverbs of the participle can only at the back, e.g. "reconciling in public"
                } ! vpi.frontable
            );
        
        
        
        
    lincat
        TestSentence = S;
        Parties = NP;
        PositiveAct = {np: NP; vp: VPWithAdvMovement};
        DiscordantRelation = {subj : NP; s : S; pron_s : S;subj_pron : NP};
        Country = NP;
        SerialCommaType = SerialCommaParam;

    lin
                
        --"despite public reconciliation, the two parties still have differences
        DespitePositiveAct posact discord =
            let
                mkSImperfectOrPerfect : Cl -> S = variants {\cl-> mkS presentTense anteriorAnt cl;\cl-> mkS pastTense simultaneousAnt cl};
                posact_vp = mkVP posact.vp.vp posact.vp.adv;
            in
                variants
                {
                    --despite posact 
                    let
                        adv_variant : Adv = variants
                            {
                                participleAdv despite_Prep posact.vp;
                                mkAdv despite_Prep posact.np;
                            };
                        comma_adv_variant : Adv = adv_variant ** {s = adv_variant.s ++ ","};
                    in
                        variants
                            {
                                mkS comma_adv_variant discord.s;
                            };
                    
                    --even though posact, still have discord/still have discord, even though posact  
                    let
                        although_variant : Subj = variants {even_though_Subj;although_Subj;};
                        adv_variant : Adv = (mkAdv although_variant (mkSImperfectOrPerfect (mkCl discord.subj posact_vp)));
                        comma_adv_variant : Adv = adv_variant ** {s = adv_variant.s ++ ","};
                    in
                        variants
                            {
                                mkS comma_adv_variant discord.pron_s;
                                SSubjS discord.s although_variant (mkSImperfectOrPerfect (mkCl discord.subj_pron posact_vp));
                            };
                    
                    
                    mkS (mkConj ", but") (mkSImperfectOrPerfect (mkCl discord.subj posact_vp)) discord.pron_s;
                    
                };
        
        PublicReconciliation =
            let
                a_variant : A = public_A;
                a_n_variant : N = public_N;
            in
                let
                    n : N = reconciliation_N;
                    np : NP = mkNP (mkCN a_variant n);
                    v : V = mkV "reconcile";
                    mend_variant : V2 = variants
                        {
                            repair_V2;
                            mend_V2;
                            restore_V2;
                        };
                    relation_variant : NP = variants
                        {
                            mkNP they_Pron relationship_N;
                            mkNP (mkDet they_Pron pluralNum) (relation_N);
                        };
                    vp_variant : VP = variants
                        {
                            mkVP v;
                            mkVP mend_variant relation_variant;
                        };
                    adj_adv_variant : Adv = variants {mkAdv a_variant};
                    prep_adv_variant : Adv = variants {mkAdv in_Prep (mkNP a_n_variant)};
                    vp_adv : VPWithAdvMovement =
                        variants
                        {
                            {vp = vp_variant; frontable = NonFrontable; adv =  prep_adv_variant}; --"reconcile in public"
                            {vp = vp_variant; frontable = Frontable; adv = adj_adv_variant}; --"reconcile publicly"
                            
                        };
                    np_variant : NP = variants {np}
                in
                    {np = np_variant; vp = vp_adv};

        --DiscordantRelationWrap discord = discord.s;
        HaveDifferences parties =
            let
                still_adv : AdV = mkAdV "still";
                significant_A : A = variants {major_A;significant_A;substantial_A};
                sig_dif_cn : CN = mkCN significant_A (difference_N);
            in
                variants
                {
                    let
                        subj_pron = mkNP they_Pron;
                        vp : VP = mkVP still_adv (mkVP have_V2 (mkNP aPl_Det sig_dif_cn));
                        s : S = mkS presentTense (mkCl parties vp);
                        pron_s : S = mkS presentTense (mkCl subj_pron vp);
                    in
                        {s = s; pron_s = pron_s;subj = parties;subj_pron = subj_pron};
                        
                };
                
        SemVar1aTwoCountries =
            let
                quant_variant = variants {the_Quant;this_Quant};
            in
                variants
                {
                    --mkNP quant_variant (mkNum "2");
                    mkNP quant_variant (mkNum "2") (country_N);
                };
                
        --should have "states/countries/governments of x and y"
        SemVar1bTwoSpecificCountries c1 c2 = 
            let
                state_variant_N : N = variants {government_N;country_N;state_N};
                country_list : ListNP = mkListNP c1 c2;
            in
                variants
                {
                    mkNP thePl_Det (mkCN (mkN2 state_variant_N) (mkNP and_Conj country_list));
                    mkNP and_Conj country_list;
                };
                
        SemVar1cThreeSpecificCountries c1 c2 c3 serialcomma = 
            let
                state_variant_N : N = variants {government_N;country_N;state_N};
                conj : Conj = case serialcomma of {Ser => mkConj ", and";NonSer => mkConj "and"};
                country_list : ListNP = mkListNP c1 (mkListNP c2 c3);
            in
                variants
                {
                    mkNP thePl_Det (mkCN (mkN2 state_variant_N) (mkNP conj country_list));
                    mkNP conj country_list;
                };
        
        SemVar2bTurkey = mkNP turkey_PN;
        SemVar2aRussia = mkNP russia_PN;
        SemVar2cUkraine = variants {mkNP (mkPN "Ukraine");mkNP (mkPN "the Ukraine")};
        SemVar2dChina = mkNP china_PN;
        SemVar2eMongolia = mkNP mongolia_PN;
        SemVar2fArmenia = mkNP (mkPN "Armenia");
        Contrast1aSerialComma = Ser;
        Contrast1bNonSerialComma = NonSer;
}   