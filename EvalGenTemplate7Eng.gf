concrete EvalGenTemplate7Eng of EvalGenTemplate7 = open Prelude,LangEng,ParadigmsEng,SyntaxEng,DictEng,(ResEng = ResEng),ExtraEng,MorphoEng in {
    param
        Specificity = Specific|Unspecific|AdverbSpecified;
    oper
        storeref_rec : Type = {np : NP;specific : Specificity};
        sub_rec : Type = {sent : S;specific : Specificity};
        itemref_rec : Type = {np : NP;pol : Pol;pleonastic_np : NP};
        set_np_agr : Number -> NP -> NP = \number,np->
            let
                np_agr: ResEng.Agr = table
                    {
                        ResEng.Sg => ResEng.AgP3Sg ResEng.Neutr;
                        ResEng.Pl => ResEng.AgP3Pl ResEng.Neutr
                    } ! number;
            in
                np ** {a = np_agr};
        make_item_ref_2 : NP -> Pol -> NP -> Number -> {np : NP;pol : Pol;pleonastic_np : NP} = \np1,pol,np2,number->
            {np = set_np_agr number np1; pol = pol;pleonastic_np = set_np_agr number np2};
        make_item_ref : NP -> Pol -> Number -> {np : NP;pol : Pol;pleonastic_np : NP} = \np1,pol,number->
            make_item_ref_2 np1 pol np1 number;
        plain_subclause : {np : NP;specific : Specificity} -> VP -> {np : NP;pol : Pol} -> {sent : S;specific : Specificity} = \ref,vp,itemref->
            let
                pol : Pol = itemref.pol;
                sent : S = table {
                    AdverbSpecified => mkS pastTense simultaneousAnt pol (mkCl they_NP (mkVP vp (mkAdv in_Prep ref.np)));
                    _ => mkS pastTense simultaneousAnt pol (mkCl ref.np vp)
                    
                    
                } ! ref.specific;
            in
                {sent = sent;specific = ref.specific};
        pleonastic_subclause : {np : NP;specific : Specificity} -> NP -> {np : NP;pol : Pol} -> {sent : S;specific : Specificity} = \ref,np,itemref->
            let
                pol : Pol = itemref.pol;
                sent : S = table {
                    Unspecific => mkS pastTense simultaneousAnt pol (mkCl np);
                    _ => mkS pastTense simultaneousAnt pol (mkCl (mkNP np (mkAdv in_Prep ref.np)))
                } ! ref.specific;
            in
                {sent = sent;specific = ref.specific};
        
    lincat
        VolitionVerb = VV;
        Statement = S;
        Customer = NP;
        Store = NP;
        Item = NP;
        StoreReferent = storeref_rec;
        Subclause = sub_rec;
        ItemReferent = itemref_rec;
        ForSaleVerb = V2;
        ForSaleAdverb = Adv;
        HaveVerb = V2;
        Media = CN;
        MediaOwner = NP;
        Drink = CN;
        JuiceBase = CN;
        
    lin
        SemVar3dDrinkCan drink = variants
        {
            mkNP aSg_Det (ExtraEng.CompoundCN drink (mkCN can_N));
            mkNP aSg_Det (mkCN (mkN2 can_N) (mkNP drink));
        };
        SemVar7aApple = mkCN (apple_N);
        SemVar7bOrange = mkCN (orange_1_N);
        SemVar7cGrape = mkCN (grape_N);
        SemVar7dCarrot = mkCN (carrot_N);
        SemVar7ePear = mkCN (pear_N);
        SemVar6bJuiceFrom base = ExtraEng.CompoundCN base (mkCN juice_N);
        SemVar6aSoda = mkCN (soda_N);
        
        Want = want_VV;
        Intend = intend_VV;
        InStock = mkAdv in_Prep (mkNP stock_N);
        ForSale = mkAdv for_Prep (mkNP sale_N);
        Them = make_item_ref_2 they_NP negativePol (mkNP (mkN "any of them")) plural;
        One = make_item_ref (mkNP (mkN "one")) negativePol singular;
        Any = make_item_ref (mkNP (mkN "any")) negativePol plural;
        AnyOfThem = make_item_ref (mkNP (mkN "any of them")) negativePol plural;
        None = make_item_ref (mkNP (mkN "none")) positivePol plural;
        NoneOfThem = make_item_ref (mkNP (mkN "none of them")) positivePol plural;
        
        Have = have_V2;
        Sell = sell_V2;
        Carry = carry_V2;
        SemVar1aGenericStore = variants
        {
            mkNP the_Det (store_N);
            mkNP the_Det (shop_N);
        };
        StoreToStoreReferent store = {np = store;specific = Specific};
        StoreToStoreReferentAdverb store = {np = store;specific = AdverbSpecified};
        UnspecifiedStore = {np = they_NP;specific=Unspecific};
        SemVar2aWe = we_NP;
        SemVar3aCassettePlayer = mkNP aSg_Det (mkN "cassette" player_N);
        SemVar3bPlayerFor media mediaowner = mkNP aSg_Det (mkCN (mkCN player_N) (mkAdv for_Prep (mkNP(GenNP mediaowner) pluralNum media)));
        SemVar3cPlayerForOld media mediaowner = mkNP aSg_Det (mkCN (mkCN player_N) (mkAdv for_Prep (mkNP (GenNP mediaowner) pluralNum (mkCN old_A media))));
        SemVar4aCassetteMedia = mkCN (cassette_N);
        SemVar5aMe = i_NP;
        
        ActiveTransitiveVolitionVerbInfinitivalSubordinateClause0Subject_Customer1Verb_Volition2Verb3InfinitivalObject_Item4Source_Store3Subordinate_StoreSubclause5 customer volition_verb item store sub =
            let
                inner_vp : VP = mkVP buy_V2 item;
                outer_vp : VP = mkVP volition_verb inner_vp;
                store_adv_vp : VP = mkVP outer_vp (mkAdv from_Prep store);
                subbed_vp_stored_adv : VP = mkVP store_adv_vp (mkAdv but_Subj sub.sent);
                subbed_vp : VP = mkVP outer_vp (mkAdv but_Subj sub.sent);
                
            in
                table
                {
                    Unspecific => mkS pastTense simultaneousAnt positivePol(mkCl customer subbed_vp_stored_adv);
                    _ => mkS pastTense simultaneousAnt positivePol(mkCl customer subbed_vp)
                } ! sub.specific;
                
        
        
        
        SubclauseActiveTransitive0Subject_Store1Verb_ForSaleVerb2Object_ItemReferent3 ref verb itemref = plain_subclause ref (mkVP verb itemref.np) itemref;
        
        SubclauseActiveTransitive0Subject_Store1Verb_HaveVerb2Object_ItemReferent3 ref verb itemref = plain_subclause ref (mkVP verb itemref.np) itemref;
        
        SubclauseActiveTransitive0Subject_Store1Verb_HaveVerb2Object_ItemReferent3Adverb_ForSaleAdverb4 ref verb itemref forsaleadverb =
            plain_subclause ref (mkVP (mkVP verb itemref.np) forsaleadverb) itemref;
        
        SubclauseNeoplasticThere0Adverbial_Store1Verb_HaveVerb2Object_ItemReferent3Adverb_ForSaleAdverb4 ref itemref forsaleadverb =
            pleonastic_subclause ref (mkNP itemref.pleonastic_np forsaleadverb) itemref;
        
       }   